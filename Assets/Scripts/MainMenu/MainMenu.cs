﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    private const string HUB = "Hub";
    private const string MAIN = "Main";

    public Dialogger dialogger;

    #region PRIVATE_METHODS
    void Awake() {

    }

    void Start() {
        if (!GameProgression.Instance.ObjectiveComplete(GameProgressionID.WELCOME)) {
            dialogger.ShowText(GameProgression.Instance.CurrentObjective.dialogModule);
        }
    }
    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    public void LoadMain() {
        SceneManager.LoadScene(MAIN);
    }

    public void LoadHub() {
        SceneManager.LoadScene(HUB);
    }

    #endregion PUBLIC_METHODS

    void Update() {

    }
}
