﻿using UnityEngine;
using System.Collections;

public class SFXLibrary : MonoBehaviour {

    public SFXScriptable[] sfxList;


    public AudioClip GetClipByName(SFXName name) {
        AudioClip rVal = null;
        for (int i = 0; i < sfxList.Length; i++) {
            if (sfxList[i].sfxName == name) {
                rVal = sfxList[i].clip;
            }
        }
        return rVal;
    }
}
