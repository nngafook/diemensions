﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ResultEvent : UnityEvent<bool> { }

public class ConfirmActionPopup : MonoBehaviour {

    private static ConfirmActionPopup instance = null;

    public static ResultEvent OnResult;

    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;

    private static bool decisionMade = false;
    public static bool DecisionMade { get { return decisionMade; } }
    private static bool result;
    public static bool Result { get { return result; } }

    public Button yesButton;
    public Button noButton;

    #region PRIVATE_METHODS
    void Awake() {
        instance = this;
        canvasGroup = this.GetComponent<CanvasGroup>();
        rectTransform = this.GetComponent<RectTransform>();
    }

    void Start() {
        yesButton.onClick.AddListener(YesClicked);
        noButton.onClick.AddListener(NoClicked);
    }

    private void YesClicked() {
        result = true;
        Hide();
        if (OnResult != null) {
            OnResult.Invoke(result);
        }
    }

    private void NoClicked() {
        result = false;
        Hide();
        if (OnResult != null) {
            OnResult.Invoke(result);
        }
    }

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    public static void Show() {
        decisionMade = false;
        instance.canvasGroup.alpha = 1;
        instance.canvasGroup.interactable = true;
        instance.canvasGroup.blocksRaycasts = true;
        instance.rectTransform.DOPunchScale(new Vector3(0.06f, 0, 0.06f), 0.5f);
    }

    public void Hide() {
        canvasGroup.alpha = 0;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
        decisionMade = true;
    }

    #endregion PUBLIC_METHODS

    void Update() {

    }
}
