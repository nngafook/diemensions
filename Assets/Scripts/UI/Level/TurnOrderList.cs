﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;

public class TurnOrderList : MonoBehaviour {


    private float padding = 10;

    private int maxIcons = 5;
    private float iconSize = 50;
    private float startX;
    private string prefabPath = "Prefabs/TurnOrderItem";

    public List<TurnOrderItem> itemList;



    public RectTransform listContainer;

	#region PRIVATE_METHODS
	void Awake() {
        startX = listContainer.anchoredPosition.x;
	}
	
	void Start () {
	
	}

    //private void Slide() {
    //    RectTransform item = itemList[0].GetComponent<RectTransform>();

    //    item.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(OnScaleComplete);
    //}

    //private void OnScaleComplete() {
    //    listContainer.DOAnchorPos(new Vector2(startX - iconSize, listContainer.anchoredPosition.y), 0.5f).OnComplete(OnSlideComplete);
    //}

    private void OnSlideComplete() {
        TurnOrderItem item = itemList[0];
        itemList.RemoveAt(0);

        listContainer.anchoredPosition = listContainer.anchoredPosition.SetX(startX);

        DestroyImmediate(item.gameObject);
    }

    private TurnOrderItem ItemByUID(UID id) {
        TurnOrderItem rVal = null;

        for (int i = 0; i < itemList.Count; i++) {
            if (itemList[i].uid == id) {
                rVal = itemList[i];
                break;
            }
        }

        return rVal;
    }

    private void OnItemScaleComplete(TurnOrderItem item) {
        item.RemoveScaleCompleteCallback(OnItemScaleComplete);

        itemList.RemoveAt(item.ListIndex);
        DestroyImmediate(item.gameObject);

        UpdateListIndexes();
        UpdateItemPositions();
    }

    private void OnTurnEndScaleComplete(TurnOrderItem item) {
        item.RemoveScaleCompleteCallback(OnTurnEndScaleComplete);

        MoveItemToBack(item);
        UpdateListIndexes();
        UpdateItemPositions();
        item.ResetScale();
    }

    private void MoveItemToBack(TurnOrderItem item) {
        itemList.Remove(item);
        itemList.Add(item);
    }

    private void UpdateListIndexes() {
        for (int i = 0; i < itemList.Count; i++) {
            itemList[i].ListIndex = i;
        }
    }

    private void UpdateItemPositions() {
        float targetX = 0;
        for (int i = 0; i < itemList.Count; i++) {
            targetX = (i * iconSize) + (iconSize / 2) + (padding * i);
            if (itemList[i].AnchoredX != targetX) {
                itemList[i].TweenMoveX(targetX);
            }
        }
    }

    private void ClearItems() {
        for (int i = 0; i < itemList.Count; i++) {
            DestroyImmediate(itemList[i].gameObject);
        }
        itemList.Clear();
    }

	#endregion PRIVATE_METHODS

	#region PUBLIC_METHODS

    public void AddEntity(Entity entity) {
        TurnOrderItem item = (Instantiate(Resources.Load(prefabPath)) as GameObject).GetComponent<TurnOrderItem>();
        item.uid = entity.uid;
        item.ListIndex = itemList.Count;

        if (entity.spriteTransform != null) {
            item.SetEntityIcon(entity.spriteTransform.GetComponent<SpriteRenderer>().sprite);
        }
        else if (entity.skeletonAnimation != null) {
            item.SetEntityIcon(entity.skeletonAnimation.skeletonDataAsset, entity.skeletonAnimation.initialSkinName);
        }

        item.transform.SetParent(listContainer);

        item.SetAnchoredPos((itemList.Count * iconSize) + (iconSize / 2) + (padding * itemList.Count), -(iconSize / 2));

        itemList.Add(item);
    }

    public void TurnEnded() {
        TurnOrderItem item = itemList[0];

        item.AddScaleCompleteCallback(OnTurnEndScaleComplete);
        item.ScaleOut();

    }

    public void RemoveAt(UID id) {
        TurnOrderItem item = null;

        for (int i = 0; i < itemList.Count; i++) {
            if (itemList[i].uid == id) {
                item = itemList[i];
                item.ScaleOut();
            }
        }
        item.AddScaleCompleteCallback(OnItemScaleComplete);

    }

	#endregion PUBLIC_METHODS

	void Update () {
        if (Input.GetKeyUp(KeyCode.Alpha1)) {
            RemoveAt(itemList[0].uid);
        }
        if (Input.GetKeyUp(KeyCode.Alpha2)) {
            RemoveAt(itemList[1].uid);
        }
        if (Input.GetKeyUp(KeyCode.Alpha3)) {
            RemoveAt(itemList[2].uid);
        }
        if (Input.GetKeyUp(KeyCode.Alpha4)) {
            RemoveAt(itemList[3].uid);
        }
        if (Input.GetKeyUp(KeyCode.Alpha5)) {
            RemoveAt(itemList[4].uid);
        }
        if (Input.GetKeyUp(KeyCode.Alpha6)) {
            RemoveAt(itemList[5].uid);
        }
        if (Input.GetKeyUp(KeyCode.Alpha7)) {
            RemoveAt(itemList[6].uid);
        }
        if (Input.GetKeyUp(KeyCode.Alpha8)) {
            RemoveAt(itemList[7].uid);
        }
        if (Input.GetKeyUp(KeyCode.Alpha9)) {
            RemoveAt(itemList[8].uid);
        }
        if (Input.GetKeyUp(KeyCode.Alpha0)) {
            ClearItems();
            GameManager.Instance.SetOrderIcons();
        }
	}
}
