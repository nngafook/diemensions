﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class TurnOrderScaleEvent : UnityEvent<TurnOrderItem> { }

public class TurnOrderItem : MonoBehaviour {

    private float animationSpeed = 0.25f;
    private Vector3 baseScale;
    private RectTransform rectTransform;
    [SerializeField]
    private int listIndex;
    public int ListIndex { get { return listIndex; } set { listIndex = value; } }

    [HideInInspector]
    public TurnOrderScaleEvent ScaleCompleteEvent;

    public Image entityIcon;
    public SkeletonGraphic entitySkeleton;
    public UID uid;

    public float AnchoredX { get { return rectTransform.anchoredPosition.x; } }

	#region PRIVATE_METHODS
	void Awake() {
        rectTransform = this.GetComponent<RectTransform>();
	}
	
	void Start () {
        baseScale = Vector3.one;
        rectTransform.localScale = baseScale;
	}

    private void OnScaleComplete() {
        if (ScaleCompleteEvent != null) {
            ScaleCompleteEvent.Invoke(this);
        }
    }

	#endregion PRIVATE_METHODS

	#region PUBLIC_METHODS

    public void SetAnchoredPos(float x, float y) {
        rectTransform.anchoredPosition = rectTransform.anchoredPosition.SetX(x);
        rectTransform.anchoredPosition = rectTransform.anchoredPosition.SetY(y);
    }

    public void SetEntityIcon(Sprite sprite) {
        entitySkeleton.gameObject.SetActive(false);
        entityIcon.sprite = sprite;
    }

    public void SetEntityIcon(SkeletonDataAsset dataAsset, string skinName) {
        entityIcon.gameObject.SetActive(false);
        entitySkeleton.skeletonDataAsset = dataAsset;
        entitySkeleton.initialSkinName = skinName;
    }

    public void ScaleOut() {
        rectTransform.DOScale(0, animationSpeed).SetEase(Ease.InBack).OnComplete(OnScaleComplete);
    }

    public void ResetScale() {
        rectTransform.DOScale(baseScale, animationSpeed);
    }

    public void AddScaleCompleteCallback(UnityAction<TurnOrderItem> callback) {
        if (ScaleCompleteEvent == null) {
            ScaleCompleteEvent = new TurnOrderScaleEvent();
        }
        ScaleCompleteEvent.AddListener(callback);
    }

    public void RemoveScaleCompleteCallback(UnityAction<TurnOrderItem> callback) {
        if (ScaleCompleteEvent != null) {
            ScaleCompleteEvent.RemoveListener(callback);
            ScaleCompleteEvent = null;
        }
    }

    public void TweenMoveX(float target) {
        rectTransform.DOAnchorPosX(target, animationSpeed);
    }

	#endregion PUBLIC_METHODS

	void Update () {
	
	}
}
