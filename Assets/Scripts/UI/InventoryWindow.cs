﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InventoryWindow : MonoBehaviour {

    private CanvasGroup canvasGroup;
    private List<InventoryTextItem> textList = new List<InventoryTextItem>();

    public Transform textListPanel;

    #region PRIVATE_METHODS
    void Awake() {
        canvasGroup = this.GetComponent<CanvasGroup>();
    }

    void Start() {

    }
    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    public void SetOpen(bool value) {
        canvasGroup.alpha = value.AsInt();
        canvasGroup.blocksRaycasts = value;
        canvasGroup.interactable = value;
    }

    public void AddItem(string id, int amount) {
        Debug.LogWarning("Pool this shit".Bold().Colored(Color.magenta));
        InventoryTextItem item = Instantiate(Resources.Load("Prefabs/InventoryTextItem") as GameObject).GetComponent <InventoryTextItem>();
        item.transform.SetParent(textListPanel);
        item.SetInfo(id, amount);
        textList.Add(item);
    }

    public void AddToItem(string id, int amount) {
        for (int i = 0; i < textList.Count; i++) {
            if (textList[i].ID == id) {
                textList[i].AddAmount(amount);
                break;
            }
        }
    }

    #endregion PUBLIC_METHODS

    void Update() {

    }
}
