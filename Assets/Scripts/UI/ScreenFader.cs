﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;

public class ScreenFader : MonoBehaviour {

    private static ScreenFader instance;

    private static CanvasGroup canvasGroup;
    private static RectTransform rectTransform;

    public Image overlayImage;

	#region PRIVATE_METHODS
	void Awake() {
        instance = this;
        canvasGroup = instance.GetComponent<CanvasGroup>();
        rectTransform = instance.GetComponent<RectTransform>();
	}
	
    private static void SetColorTransparency(float transparency) {
        Color c = instance.overlayImage.color;
        c.a = transparency;
        instance.overlayImage.color = c;
    }

    private static void OnFadeToBlackComplete() {
        MSN.Instance.TriggerEvent(EventName.FADE_TO_BLACK_COMPLETE);
    }

    private static void OnFadeFromBlackComplete() {
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
        rectTransform.localScale = Vector3.zero;
        MSN.Instance.TriggerEvent(EventName.FADE_FROM_BLACK_COMPLETE);
    }

	#endregion PRIVATE_METHODS

	#region PUBLIC_METHODS
	
    public static void FadeFromBlack() {
        canvasGroup.alpha = 1;
        canvasGroup.DOFade(0, 0.5f).OnComplete(OnFadeFromBlackComplete);
    }

    public static void FadeToBlack(float transparency = 1) {
        SetColorTransparency(transparency);

        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;
        canvasGroup.alpha = 0;
        rectTransform.localScale = Vector3.one;
        canvasGroup.DOFade(1, 0.5f).OnComplete(OnFadeToBlackComplete);
    }

    public static void SnapToBlack(float transparency = 1) {
        SetColorTransparency(transparency);

        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;
        rectTransform.localScale = Vector3.one;
        canvasGroup.alpha = 1;
    }

    public static void SnapFromBlack() {
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
        rectTransform.localScale = Vector3.zero;
        canvasGroup.alpha = 0;
    }

	#endregion PUBLIC_METHODS

	void Update () {
	
	}
}
