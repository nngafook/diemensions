﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageNotifier : MonoBehaviour {

    private static MessageNotifier instance = null;

    private static Vector2 offPosition;
    private static Vector2 onPosition = Vector2.zero;
    private static List<string> queuedMessages = new List<string>();

    private static bool isShowing = false;
    private float showDuration = 1f;

    public Text messageTextField;
    public static Text MessageTextField { get { return instance.messageTextField; } }

    public CanvasGroup canvasGroupObject;
    public RectTransform bgRectTransform;
    public RectTransform textRectTransform;

    #region PRIVATE_METHODS
    void Awake() {
        instance = this;
        offPosition = new Vector2(1200, 0);
    }

    void Start() {

    }

    private static void ShowMessage() {
        instance.canvasGroupObject.alpha = 1;
        instance.bgRectTransform.DOAnchorPos(onPosition, 0.25f).SetEase(Ease.Linear);
        instance.textRectTransform.DOAnchorPos(onPosition, 0.5f).SetDelay(0.2f).SetEase(Ease.OutExpo).OnComplete(MessageShowTweenComplete);
    }

    private static void HideMessage() {
        instance.canvasGroupObject.DOFade(0, 0.5f).OnComplete(MessageHideTweenComplete);
    }

    private static void MessageShowTweenComplete() {
        instance.StartShowingTimer();
        //PunchMessage();
    }

    private static void PunchMessage() {
        MessageTextField.GetComponent<RectTransform>().DOPunchScale(new Vector3(0.06f, 0.06f, 0f), 0.75f);
    }

    private static void MessageHideTweenComplete() {
        isShowing = false;
        instance.bgRectTransform.anchoredPosition = offPosition;
        instance.textRectTransform.anchoredPosition = offPosition;
        instance.PushNextMessage();
    }

    public void PushNextMessage() {
        if (queuedMessages.Count > 0) {
            PushMessage(queuedMessages[0]);
            queuedMessages.RemoveAt(0);
        }
        else {
            MSN.Instance.TriggerEvent(EventName.PHASE_MESSAGE_COMPLETE);
        }
    }

    private void StartShowingTimer() {
        StartCoroutine("ShowingCounter");
    }

    private IEnumerator ShowingCounter() {
        yield return new WaitForSeconds(showDuration);
        HideMessage();
    }

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS
    public static void PushMessage(string message, bool pushToFront = false) {
        if (!isShowing) {
            isShowing = true;
            MessageTextField.text = message.ToUpper();
            ShowMessage();
        }
        else {
            if (pushToFront) {
                queuedMessages.Insert(0, message);
                instance.EndMessageShowingEarly();
            }
            else {
                queuedMessages.Add(message);
            }
        }
    }

    public void EndMessageShowingEarly() {
        StopAllCoroutines();
        isShowing = false;
        instance.PushNextMessage();
    }

    #endregion PUBLIC_METHODS

    void Update() {

    }
}
