﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class EntityStatusBars : MonoBehaviour {

    public UnityEvent OnHealthScaleComplete;
    public UnityEvent OnResourceScaleComplete;

    public Transform barsBG;
    public Transform healthBar;
    public Transform resourceBar;

    private float maxHealthScaleX;
    private float maxResourceScaleX;

    private float scaleSpeed = 0.25f;

    #region PRIVATE_METHODS
    void Awake() {
        maxHealthScaleX = healthBar.localScale.x;
        maxResourceScaleX = resourceBar.localScale.x;
    }

    void Start() {

    }

    private void HealthScaleComplete() {
        if (OnHealthScaleComplete != null) {
            OnHealthScaleComplete.Invoke();
        }
    }

    private void ResourceScaleComplete() {
        if (OnResourceScaleComplete != null) {
            OnResourceScaleComplete.Invoke();
        }
    }

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    public void SetHealthPercentage(float percent) {
        healthBar.DOScaleX(maxHealthScaleX * percent, scaleSpeed).OnComplete(HealthScaleComplete);
    }

    public void SetResourcePercentage(float percent) {
        resourceBar.DOScaleX(maxResourceScaleX * percent, scaleSpeed).OnComplete(ResourceScaleComplete);
    }

    public void SetSortingLayer(string layerName) {
        barsBG.GetComponent<SpriteRenderer>().sortingLayerName = layerName;
        healthBar.GetComponent<SpriteRenderer>().sortingLayerName = layerName;
        resourceBar.GetComponent<SpriteRenderer>().sortingLayerName = layerName;
    }

    #endregion PUBLIC_METHODS

    void Update() {

    }
}
