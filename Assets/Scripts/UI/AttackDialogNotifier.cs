﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;

public class AttackDialogNotifier : MonoBehaviour {

    private static AttackDialogNotifier instance;

    private Text dialogText;
    private RectTransform rectTransform;

    private WaitForSeconds lifetimeWait;
    private float lifetime = 0.65f;
    private float animationSpeed = 0.2f;
    private bool showing = false;

	#region PRIVATE_METHODS
	void Awake() {
        instance = this;
        rectTransform = this.GetComponent<RectTransform>();
        dialogText = this.GetComponent<Text>();
	}
	
	void Start () {
        lifetimeWait = new WaitForSeconds(lifetime);
	}

    private void OnScaleDownComplete() {
        showing = false;
    }

    private IEnumerator ProcessLifetime() {
        yield return lifetimeWait;

        rectTransform.DOScale(0, animationSpeed).OnComplete(OnScaleDownComplete);
    }

	#endregion PRIVATE_METHODS

	#region PUBLIC_METHODS

    public static void Show(Vector2 pos, string text) {
        instance.showing = true;
        instance.rectTransform.anchoredPosition = pos;
        
        instance.dialogText.text = text;
        instance.rectTransform.DOScale(1, instance.animationSpeed);

        instance.StartCoroutine(instance.ProcessLifetime());
    }

    public static IEnumerator WaitForComplete() {
        while (instance.showing) {
            yield return null;
        }
    }

	#endregion PUBLIC_METHODS

	void Update () {
	
	}
}
