﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class Dialogger : MonoBehaviour {

    private float arrowBounceAmount = 10;
    private float arrowStartY = 0;
    private int blockIndex = 0;
    private int lineIndex = 0;
    private DialogModule currentDialogModule;
    private DialogBlock currentDialogBlock;
    private DialogLine currentDialogLine;

    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;

    public Text speakerNameText;
    public TextTypewriter dialogText;
    public GameObject overlayObject;
    public RectTransform nextArrow;

    [HideInInspector]
    public UnityEvent OnDialogComplete;

	#region PRIVATE_METHODS
	void Awake() {
        rectTransform = overlayObject.GetComponent<RectTransform>();
        canvasGroup = this.GetComponent<CanvasGroup>();
        arrowStartY = nextArrow.anchoredPosition.y;
        SetVisible(false);
	}
	
	void Start () {
        
	}

    private void CheckArrow() {
        if ((lineIndex == currentDialogBlock.dialogList.Count - 1) && (blockIndex == currentDialogModule.dialogBlocks.Count - 1)) {
            SetArrowVisible(false);
        }
    }

    private void WriteText() {
        speakerNameText.text = currentDialogBlock.speakerName;
        dialogText.SetText(currentDialogLine.lineOfDialog);
    }

    private void DialogComplete() {
        lineIndex++;
        if (lineIndex == currentDialogBlock.dialogList.Count) {
            lineIndex = 0;
            blockIndex++;
            if (blockIndex == currentDialogModule.dialogBlocks.Count) {
                blockIndex = 0;
                SetVisible(false);
                if (OnDialogComplete != null) {
                    OnDialogComplete.Invoke();
                }
                return;
            }
            currentDialogBlock = currentDialogModule.dialogBlocks[blockIndex];
        }
        currentDialogLine = currentDialogBlock.dialogList[lineIndex];
        CheckArrow();
        WriteText();
    }

    private void SetVisible(bool value) {
        canvasGroup.alpha = value.AsInt();
        canvasGroup.interactable = value;
        canvasGroup.blocksRaycasts = value;
        overlayObject.SetActive(value);
        SetArrowVisible(value);
    }

    private void SetArrowVisible(bool value) {
        if (value) {
            nextArrow.gameObject.SetActive(value);
            nextArrow.DOAnchorPosY(arrowStartY + arrowBounceAmount, 0.5f).SetLoops(-1, LoopType.Yoyo);
        }
        else {
            nextArrow.DOKill();
            nextArrow.gameObject.SetActive(value);
        }
    }

	#endregion PRIVATE_METHODS

	#region PUBLIC_METHODS

    public void ShowText(DialogModule module) {
        SetVisible(true);
        currentDialogModule = module;
        currentDialogBlock = currentDialogModule.dialogBlocks[blockIndex];
        currentDialogLine = currentDialogBlock.dialogList[lineIndex];
        WriteText();
    }

    public void AdvanceText() {
        DialogComplete();
    }

	#endregion PUBLIC_METHODS

}
