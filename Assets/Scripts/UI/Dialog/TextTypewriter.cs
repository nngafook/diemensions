﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class TextTypewriter : MonoBehaviour {

    private string textToDisplay;
    private bool isTyping = false;

    private Text displayedTextField;

    public Button dialogTextButton;

    public bool IsTyping { get { return isTyping; } }

	#region PRIVATE_METHODS
	void Awake() {
        displayedTextField = this.GetComponent<Text>();
	}

    private IEnumerator TypewriteIt() {
        isTyping = true;
        //dialogTextButton.interactable = false;

        WaitForSeconds wait = new WaitForSeconds(0.25f);

        for (int i = 0; i < (textToDisplay.Length + 1); i++) {
            displayedTextField.text = textToDisplay.Substring(0, i);
            yield return null;
        }

        yield return wait;

        isTyping = false;
        //dialogTextButton.interactable = true;
    }

	#endregion PRIVATE_METHODS

	#region PUBLIC_METHODS

    public void SetText(string text) {
        textToDisplay = text;
        StartCoroutine("TypewriteIt");
    }

	#endregion PUBLIC_METHODS

	void Update () {
	
	}
}
