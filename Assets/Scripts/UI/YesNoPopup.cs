﻿using UnityEngine;
using UnityEngine.UI;

public class YesNoPopup : MonoBehaviour {

    private static YesNoPopup instance = null;

    private CanvasGroup canvasGroup;

    public delegate void OnResultEvent(bool result);
    public event OnResultEvent OnResult;

    public Button yesButton;
    public Button noButton;
    public Text popupMessage;

    #region PRIVATE_METHODS
    void Awake() {
        instance = this;

        canvasGroup = this.GetComponent<CanvasGroup>();
    }

    void Start() {
        SetupButtonEvents();
    }

    private void SetupButtonEvents() {
        yesButton.onClick.AddListener(OnYesClicked);
        noButton.onClick.AddListener(OnNoClicked);
    }

    private void OnYesClicked() {
        if (OnResult != null) {
            OnResult(true);
        }
        Close();
    }

    private void OnNoClicked() {
        if (OnResult != null) {
            OnResult(false);
        }
        Close();
    }

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS
    public static void Show(string message, OnResultEvent callback) {
        instance.popupMessage.text = message.ToUpper();
        instance.OnResult += callback;
        instance.canvasGroup.alpha = 1;
        instance.canvasGroup.interactable = true;
        instance.canvasGroup.blocksRaycasts = true;
    }

    public static void Close() {
        instance.canvasGroup.alpha = 0;
        instance.canvasGroup.interactable = false;
        instance.canvasGroup.blocksRaycasts = false;
    }

    public static void RemoveEventListener(OnResultEvent callback) {
        instance.OnResult -= callback;
    }
    #endregion PUBLIC_METHODS

    void Update() {

    }
}
