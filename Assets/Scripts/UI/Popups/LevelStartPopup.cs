﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class LevelStartPopup : MonoBehaviour {

    private CanvasGroup canvasGroup;

	#region PRIVATE_METHODS
	void Awake() {
        canvasGroup = this.GetComponent<CanvasGroup>();
	}
	
	void Start () {
        canvasGroup.alpha = 1;
        canvasGroup.blocksRaycasts = true;
        canvasGroup.interactable = true;
	}

    private void OnCloseComplete() {
        ScreenFader.SnapFromBlack();
        MSN.Instance.TriggerEvent(EventName.START_LEVEL);
    }

	#endregion PRIVATE_METHODS

	#region PUBLIC_METHODS

    public void StartLevel() {
        Close();
    }

    public void Close() {
        this.GetComponent<RectTransform>().DOScale(0, 0.1f).SetEase(Ease.InBack).OnComplete(OnCloseComplete);
    }

	#endregion PUBLIC_METHODS

	void Update () {
	
	}
}
