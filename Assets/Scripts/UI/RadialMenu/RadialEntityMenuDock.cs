﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class RadialEntityMenuDock : MonoBehaviour {

    private float animationSpeed = 0.15f;

    private RectTransform rectTransform;

    #region PRIVATE_METHODS
    void Awake() {
        rectTransform = this.GetComponent<RectTransform>();
    }

    void Start() {

    }

    private void OpenComplete() {
        MSN.Instance.TriggerEvent(EventName.RADIAL_DOCK_OPEN_COMPLETE);
    }

    private void CloseComplete() {
        MSN.Instance.TriggerEvent(EventName.RADIAL_DOCK_CLOSE_COMPLETE);
    }

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    public void Open() {
        rectTransform.DOScale(1, animationSpeed).OnComplete(OpenComplete);
    }

    public void Close() {
        rectTransform.DOScale(0, animationSpeed).OnComplete(CloseComplete);
    }

    public void OpenForEditor() {
        this.GetComponent<RectTransform>().localScale = Vector3.one;
    }

    public void CloseForEditor() {
        this.GetComponent<RectTransform>().localScale = Vector3.zero;
    }

    #endregion PUBLIC_METHODS

}
