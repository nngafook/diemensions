﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;

public class RadialEntityMenuButton : MonoBehaviour {

    private RadialEntityMenu menu;
    private RectTransform rectTransform;

    private bool showing = false;
    private float animationTime = 0.075f;
    private float shakeDuration = 0.075f;
    private float shakeStrength = 5f;
    private int vibratoStrength = 50;

    private bool isSelected = false;
    public bool IsSelected { get { return isSelected; } }

    [SerializeField]
    private CommandType commandType = CommandType.MOVE;
    public CommandType CommandType { get { return commandType; } }

    public Vector2 endPos;
    public Button buttonComponent;

	#region PRIVATE_METHODS
	void Awake() {
        rectTransform = this.GetComponent<RectTransform>();
	}
	
	void Start () {
	
	}

    private void OpenComplete() {
        showing = true;
        Shake();
    }

    private void Shake() {
        rectTransform.DOShakeAnchorPos(shakeDuration, shakeStrength, vibrato:vibratoStrength, snapping: true);
    }

	#endregion PRIVATE_METHODS

	#region PUBLIC_METHODS

    public void OnPressed() {
        if (showing) {
            Shake();
            isSelected = !isSelected;
            menu.ButtonPressed(this);
        }
    }

    public void SetSelected(bool value) {
        isSelected = value;
    }

    public void SetInteractable(bool value) {
        buttonComponent.interactable = value;
    }

    public void RegisterToMenu(RadialEntityMenu radialMenu) {
        menu = radialMenu;
    }

    public void OpenForEditor() {
        this.GetComponent<RectTransform>().anchoredPosition = endPos;
        this.GetComponent<RectTransform>().localScale = Vector3.one;
    }

    public void CloseForEditor() {
        this.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        this.GetComponent<RectTransform>().localScale = Vector3.zero;
    }

    public void SaveValuesForEditor() {
        endPos = this.GetComponent<RectTransform>().anchoredPosition;
    }

    public void AnimateOpen() {
        rectTransform.DOAnchorPos(endPos, animationTime);
        rectTransform.DOScale(Vector3.one, animationTime).SetEase(Ease.OutBounce).OnComplete(OpenComplete);
    }

    public void AnimateClose() {
        showing = false;
        isSelected = false;
        rectTransform.DOAnchorPos(Vector2.zero, animationTime);
        rectTransform.DOScale(Vector3.zero, animationTime).SetEase(Ease.InBounce);
    }

    public void Minimize() {
        rectTransform.DOAnchorPos(Vector2.zero, animationTime);
    }

	#endregion PUBLIC_METHODS

	void Update () {
	
	}
}
