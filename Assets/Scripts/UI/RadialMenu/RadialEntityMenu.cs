﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class RadialEntityMenu : MonoBehaviour {

    private const string ENTITIES_SORTING_LAYER = "Entities";
    private const string ENTITY_FOCUSED_SORTING_LAYER = "EntityFocused";
    private const int BACKGROUND_SORTING = 10;
    private const int FOREGROUND_SORTING = 30;

    public Canvas rootCanvas;
    public RadialEntityMenuDock radialEntityMenuDock;

    [Header("Radial Buttons")]
    public RadialEntityMenuButton[] menuButtonsList;

    private RadialEntityMenuButton selectedButton = null;
    public RadialEntityMenuButton SelectedButton { get { return selectedButton; } }

    // This is the entity that is waiting to have the radial menu shown on, due to animation timings.
    private Entity queuedEntity;

    private WaitForSeconds shortWait = new WaitForSeconds(0.05f);
    private WaitForFixedUpdate fixedUpdateWait = new WaitForFixedUpdate();

    private Entity entityOwner;
    //private float animateSpeed = 0.2f;
    private int menuDepth = 0;
    // Minimized != closed. When minimized, the back button is showing
    private bool isMinimized = false;
    private bool isAnimating = false;

    //private Vector2 endSize = new Vector2(200, 200);

	#region PRIVATE_METHODS
	void Awake() {
        
	}
	
	void Start () {
        RegisterButtons();
	}

    private void RegisterButtons() {
        for (int i = 0; i < menuButtonsList.Length; i++) {
            menuButtonsList[i].RegisterToMenu(this);
        }
    }

    private void UpdateButtonsBasedOnEntityStatus() {
        ButtonByCommandType(CommandType.MOVE).SetInteractable(!entityOwner.entityStatus.hasMoved);
        ButtonByCommandType(CommandType.ATTACK).SetInteractable(!entityOwner.entityStatus.hasAttacked);
        ButtonByCommandType(CommandType.SKILL).SetInteractable(!entityOwner.entityStatus.hasAttacked);
    }

    private RadialEntityMenuButton ButtonByCommandType(CommandType type) {
        RadialEntityMenuButton rVal = null;
        for (int i = 0; i < menuButtonsList.Length; i++) {
            if (menuButtonsList[i].CommandType == type) {
                rVal = menuButtonsList[i];
                break;
            }
        }
        return rVal;
    }

    private void OpenMenu() {
        menuDepth = 1;
        UpdateButtonsBasedOnEntityStatus();
        radialEntityMenuDock.Open();
        MSN.Instance.StartListening(EventName.RADIAL_DOCK_OPEN_COMPLETE, OnDockOpenComplete);
        //MSN.Instance.StartListening(EventName.CAMERA_PAN_TO_POSITION_COMPLETE, OnPanToEntityPositionComplete);
        //CameraManager.Instance.PanTo(entityOwner.Position, 0.25f);
    }

    private void OnPanToEntityPositionComplete() {
        MSN.Instance.StopListening(EventName.CAMERA_PAN_TO_POSITION_COMPLETE, OnPanToEntityPositionComplete);
        radialEntityMenuDock.Open();
        MSN.Instance.StartListening(EventName.RADIAL_DOCK_OPEN_COMPLETE, OnDockOpenComplete);
    }

    private void OnDockOpenComplete() {
        MSN.Instance.StopListening(EventName.RADIAL_DOCK_OPEN_COMPLETE, OnDockOpenComplete);
        StartCoroutine(AnimateButtonsOpen());
    }

    private void SetSelectedButtonSelected(bool value, bool nullButton = false) {
        if (selectedButton != null) {
            selectedButton.SetSelected(value);
            if ((nullButton) && (!value)) {
                selectedButton = null;
            }
        }
    }

    /// <summary>
    /// Gets called if the menu tried to open during a close
    /// </summary>
    private void EventDrivenOpenMenu() {
        MSN.Instance.StopListening(EventName.ENTITY_MENU_CLOSE_COMPLETE, EventDrivenOpenMenu);
        OpenOnEntity(queuedEntity);
        //StartCoroutine(DockToEntity());
    }

    private IEnumerator AnimateButtonsOpen() {
        isAnimating = true;
        //this.GetComponent<RectTransform>().DOSizeDelta(endSize, animateSpeed).SetEase(Ease.OutBounce);
        for (int i = 0; i < menuButtonsList.Length; i++) {
            menuButtonsList[i].AnimateOpen();
            yield return shortWait;
        }
        isAnimating = false;
        rootCanvas.sortingOrder = BACKGROUND_SORTING;
        rootCanvas.sortingLayerName = ENTITY_FOCUSED_SORTING_LAYER;
        MSN.Instance.TriggerEvent(EventName.ENTITY_MENU_OPEN_COMPLETE);
    }

    //private void SetCanvasSortingLayer(int layerOrder) {
    //    rootCanvas.sortingOrder = layerOrder;
    //    if (layerOrder == BACKGROUND_SORTING) {
    //        rootCanvas.sortingLayerName = ENTITIES_SORTING_LAYER;
    //    }
    //    else if (layerOrder == FOREGROUND_SORTING) {
    //        rootCanvas.sortingLayerName = ENTITY_FOCUSED_SORTING_LAYER;
    //    }
    //}

    private IEnumerator AnimateButtonsClose() {
        isAnimating = true;
        //this.GetComponent<RectTransform>().DOSizeDelta(Vector2.zero, animateSpeed).SetEase(Ease.InBounce);
        for (int i = menuButtonsList.Length - 1; i >= 0; i--) {
            menuButtonsList[i].AnimateClose();
            yield return shortWait;
        }
        isAnimating = false;
        rootCanvas.sortingOrder = BACKGROUND_SORTING;
        rootCanvas.sortingLayerName = ENTITIES_SORTING_LAYER;
        MSN.Instance.TriggerEvent(EventName.ENTITY_MENU_CLOSE_COMPLETE);
    }

    private IEnumerator AnimateButtonsMinimize() {
        isAnimating = true;
        //this.GetComponent<RectTransform>().DOSizeDelta(Vector2.zero, animateSpeed).SetEase(Ease.OutBounce);
        for (int i = menuButtonsList.Length - 1; i > 0; i--) {
            menuButtonsList[i].AnimateClose();
            yield return shortWait;
        }
        menuButtonsList[0].Minimize();
        isAnimating = false;
        MSN.Instance.TriggerEvent(EventName.ENTITY_MENU_MINIMIZE_COMPLETE);
    }

	#endregion PRIVATE_METHODS

	#region PUBLIC_METHODS

    #region COMMAND_CALLBACKS
    public void ButtonPressed(RadialEntityMenuButton btn) {
        if (!isAnimating) {
            if (isMinimized) {
                if (btn.CommandType == CommandType.BACK) {
                    isMinimized = false;
                    SetSelectedButtonSelected(false);
                    EntityManager.Instance.EntityButtonPressed(selectedButton.CommandType, selectedButton.IsSelected);
                    OpenMenu();
                    selectedButton = null;
                }
            }
            else {
                selectedButton = btn;
                EntityManager.Instance.EntityButtonPressed(selectedButton.CommandType, selectedButton.IsSelected);
            }
        }
    }
    #endregion COMMAND_CALLBACKS

    public void Minimize() {
        isMinimized = true;
        radialEntityMenuDock.Close();
        StartCoroutine(AnimateButtonsMinimize());
        rootCanvas.sortingOrder = FOREGROUND_SORTING;
    }

    public void OpenForEditor() {
        for (int i = 0; i < menuButtonsList.Length; i++) {
            menuButtonsList[i].OpenForEditor();
        }
        radialEntityMenuDock.OpenForEditor();
        //this.GetComponent<RectTransform>().sizeDelta = endSize;
    }

    public void CloseForEditor() {
        for (int i = 0; i < menuButtonsList.Length; i++) {
            menuButtonsList[i].CloseForEditor();
        }
        radialEntityMenuDock.CloseForEditor();
        //this.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
    }

    public void SaveValuesForEditor() {
        for (int i = 0; i < menuButtonsList.Length; i++) {
            menuButtonsList[i].SaveValuesForEditor();
        }
        //endSize = this.GetComponent<RectTransform>().sizeDelta;
    }

    public void SetVisible(bool value) {
        CanvasGroup cg = rootCanvas.GetComponent<CanvasGroup>();
        cg.alpha = value.AsInt();
        cg.blocksRaycasts = value;
        cg.interactable = value;
    }

    public void OpenOnEntity(Entity entity) {
        if (!isAnimating) {
            entityOwner = entity;
            rootCanvas.sortingOrder = BACKGROUND_SORTING;
            rootCanvas.sortingLayerName = ENTITY_FOCUSED_SORTING_LAYER;
            OpenMenu();
            rootCanvas.GetComponent<RectTransform>().anchoredPosition = entity.Position;
            //StartCoroutine(DockToEntity());
        }
        else {
            queuedEntity = entity;
            MSN.Instance.StartListening(EventName.ENTITY_MENU_CLOSE_COMPLETE, EventDrivenOpenMenu);
        }
    }

    private IEnumerator DockToEntity() {
        Vector3 screenPos = Vector3.zero;
        Camera mainCamera = Camera.main;
        while (menuDepth == 1) {
            screenPos = mainCamera.WorldToViewportPoint(entityOwner.Position);
            screenPos.x *= rootCanvas.pixelRect.width;
            screenPos.y *= rootCanvas.pixelRect.height;
            transform.position = screenPos;
            yield return fixedUpdateWait;
        }
    }

    public void CloseMenu() {
        menuDepth = 0;
        if (isMinimized) {
            isMinimized = false;
            menuButtonsList[0].AnimateClose();
        }
        else {
            radialEntityMenuDock.Close();
            StartCoroutine(AnimateButtonsClose());
        }
        SetSelectedButtonSelected(false, true);
    }

    //public void SetButtonEnabled(CommandType type, bool value) {
    //    RadialEntityMenuButton button = ButtonByCommandType(type);
    //    button.SetInteractable(value);
    //}

    #endregion PUBLIC_METHODS

    void Update () {
        //if (Input.GetKeyUp(KeyCode.Alpha1)) {
        //    OpenMenu();
        //}
	}
}
