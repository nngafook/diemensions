﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InventoryTextItem : MonoBehaviour {

    private string itemID;
    private int itemAmount;
    public string ID { get { return itemID; } }
    public int Amount { get { return itemAmount; } }

    private Text textField = null;

    void Awake() {
        textField = this.GetComponent<Text>();
    }

    private void SetText() {
        textField.text = (Utility.IDToName(itemID) + (" x" + itemAmount).Colored(Color.green));
    }

    public void SetInfo(string id, int amount) {
        itemID = id;
        itemAmount = amount;

        SetText();
    }

    public void AddAmount(int amount) {
        itemAmount += amount;
        SetText();
    }

}
