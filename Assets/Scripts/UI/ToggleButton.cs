﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
[RequireComponent(typeof(EventTrigger))]
public class ToggleButton : MonoBehaviour {

    public delegate void ClickedEvent(ToggleButton btn);
    public event ClickedEvent OnClickedEvent;

    protected Toggle toggleComponent;
    protected EventTrigger eventTrigger;

    public bool IsSelected { get { return toggleComponent.isOn; } }

    #region PRIVATE_METHODS
    protected virtual void Awake() {
        toggleComponent = this.GetComponent<Toggle>();
        eventTrigger = this.GetComponent<EventTrigger>();
    }

    protected virtual void Start() {
        AddEventTrigger(OnPointerClick, EventTriggerType.PointerClick);
    }

    private void AddEventTrigger(UnityAction<BaseEventData> action, EventTriggerType triggerType) {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = triggerType;
        entry.callback = new EventTrigger.TriggerEvent();
        entry.callback.AddListener(action);
        eventTrigger.triggers.Add(entry);
    }

    private void OnPointerClick(BaseEventData baseData) {
        if (OnClickedEvent != null) {
            OnClickedEvent(this);
        }
    }
    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS
    public void AddValueChangedEvent(UnityAction<bool> callback) {
        toggleComponent.onValueChanged.AddListener(callback);
    }

    public void RemoveValueChangedEvent(UnityAction<bool> callback) {
        toggleComponent.onValueChanged.RemoveListener(callback);
    }

    public void SetButtonEnabled(bool value) {
        if ((!value) && (toggleComponent.isOn)) {
            toggleComponent.isOn = false;
        }
        toggleComponent.interactable = value;
    }

    public void SetToggle(bool value) {
        toggleComponent.isOn = value;
    }

    #endregion PUBLIC_METHODS

    void Update() {

    }
}
