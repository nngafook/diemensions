﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class FloatingDamageNumber : MonoBehaviour {

    [HideInInspector]
    public UnityEvent OnShowComplete;

    private TextMesh textMesh;
    private float moveTotal = 100;
    private float moveDuration = 0.5f;
    private float lifetime = 1.0f;

    #region PRIVATE_METHODS
    void Awake() {
        textMesh = this.GetComponent<TextMesh>();
    }

    private void ScaleComplete() {
        OnShowComplete.Invoke();
        Destroy(this.gameObject);
    }
    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS
    public void SetValue(int value, bool isAHeal, bool isCrit) {
        if (isAHeal) {
            textMesh.text = value.ToString().Colored(Color.green);
        }
        else {
            textMesh.text = value.ToString();
        }
        //if (isCrit) {
        //    textMesh.text = textMesh.text.Bold();
        //}

        if (isCrit) {
            textMesh.fontStyle = FontStyle.Bold;
            textMesh.fontSize += 10;
            transform.DOMoveY(transform.position.y + moveTotal, 0.15f);
            transform.DOPunchScale(new Vector3(2f, 2f, 2f), 0.25f).OnComplete(CritPunchTweenComplete);
        }
        else {
            transform.DOMoveY(transform.position.y + moveTotal, moveDuration);
            transform.DOScale(0, lifetime).SetEase(Ease.InElastic).OnComplete(ScaleComplete);
        }
    }

    private void CritPunchTweenComplete() {
        //transform.DOMoveY(transform.position.y + moveTotal, moveDuration);
        transform.DOScale(0, (lifetime / 2)).SetEase(Ease.InElastic).OnComplete(ScaleComplete);
    }
    #endregion PUBLIC_METHODS

    void Update() {

    }
}
