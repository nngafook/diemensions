﻿using UnityEngine;
using UnityEngine.Events;

public class FloatingDamageNotifier : MonoBehaviour {

    [HideInInspector]
    public UnityEvent OnNotificationComplete;

    private static FloatingDamageNotifier instance = null;

    public GameObject floatingNumberPrefab;

    public static FloatingDamageNotifier Instance { get { return instance; } }

    #region PRIVATE_METHODS
    void Awake() {
        instance = this;
    }

    void Start() {

    }

    private void DamageNumberShowComplete() {
        OnNotificationComplete.Invoke();
    }
    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    public void PushNumber(Vector3 pos, int value, bool isAHeal, bool isCrit) {
        pos.z = -1;
        FloatingDamageNumber damageNumber = (Instantiate(floatingNumberPrefab, pos, Quaternion.identity) as GameObject).GetComponent<FloatingDamageNumber>();
        damageNumber.SetValue(value, isAHeal, isCrit);
        damageNumber.OnShowComplete.AddListener(DamageNumberShowComplete);
    }

    public void AddNotificationListener(UnityAction callback) {
        OnNotificationComplete.AddListener(callback);
    }

    public void RemoveNotificationListener(UnityAction callback) {
        OnNotificationComplete.RemoveListener(callback);
    }

    #endregion PUBLIC_METHODS

    void Update() {

    }
}
