﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class DragPanel : MonoBehaviour, IPointerDownHandler, IDragHandler {

    private Vector2 pointerOffset;
    private RectTransform canvasRectTransform;
    
    public RectTransform dragRectTransform;

    void Awake() {
        Canvas canvas = GetComponentInParent<Canvas>();
        if (canvas != null) {
            canvasRectTransform = canvas.transform as RectTransform;
        }
    }

    public void OnPointerDown(PointerEventData data) {
        dragRectTransform.SetAsLastSibling();
        RectTransformUtility.ScreenPointToLocalPointInRectangle(dragRectTransform, data.position, data.pressEventCamera, out pointerOffset);
    }

    public void OnDrag(PointerEventData data) {
        if (dragRectTransform == null)
            return;

        Vector2 pointerPostion = ClampToWindow(data);
        Vector2 localPointerPosition;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRectTransform, pointerPostion, data.pressEventCamera, out localPointerPosition)) {
            dragRectTransform.localPosition = localPointerPosition - pointerOffset;
        }
    }

    Vector2 ClampToWindow(PointerEventData data) {
        Vector2 rawPointerPosition = data.position;

        Vector3[] canvasCorners = new Vector3[4];
        canvasRectTransform.GetWorldCorners(canvasCorners);

        float clampedX = Mathf.Clamp(rawPointerPosition.x, canvasCorners[0].x, canvasCorners[2].x);
        float clampedY = Mathf.Clamp(rawPointerPosition.y, canvasCorners[0].y, canvasCorners[2].y);

        Vector2 newPointerPosition = new Vector2(clampedX, clampedY);
        return newPointerPosition;
    }
}