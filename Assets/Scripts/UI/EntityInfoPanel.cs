﻿using DG.Tweening;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class EntityInfoPanel : MonoBehaviour {

    private RectTransform rectTransform;
    private Vector2 showingPosition = new Vector2(-5, 5);
    private Vector2 hiddenPosition = new Vector2(-5, -200);

    private float animationSpeed = 0.1f;
    private bool playerPhase = false;
    private bool isShowing = true;

    private string entityNameTextPrefix = "NAME: ";
    private string entityHealthTextPrefix = "HEALTH: ";
    private string entityResourceTextPrefix = "RESOURCE: ";

    public Text entityNameTextField;
    public Text entityHealthTextField;
    public Text entityResourceTextField;

    #region PRIVATE_METHODS
    void Awake() {
        rectTransform = this.GetComponent<RectTransform>();
        showingPosition = rectTransform.anchoredPosition;
        hiddenPosition = showingPosition.AddY(-200);
        GameManager.Instance.EnterPhaseEvent.AddListener(OnGamePhaseEntered);
    }

    void Start() {
        Debug.LogWarning("This class is horribly optimized cause it's manipulating strings in an 'update'".Bold());
    }

    private void OnGamePhaseEntered() {
        playerPhase = (GameManager.Instance.CurrentGamePhase == GamePhase.PLAYER);

        if (playerPhase) {
            StartCoroutine("FauxUpdate");
        }
        else {
            StopCoroutine("FauxUpdate");
            Hide();
        }
    }

    private void Show() {
        if (!isShowing) {
            isShowing = true;
            rectTransform.DOAnchorPos(showingPosition, animationSpeed).SetEase(Ease.OutBack);
        }
    }

    private void Hide() {
        if (isShowing) {
            isShowing = false;
            rectTransform.DOAnchorPos(hiddenPosition, animationSpeed).SetEase(Ease.OutBack);
        }
    }

    private IEnumerator FauxUpdate() {
        Entity selectedEntity = null;

        while (playerPhase) {
            selectedEntity = EntityManager.Instance.SelectedEntity;

            if (selectedEntity) {
                Show();
                SetName(selectedEntity.Properties.name.ToUpper());
                SetHealthText(selectedEntity.Status.currentHealth.ToString().ToUpper(), selectedEntity.Properties.maxHealth.ToString().ToUpper());
                SetResourceText(selectedEntity.Status.currentResource.ToString().ToUpper(), selectedEntity.Properties.maxResource.ToString().ToUpper());
            }
            else {
                Hide();
                SetName(string.Empty);
                SetHealthText(string.Empty, string.Empty);
                SetResourceText(string.Empty, string.Empty);
            }

            yield return new WaitForSeconds(0.05f);
        }
    }

    private void SetName(string name) {
        entityNameTextField.text = entityNameTextPrefix + name;
    }

    private void SetHealthText(string currentHealth, string maxHealth) {
        entityHealthTextField.text = entityHealthTextPrefix + "<color=#FF0000FF>" + currentHealth + " / " + maxHealth + "</color>";
    }

    private void SetResourceText(string currentResource, string maxResource) {
        entityResourceTextField.text = entityResourceTextPrefix + "<color=#336699FF>" + currentResource + " / " + maxResource + "</color>";
    }

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    #endregion PUBLIC_METHODS

    void Update() {

    }
}
