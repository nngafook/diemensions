﻿using UnityEngine;
using System.Collections;

public class CameraZoomController : MonoBehaviour {

    private Camera mainCamera;
    private float wheelZoomSpeed = 50f;
    private float pinchZoomSpeed = 1f;

    public float minZoom = 1080;
    public float maxZoom = 540;

    #region PRIVATE_METHODS
    void Awake() {
        mainCamera = this.GetComponent<Camera>();
    }

    void Start() {

    }
    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    private void OnScrollWheel() {
        // back
        if (Input.GetAxis("Mouse ScrollWheel") < 0) {
            float value = mainCamera.orthographicSize + wheelZoomSpeed;
          
            value = Mathf.Clamp(value, maxZoom, minZoom);
            mainCamera.orthographicSize = value;
        }
        // forward
        if (Input.GetAxis("Mouse ScrollWheel") > 0) {
            float value = mainCamera.orthographicSize - wheelZoomSpeed;
            value = Mathf.Clamp(value, maxZoom, minZoom);
            mainCamera.orthographicSize = value;
        }
    }

    private void OnPinch() {
        // If there are two touches on the device...
        if (Input.touchCount == 2) {
            // Store both touches.
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            // ... change the orthographic size based on the change in distance between the touches.
            mainCamera.orthographicSize += deltaMagnitudeDiff * pinchZoomSpeed;

            // Make sure the orthographic size never drops below zero.
            //camera.orthographicSize = Mathf.Max(camera.orthographicSize, 0.1f);

            mainCamera.orthographicSize = Mathf.Clamp(mainCamera.orthographicSize, maxZoom, minZoom);
        }
    }

    #endregion PUBLIC_METHODS

    void Update() {
        OnScrollWheel();
        OnPinch();
    }
}
