﻿using UnityEngine;
using UnityEngine.EventSystems;

public class CameraDragController : MonoBehaviour {

    private int fingerID = -1;
    private bool isDragging = false;
    private Vector3 dragStartPosition;
    private Vector3 targetPos;

    [Tooltip("If true, the component will call it's drag logic in it's own LateUpdate. If left false, ProcessDrag() must get called from somewhere else.")]
    public bool processOwnDrag = false;
    [Tooltip("Width of the bounds to keep the camera in. Probably only needed if processOwnDrag is true")]
    public int mapWidth = 1920;
    [Tooltip("Height of the bounds to keep the camera in. Probably only needed if processOwnDrag is true")]
    public int mapHeight = 1080;

    public bool IsDragging { get { return isDragging; } }

    void Awake() {
        #if !UNITY_EDITOR
            fingerID = 0;
        #endif
    }



    private void ProcessInput() {
        if (Input.GetMouseButtonDown(0)) {
            BeginDrag();
        }

        if (Input.GetMouseButtonUp(0)) {
            EndDrag();
        }
    }

    public bool BeginDrag(int width = 0, int height = 0) {
        bool rVal = false;
        if ((EventSystem.current == null) || ((EventSystem.current != null) && (!EventSystem.current.IsPointerOverGameObject(fingerID)))) {
            isDragging = true;
            mapWidth = (width == 0) ? mapWidth : width;
            mapHeight = (height == 0) ? mapHeight : height;
            dragStartPosition = Utility.MouseWorldPosition();
            rVal = true;
        }
        return rVal;
    }

    public void EndDrag() {
        isDragging = false;
    }

    public void ProcessDrag() {
        if (isDragging) {
            targetPos = transform.position + (dragStartPosition - Utility.MouseWorldPosition());
            targetPos.x = Mathf.Clamp(targetPos.x, -mapWidth / 2, mapWidth / 2);
            targetPos.y = Mathf.Clamp(targetPos.y, -mapHeight / 2, mapHeight / 2);
            targetPos.z = transform.position.z;
            transform.position = targetPos;
        }
    }

    void LateUpdate() {
        if (processOwnDrag) {
            ProcessInput();
            ProcessDrag();
        }
    }

}
