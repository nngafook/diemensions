﻿using DG.Tweening;
using UnityEngine;

public class Projectile : AttackView {

    #region PRIVATE_METHODS


    protected override void AttackComplete() {
        base.AttackComplete();
        fastPool = FastPoolManager.GetPool((int)(attackVFXType), null, false);
        FastPoolManager.GetPool((int)(attackLandedVFXType), null, false).FastInstantiate(transform.position, Quaternion.identity);
        fastPool.FastDestroy(this.gameObject);
    }

    public override void Invoke() {
        base.Invoke();
        transform.DOMove(targetPosition, 0.5f).SetEase(Ease.Linear).OnComplete(AttackComplete);
    }
    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS
    public override void SetTargetPosition(Vector3 pos) {
        base.SetTargetPosition(pos);
        Invoke();
    }

    public override void SetTargetPosition(float x, float y) {
        base.SetTargetPosition(x, y);
        Vector3 pos = new Vector3(x, y, 0);
        targetPosition = pos;
        Invoke();
    }
    #endregion PUBLIC_METHODS

    void Update() {

    }
}
