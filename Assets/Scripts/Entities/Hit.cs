﻿using UnityEngine;

public class Hit : AttackView {

    #region PRIVATE_METHODS

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS
    protected override void Destroyed() {
        base.Destroyed();
        base.AttackComplete();
    }

    public override void Invoke(Vector3 pos) {
        base.Invoke(pos);
        //fastPool = FastPoolManager.GetPool((int)(attackVFXType), null, false);
    }
    #endregion PUBLIC_METHODS

    void Update() {

    }
}
