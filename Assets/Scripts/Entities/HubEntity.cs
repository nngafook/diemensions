﻿using DG.Tweening;
using System.Collections;
using UnityEngine;

public class HubEntity : Entity {

    //private List<Tile> currentPath = new List<Tile>();
    private bool isWandering = true;

    #region PRIVATE_METHODS

    protected override void Start() {
        base.Start();
        HubManager.Instance.SetTileOccupied(true, this);
        if (entityPropertiesData.hasMobility) {
            StartCoroutine(GetPathAndGo());
        }
    }

    private IEnumerator GetPathAndGo() {
        yield return new WaitForSeconds(Random.Range(0.5f, 1.5f));
        if (isWandering) {
            movePath = HubManager.Instance.GetRandomPath(this.transform.position);

            for (int i = 0; i < movePath.Count; i++) {
                movePath[i].SetAttackRangeHighlight(true);
            }
            HubManager.Instance.SetTileOccupied(false, this);
            MoveToNextNode();
        }
    }

    protected override void MoveToNextNode() {
        base.MoveToNextNode();
        Vector3 targetPos = movePath[0].Position;
        transform.DOMove(targetPos, entityPropertiesData.movementSpeed).SetEase(Ease.Linear).OnComplete(MoveToNodeComplete);
    }

    protected override void MoveToNodeComplete() {
        base.MoveToNodeComplete();
        movePath[0].ClearHighlight();
        movePath.RemoveAt(0);
        if (movePath.Count > 0) {
            MoveToNextNode();
        }
        else {
            MoveComplete();
        }
    }

    protected override void MoveComplete() {
        base.MoveComplete();
        HubManager.Instance.SetTileOccupied(true, this);
        StartCoroutine(GetPathAndGo());
    }

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    #endregion PUBLIC_METHODS

    void Update() {
        //if (Input.GetKeyUp(KeyCode.Space)) {
        //    isWandering = !isWandering;
        //    if (entityPropertiesData.hasMobility) {
        //        StartCoroutine(GetPathAndGo());
        //    }
        //}
    }
}
