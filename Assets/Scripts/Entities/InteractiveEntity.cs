﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using System;
using Random = UnityEngine.Random;

public class InteractiveEntity : MonoBehaviour {

    private Entity entityComponent;

    public Entity EntityComponent { get { return entityComponent; } }

    private string reward;
    public string Reward { get { return reward; } }

	#region PRIVATE_METHODS
	void Awake() {
        entityComponent = this.GetComponent<Entity>();
	}
	
	void Start () {
        SetRandomTestReward();
	}

    private void SetRandomTestReward() {
        EquippableItemType type = (EquippableItemType)(Random.Range(0, Enum.GetNames(typeof(EquippableItemType)).Length - 1));
        reward = type.ToString();
    }

    private void OnScaleOutComplete() {
        Destroy(this.gameObject);
    }

	#endregion PRIVATE_METHODS

	#region PUBLIC_METHODS

    /// <summary>
    /// Is called when the model should be removed. Probably needs to be refactored later on, not really an applicable craftableName for things like doors and stuff.
    /// </summary>
    public void GetGot() {
        transform.DOScale(0, 1f).OnComplete(OnScaleOutComplete);
    }

	#endregion PUBLIC_METHODS

	void Update () {
	
	}
}
