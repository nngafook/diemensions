﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class LevelEntity : Entity {

    #region PRIVATE_METHODS
    protected override void Awake() {
        base.Awake();
        aiController = this.GetComponent<AIController>();

        aiController.SetEntityOwner(this);
        EntityManager.Instance.RegisterEntity(this);
        gameObject.name = gameObject.name + "(" + EntityManager.EntityFactoryID + ")";
    }

    protected override void Start() {
        base.Start();
        //FogManager.Instance.RevealCircle(transform.position, entityPropertiesData.visionRadius);

        MapManager.Instance.SetTileOccupied(this, true);
        if (animator != null) {
            animator.Play("Idle", -1, Random.Range(0.0f, 1.0f));
        }

        if (entityPropertiesData.entityType != EntityType.PLAYER) {
            GetNearbyPlayers();
            FaceMoveDirection(nearbyPlayerEntities[0].Position);
        }
    }

    private void OnMouseUpAsButton() {
        InputManager.Instance.EntityClickedAsButton(this);
    }

    private void SetInRangeOfPlayer() {
        GetNearbyPlayers();
        if (nearbyPlayerEntities.Count > 0) {
            entityStatus.isInRangeOfPlayerEntity = true;
        }
        else {
            entityStatus.isInRangeOfPlayerEntity = false;
        }
    }

    private void GetNearbyPlayers() {
        nearbyPlayerEntities.Clear();
        EntityManager.Instance.GetNearbyPlayerEntities(this, ref nearbyPlayerEntities);
    }

    protected override void MoveToNextNode() {
        Vector3 targetPos = movePath[0].Position;
        FaceMoveDirection(targetPos);
        transform.DOMove(targetPos, entityPropertiesData.movementSpeed).SetEase(Ease.Linear).OnComplete(MoveToNodeComplete);
    }

    protected override void FaceMoveDirection(Vector2 pos) {
        if (pos.x > Position.x) {
            if (spriteTransform) {
                spriteTransform.GetComponent<SpriteRenderer>().flipX = true;
            }
        }
        else if (pos.x < Position.x) {
            if (spriteTransform) {
                spriteTransform.GetComponent<SpriteRenderer>().flipX = false;
            }
        }
    }

    protected override void MoveToNodeComplete() {
        base.MoveToNodeComplete();
        movePath.RemoveAt(0);
        if (movePath.Count > 0) {
            MoveToNextNode();
        }
        else {
            MoveComplete();
        }
    }

    protected override void MoveComplete() {
        base.MoveComplete();
        Vector3 pos = transform.position;
        pos.z = Camera.main.transform.position.z;
        //FogManager.Instance.RevealCircle(transform.position, entityPropertiesData.visionRadius);
        MapManager.Instance.EntityLandedOnTile(this);
        //MapManager.Instance.SetTileOccupied(this, true);
        EntityManager.Instance.ShowEntityMenu();
        //MSN.Instance.StartListening(EventName.CAMERA_SNAP_TO_PLAYER_COMPLETE, OnSnapComplete);
        //CameraManager.Instance.SnapToPlayer();
        //CameraManager.Instance.PanTo(pos);
        GameManager.Instance.EntityMoving = false;
    }

    private int NegatedDamage(int damage, DamageType damageType) {
        int rVal = damage;
        switch (damageType) {
            case DamageType.PHYSICAL:
                rVal -= entityPropertiesData.physicalDefense.RandomInt;
                break;
            case DamageType.MAGICAL:
                rVal -= entityPropertiesData.magicDefense.RandomInt;
                break;
            case DamageType.NONE:
                break;
            default:
                break;
        }
        return rVal;
    }

    private void HealthScaleToZeroComplete() {
        statusBars.OnHealthScaleComplete.RemoveListener(HealthScaleToZeroComplete);
        EntityManager.Instance.KillEntity(this);
    }

    private void ResourceScaleComplete() {
        statusBars.OnResourceScaleComplete.RemoveListener(ResourceScaleComplete);
    }

    private IEnumerator FlashSelected() {
        Color baseColor = skeletonAnimation.Skeleton.GetColor();
        Color endColor = new Color(0.75f, 0.75f, 0.75f, 1);
        float t = 0;
        while (isSelected) {
            t = (Mathf.Sin(Time.time * 6f) + 1) / 2.0f;
            skeletonAnimation.Skeleton.SetColor(Color.Lerp(baseColor, endColor, t));
            yield return null;
        }
        skeletonAnimation.Skeleton.SetColor(baseColor);
    }

    private IEnumerator WaitForMoveConfirm(Tile tile) {
        while (!ConfirmActionPopup.DecisionMade) {
            yield return null;
        }

        if (ConfirmActionPopup.Result) {
            EntityManager.Instance.MoveSelectedEntityTo(tile);
        }
    }

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    public override void ResetCooldowns() {
        base.ResetCooldowns();
        Command command = null;
        for (int i = 0; i < attacks.Length; i++) {
            command = attacks[i];
            command.onCooldown = false;
        }
        entityStatus.hasMoved = false;
        entityStatus.hasAttacked = false;
    }

    public override void UpdateStatus() {
        base.UpdateStatus();
        switch (entityPropertiesData.entityType) {
            case EntityType.PLAYER:
                break;
            case EntityType.ENEMY:
                //entityStatus.isVisibleToThePlayer = (FogManager.Instance.GetTileFromPosition(transform.position).IsFullyRevealed);
                SetInRangeOfPlayer();
                break;
            case EntityType.BOSS:
                break;
            case EntityType.NEUTRAL:
                break;
            case EntityType.PROP:
                break;
            case EntityType.NONE:
                break;
            default:
                break;
        }
    }

    public override void TileClicked(Tile tile) {
        base.TileClicked(tile);
        //if ((FogManager.Instance.FogRevealedAt(Utility.MouseWorldPosition())) && (MapManager.Instance.IsMoveableTile(tile))) {
        if (MapManager.Instance.IsMoveableTile(tile)) {
            ConfirmActionPopup.Show();
            //CameraManager.Instance.PanToActiveEntity(0.25f);
            StartCoroutine(WaitForMoveConfirm(tile));
        }
    }

    public override bool MoveTo(Vector3 pos) {
        base.MoveTo(pos);
        if (!entityStatus.hasMoved) {
            movePath = MapManager.Instance.GetPathInRegion(this, Position, pos);
            if (movePath.Count > 0) {
                Debug.LogWarning("Entity HasMoved has been disabled here!!!".Bold().Sized(15).Colored(Color.red));
                entityStatus.hasMoved = (Cheater.Instance.WTFMode) ? false : true;
                GameManager.Instance.EntityMoving = true;
                EntityManager.Instance.CloseRadialMenu();
                MapManager.Instance.SetTileOccupied(this, false);
                //CameraManager.Instance.SetToFollow(this);
                MoveToNextNode();
            }
            else {
                Debug.LogWarning("Cannot find path".Bold().Colored(Color.cyan));
            }
            //transform.DOMove(pos, 1.0f).SetEase(Ease.OutQuad).OnComplete(MoveComplete);
        }
        return (movePath.Count > 0);
    }

    public override void SnapTo(Vector3 pos) {
        base.SnapTo(pos);
        if (!entityStatus.hasMoved) {
            entityStatus.hasMoved = true;
            GameManager.Instance.EntityMoving = true;
            EntityManager.Instance.CloseRadialMenu();
            MapManager.Instance.SetTileOccupied(this, false);
            transform.position = pos;
            MoveComplete();
        }
    }

    public override void SetSelected(bool value) {
        base.SetSelected(value);
        isSelected = value;
        SetSortingLayer(ENTITIES_SORTING_LAYER);
        if (isSelected) {
            SetSortingLayer(ENTITY_FOCUSED_SORTING_LAYER);
            if (skeletonAnimation != null) {
                StartCoroutine(FlashSelected());
            }
            //spriteTransform.DOScale(1.1f, 0.5f).SetEase(Ease.OutSine).SetLoops(-1, LoopType.Yoyo);
        }
        //else {
        //    StopAllCoroutines();
        //    //spriteTransform.localScale = Vector3.one;
        //    //DOTween.KillAll();
        //}
    }

    public override void TakeDamage(int damage, DamageType damageType, bool isCrit) {
        base.TakeDamage(damage, damageType, isCrit);
        int negatedDamage = NegatedDamage(damage, damageType);
        FloatingDamageNotifier.Instance.PushNumber(Position, negatedDamage, false, isCrit);
        entityStatus.currentHealth -= negatedDamage;
        if (entityStatus.currentHealth < 0) {
            Debug.Log("Players cannot die right now. Fix this".Bold().Sized(15));
            if (this.entityPropertiesData.entityType != EntityType.PLAYER) {
                entityStatus.currentHealth = 0;
                statusBars.OnHealthScaleComplete.AddListener(HealthScaleToZeroComplete);
            }
            else {
                entityStatus.currentHealth = entityPropertiesData.maxHealth;
            }
        }
        statusBars.SetHealthPercentage(entityStatus.currentHealth / entityPropertiesData.maxHealth);
    }

    public override void SpendResource(int amount) {
        base.SpendResource(amount);
        entityStatus.currentResource -= amount;
        if (entityStatus.currentResource < 0) {
            entityStatus.currentResource = 0;
            //statusBars.OnHealthScaleComplete.AddListener(HealthScaleToZeroComplete);
        }
        statusBars.SetResourcePercentage(entityStatus.currentResource / entityPropertiesData.maxResource);
    }

    public override void TurnBegin() {
        base.TurnBegin();
        if (entityPropertiesData.entityType == EntityType.PLAYER) {

        }
        else {
            aiController.Begin();
        }
    }

    public override void TurnEnded() {
        base.TurnEnded();
        entityStatus.hasMoved = false;
        entityStatus.hasAttacked = false;
        UpdateStatus();
        ResetCooldowns();
        // reduce duration of buffs and debuffs
    }

    #endregion PUBLIC_METHODS

    void Update() {
        
    }
}
