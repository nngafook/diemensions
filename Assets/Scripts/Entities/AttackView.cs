﻿using UnityEngine;

public class AttackView : MonoBehaviour {

    public delegate void OnAttackCompleteEvent(AttackView attackView);
    public event OnAttackCompleteEvent OnAttackComplete;

    protected FastPool fastPool;
    protected Vector3 targetPosition;

    public AttackVFXType attackVFXType;
    public AttackVFXType attackLandedVFXType;
    public ParticleSystem particles;

    #region PRIVATE_METHODS
    protected virtual void Awake() {

    }

    protected virtual void Start() {

    }

    protected virtual void AttackComplete() {
        if (OnAttackComplete != null) {
            OnAttackComplete(this);
        }
    }

    protected virtual void Instantiated() {

    }

    protected virtual void Destroyed() {

    }

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    //If you select notification via Interface, you must inherit from the IFastPoolItem interface and implement it.
    //If you select notification via SendMessage or BroadcastMessage - just implement methods below.

    //This method will be called when object FastInstantiated
    public void OnFastInstantiate() {
        //Debug.Log("I'm spawned!");
        Instantiated();
    }

    //This method will be called when object FastDestroyed
    public void OnFastDestroy() {
        //Debug.Log("I'm cached...");
        Destroyed();
    }

    public virtual void SetTargetPosition(Vector3 pos) {
        pos.z = 0;
        targetPosition = pos;
    }

    public virtual void SetTargetPosition(float x, float y) {

    }

    public virtual void Invoke() {

    }

    public virtual void Invoke(Vector3 pos) {
        SetTargetPosition(pos);
    }

    #endregion PUBLIC_METHODS

    void Update() {

    }
}
