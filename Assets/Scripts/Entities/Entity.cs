﻿using Spine;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(AIController))]
public class Entity : MonoBehaviour {

    protected const string ENTITY_FOCUSED_SORTING_LAYER = "EntityFocused";
    protected const string ENTITIES_SORTING_LAYER = "Entities";

    protected bool isSelected = false;
    protected EntityState currentState = EntityState.IDLE;
    protected BoxCollider2D boxCollider;
    protected AIController aiController;
    protected Entity targetEntity;

    protected List<Entity> nearbyPlayerEntities = new List<Entity>();
    protected List<Tile> movePath = new List<Tile>();

    public UID uid;

    public Attack[] attacks;
    public Animator animator;
    public SkeletonAnimation skeletonAnimation;
    public Transform spriteTransform;
    public EntityStatusBars statusBars;
    [Space(5)]
    public EntityPropertiesDataObject entityPropertiesData;
    public EnemyAIDataObject aiData;
    [Space(5)]
    public EntityStatus entityStatus;

    public bool IsSelected { get { return isSelected; } }
    public Entity TargetEntity { get { return targetEntity; } set { targetEntity = value; } }
    public EntityPropertiesDataObject Properties { get { return entityPropertiesData; } }
    public EntityStatus Status { get { return entityStatus; } }
    public Vector2 Position { get { return transform.position; } }
    public List<Entity> NearbyPlayerEntities { get { return nearbyPlayerEntities; } }
    public Skeleton SpineSkeleton { get { return skeletonAnimation.skeleton; } }
    public Vector2 Top { get { return Position.AddY(boxCollider.size.y); } }

    #region PRIVATE_METHODS
    protected virtual void Awake() {
        uid = new UID();
        boxCollider = this.GetComponent<BoxCollider2D>();

        GetAttacks();

        if (entityPropertiesData != null) {
            entityStatus.currentHealth = entityPropertiesData.maxHealth;
            entityStatus.currentResource = entityPropertiesData.maxResource;
        }
    }

    protected virtual void Start() {

    }

    protected virtual void MoveComplete() {

    }

    protected virtual void MoveToNextNode() {

    }

    protected virtual void MoveToNodeComplete() {

    }

    protected virtual void FaceMoveDirection(Vector2 pos) {

    }

    /// <summary>
    /// Creates the Attacks from the AttackDataObjects in the entityPropertiesData
    /// </summary>
    protected void GetAttacks() {
        if ((entityPropertiesData != null) && (entityPropertiesData.hasAttack)) {
            Attack skill = null;
            for (int i = 0; i < entityPropertiesData.attackDataObjects.Length; i++) {
                skill = new Attack();
                skill.CopyFromDataObject(entityPropertiesData.attackDataObjects[i]);
                List<Attack> tempList = new List<Attack>(attacks);
                tempList.Add(skill);
                attacks = tempList.ToArray();
            }
            GetSkillFromEquipment();
        }
    }

    protected void GetSkillFromEquipment() {
        if ((entityPropertiesData.hasAttack) && (entityStatus.equippedArmorSet != null)) {
            Attack skill = GetAttackByCommandType(CommandType.SKILL);
            if (skill != null) {
                skill.CopyFromDataObject(entityStatus.equippedArmorSet.attackDataObject);
            }
            else {
                skill = new Attack();
                skill.CopyFromDataObject(entityStatus.equippedArmorSet.attackDataObject);
                List<Attack> tempList = new List<Attack>(attacks);
                tempList.Add(skill);
                attacks = tempList.ToArray();
            }
        }
    }

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    public Attack GetAttackByCommandType(CommandType type) {
        Attack rVal = null;
        for (int i = 0; i < attacks.Length; i++) {
            if (attacks[i].type == type) {
                rVal = attacks[i];
                break;
            }
        }
        return rVal;
    }

    public virtual void ResetCooldowns() {

    }

    public virtual void GetMoveableTiles() {
        if (entityPropertiesData.hasCircularMoveRange) {
            MapManager.Instance.HighlightCircle(Position, entityPropertiesData.moveDistance, CommandType.MOVE);
        }
        else {
            MapManager.Instance.HighlightCross(Position, entityPropertiesData.moveDistance, CommandType.MOVE);
        }
        MapManager.Instance.ValidateMoveableTilePaths(this);
    }

    public virtual void TileClicked(Tile tile) {

    }

    public virtual bool MoveTo(Vector3 pos) {
        // I uncommented after seeing it commented... wut? oO
        return (movePath.Count > 0);
        //return false;
    }

    public virtual void SetSortingLayer(string layerName) {
        if (skeletonAnimation != null) {
            skeletonAnimation.GetComponent<MeshRenderer>().sortingLayerName = layerName;
        }
        else {
            spriteTransform.GetComponent<SpriteRenderer>().sortingLayerName = layerName;
        }
        statusBars.SetSortingLayer(layerName);
    }

    public virtual void SnapTo(Vector3 pos) {

    }

    public virtual void SetSelected(bool value) {

    }

    public virtual void TakeDamage(int damage, DamageType damageType, bool isCrit) {

    }

    public virtual void SpendResource(int amount) {

    }

    public virtual void TurnBegin() {

    }

    public virtual void TurnEnded() {

    }

    public virtual void UpdateStatus() {

    }

    #endregion PUBLIC_METHODS

    // Update is called once per frame
    void Update() {

    }
}
