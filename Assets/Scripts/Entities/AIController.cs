﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour {

    private Entity entityOwner;
    private List<Attack> possibleAttacks = new List<Attack>();
    private Entity targetPlayerEntity;

    #region PRIVATE_METHODS
    void Awake() {

    }

    void Start() {

    }

    private void EndTurn() {
        GameManager.Instance.EndTurn();
    }

    /// <summary>
    /// Returns the 4 adjacent tiles to the tile passed in.
    /// Returns the list shuffled.
    /// </summary>
    /// <param craftableName="tile"></param>
    /// <returns></returns>
    private List<Tile> GetAdjacentTiles(Tile tile) {
        List<Tile> rVal = new List<Tile>();
        List<int> randomChoice = new List<int>();
        for (int i = 0; i < 4; i++) {
            randomChoice.Add(i);
        }

        int choice = 0;
        int randomIndex = 0;
        Tile adjacentTile = null;
        while (rVal.Count < 4) {
            randomIndex = Random.Range(0, randomChoice.Count);
            choice = randomChoice[randomIndex];
            randomChoice.RemoveAt(randomIndex);

            switch (choice) {
                case 0:     // UP
                    adjacentTile = MapManager.Instance.GetTileFromGridCoord(tile.GridPos.column, tile.GridPos.row - 1);
                    break;
                case 1:     // RIGHT
                    adjacentTile = MapManager.Instance.GetTileFromGridCoord(tile.GridPos.column + 1, tile.GridPos.row);
                    break;
                case 2:     // DOWN
                    adjacentTile = MapManager.Instance.GetTileFromGridCoord(tile.GridPos.column, tile.GridPos.row + 1);
                    break;
                case 3:     // LEFT
                    adjacentTile = MapManager.Instance.GetTileFromGridCoord(tile.GridPos.column - 1, tile.GridPos.row - 1);
                    break;
            }

            rVal.Add(adjacentTile);
        }
        return rVal;
    }

    private Tile GetNearestReachableTile() {
        Tile rVal = null;

        targetPlayerEntity = EntityManager.Instance.GetNearestPlayerEntity(entityOwner);
        Tile ownerTile = MapManager.Instance.GetTileFromPosition(entityOwner.Position);

        // Get the tiles within the entity's movement distance
        List<Tile> moveableTiles = MapManager.Instance.GetMoveableTiles(entityOwner);
        // Make sure that all of the tiles are pathable to
        MapManager.Instance.ValidateMoveableTilePaths(entityOwner, ref moveableTiles);


        // Set the desired position to be the target's position. 
        // So the AI will always try to get to the target.
        // Get a path to the target, and then work backwards from target to entity,
        // and the first tile to be reachable through the ai's moveable tiles
        // is the tile to move to
        Tile targetTile = null;
        List<Tile> pathToTarget = new List<Tile>();
        targetTile = MapManager.Instance.GetTileFromPosition(targetPlayerEntity.Position);
        pathToTarget = MapManager.Instance.GetPathBetweenTiles(ownerTile, targetTile);

        if (pathToTarget.Count > 0) {

            //for (int i = 0; i < pathToTarget.Count; i++) {
            //    pathToTarget[i].SetHighlight(CommandType.MELEE);
            //}
            //yield return new WaitForSeconds(0.25f);
            //for (int i = 0; i < pathToTarget.Count; i++) {
            //    pathToTarget[i].ClearHighlight();
            //}


            for (int i = pathToTarget.Count - 1; i >= 0; i--) {
                if (moveableTiles.Contains(pathToTarget[i])) {
                    rVal = pathToTarget[i];
                    break;
                }
            }
        }
        return rVal;
    }

    #region COROUTINES

    /// <summary>
    /// AI procedure without ai data object
    /// </summary>
    /// <returns></returns>
    private IEnumerator EnemyConsciousness() {
        targetPlayerEntity = EntityManager.Instance.GetNearestPlayerEntity(entityOwner);
        Tile ownerTile = MapManager.Instance.GetTileFromPosition(entityOwner.Position);
        
        // Why isn't the return being checked?
        GetPossibleAttacks(targetPlayerEntity);

        if (targetPlayerEntity != null) {
            if ((possibleAttacks.Count > 0) && (!entityOwner.Status.hasAttacked)) {
                int randomIndex = Random.Range(0, possibleAttacks.Count);
                possibleAttacks[randomIndex].Invoke(targetPlayerEntity);
                FloatingDamageNotifier.Instance.AddNotificationListener(TakenDamageNotificationComplete);
            }
            else if (!entityOwner.Status.hasMoved) {
                Attack[] attacks = entityOwner.attacks;
                Attack randomAttack = attacks.RandomElement();

                yield return StartCoroutine(MoveToNearest());

                // Being done again cause the entity has moved?
                GetPossibleAttacks(targetPlayerEntity);

                if ((possibleAttacks.Count > 0) && (!entityOwner.Status.hasAttacked)) {
                    randomAttack.Invoke(targetPlayerEntity);
                    FloatingDamageNotifier.Instance.AddNotificationListener(TakenDamageNotificationComplete);
                }
                else {
                    EndTurn();
                }
            }
        }

        yield return null;
    }

    /// <summary>
    /// AI procedure with ai data object
    /// </summary>
    /// <returns></returns>
    private IEnumerator PredeterminedConsciousness() {
        EnemyAIDataObject aiData = entityOwner.aiData;

        targetPlayerEntity = EntityManager.Instance.GetNearestPlayerEntity(entityOwner);
        //Tile ownerTile = MapManager.Instance.GetTileFromPosition(entityOwner.Position);
        
        // Get the list of attacks possible based on location to target
        GetPossibleAttacks(targetPlayerEntity);
        
        switch (aiData.aggression) {
            case Aggression.AGGRESSIVE:
                if (!entityOwner.Status.hasMoved) {
                    if (aiData.distancePreference == DistancePreference.NEAR) {
                        StartCoroutine(NearThought());
                    }
                    else if (aiData.distancePreference == DistancePreference.RANGED) {
                        StartCoroutine(RangedThought());
                    }
                }
                break;
            case Aggression.DEFENSIVE:

                break;
            default:
                break;
        }

        yield return null;
    }

    private IEnumerator NearThought() {
        yield return StartCoroutine(MoveToNearest());
        if ((!entityOwner.Status.hasAttacked) && (possibleAttacks.Count > 0)) {
            Attack attack = BestCaseAttack();
            yield return StartCoroutine(InvokeAttack(attack));
        }
        else {
            EndTurn();
        }
    }

    private IEnumerator RangedThought() {
        yield return StartCoroutine(MoveAsRanged());
    }

    private Attack BestCaseAttack() {
        // == This assumes that GetPossibleAttacks has already been called == //
        Attack attack = null;
        EnemyAIDataObject aiData = entityOwner.aiData;
        string id = "";
        for (int i = 0; i < aiData.attackDataOrder.Count; i++) {
            if (attack == null) {
                id = aiData.attackDataOrder[i].ID;
                for (int j = 0; j < possibleAttacks.Count; j++) {
                    if (possibleAttacks[j].ID == id) {
                        attack = possibleAttacks[j];
                        break;
                    }
                }
            }
        }
        return attack;
    }

    private IEnumerator MoveAsRanged() {
        // Can attack where standing
        if (possibleAttacks.Count > 0) {
            Attack attack = BestCaseAttack();
            yield return StartCoroutine(InvokeAttack(attack));
        }
        else {
            Tile nearestTile = GetNearestReachableTile();

            // Get a list of attacks that is possible if the ai moves
            float distance = TileDistanceBetween(nearestTile.Position, targetPlayerEntity.Position);
            List<Attack> attacksPossibleInMoveRange = new List<Attack>();
            for (int i = 0; i < possibleAttacks.Count; i++) {
                if (distance <= possibleAttacks[i].range) {
                    attacksPossibleInMoveRange.Add(possibleAttacks[i]);
                }
            }

            if (attacksPossibleInMoveRange.Count > 0) {
                Debug.Log("Moving in range to do: " + BestCaseAttack().commandName);
                yield return StartCoroutine(MoveToNearest());

                Attack attack = BestCaseAttack();
                yield return StartCoroutine(InvokeAttack(attack));
            }
            else {
                Debug.Log("Cannot move in range to do any attacks");
                yield return StartCoroutine(MoveToNearest());
                EndTurn();
            }
        }

        yield return null;
    }

    private IEnumerator MoveToNearest() {
        Tile nearestTile = GetNearestReachableTile();
        if (nearestTile != null) {
            nearestTile.SetAttackRangeHighlight(true);
            entityOwner.MoveTo(nearestTile.Position);
            yield return StartCoroutine(WaitForMoveComplete(entityOwner));
        }
    }

    private IEnumerator WaitForMoveComplete(Entity entity) {

        while (GameManager.Instance.EntityMoving) {
            // Waits for 0.25 seconds to add that extra delay to the game loop
            yield return new WaitForSeconds(0.25f);
        }

    }

    private IEnumerator InvokeAttack(Attack attack) {
        if (attack != null) {
            AttackDialogNotifier.Show(entityOwner.Top, attack.commandName);
            yield return StartCoroutine(AttackDialogNotifier.WaitForComplete());
            attack.Invoke(targetPlayerEntity);
            // This auto ends after damage is done. Might not always want to do that
            FloatingDamageNotifier.Instance.AddNotificationListener(TakenDamageNotificationComplete);
        }
        else {
            Debug.LogError("Attack Passed to InvokeAttack by " + entityOwner.name + " is null");
        }
    }

    #endregion COROUTINES

    private void TakenDamageNotificationComplete() {
        FloatingDamageNotifier.Instance.RemoveNotificationListener(TakenDamageNotificationComplete);
        EndTurn();
        Debug.Log("Damage Complete".Colored(Color.cyan));
    }

    //private void OnAttackComplete(Attack attack) {
    //    attack.OnAttackCompleteEvent.RemoveListener(OnAttackComplete);
    //StartCoroutine(PostAttackDelay());
    //}

    private void GetPossibleAttacks(Entity nearbyPlayerEntity) {
        possibleAttacks.Clear();
        Attack attack = null;
        Attack[] attacks = entityOwner.attacks;
        if (attacks.Length > 0) {
            float distanceBetween = TileDistanceBetween(entityOwner.Position, nearbyPlayerEntity.Position);

            for (int i = 0; i < attacks.Length; i++) {
                attack = attacks[i];
                if ((attack.resourceCost <= entityOwner.entityStatus.currentResource) && (distanceBetween <= attack.range)) {
                    possibleAttacks.Add(attack);
                }
            }
        }
        else {
            Debug.LogError(entityOwner.name + " has no attacks assigned");
        }
    }

    //private void SortAttacksByPriority(ref List<Attack> attacks) {
    //    List<Attack> tempList = new List<Attack>();
    //    AttackDataObject[] priorityList = entityOwner.entityPropertiesData.attackDataObjects;
    //    for (int i = 0; i < priorityList.Length; i++) {
    //        for (int j = 0; j < attacks.Count; j++) {
    //            if (attacks[j].ID == priorityList[i].ID) {
    //                tempList.Add(attacks[j]);
    //            }
    //        }
    //    }
    //    attacks = new List<Attack>(tempList);
    //}

    private float TileDistanceBetween(Vector2 posOne, Vector2 posTwo) {
        float distanceBetween = Mathf.CeilToInt(Mathf.Abs(Vector2.Distance(posOne, posTwo) / MapManager.Instance.TileSize));
        return distanceBetween;
    }

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    public void SetEntityOwner(Entity entity) {
        entityOwner = entity;
    }

    public void Begin() {
        if (entityOwner.Properties.entityType == EntityType.ENEMY) {
            if (entityOwner.aiData == null) {
                StartCoroutine(EnemyConsciousness());
            }
            else {
                StartCoroutine(PredeterminedConsciousness());
            }
        }
    }

    #endregion PUBLIC_METHODS

    void Update() {

    }
}
