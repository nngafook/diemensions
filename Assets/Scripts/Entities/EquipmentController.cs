﻿using UnityEngine;
using System.Collections;

public class EquipmentController : MonoBehaviour {

    public Sprite headSprite;
    public Sprite chestSprite;

    [SpineSlot]
    public string helmSlot;
    [SpineSlot]
    public string torsoSlot;
    [SpineSlot]
    public string legSlot;
    [SpineSlot]
    public string mainHandSlot;
    [SpineSlot]
    public string offHandSlot;
    [SpineSlot]
    public string twoHandSlot;

    public SkeletonAnimation skeletonAnimation;

	#region PRIVATE_METHODS
	void Awake() {
        
	}
	
	void Start () {
	
	}
	#endregion PRIVATE_METHODS

	#region PUBLIC_METHODS

    public void EquipItem(EquippableItemType type, Sprite sprite) {
        string slotName = "";
        switch (type) {
            case EquippableItemType.HELM:
                slotName = helmSlot;
                break;
            case EquippableItemType.CHESTPIECE:
                slotName = torsoSlot;
                break;
            case EquippableItemType.LEG:
                slotName = legSlot;
                break;
            case EquippableItemType.MAIN_HAND_WEAPON:
                slotName = mainHandSlot;
                break;
            case EquippableItemType.OFF_HAND_WEAPON:
                slotName = offHandSlot;
                break;
            case EquippableItemType.TWO_HAND_WEAPON:
                slotName = twoHandSlot;
                break;
        }
        skeletonAnimation.skeleton.AttachUnitySprite(slotName, sprite);
    }

	#endregion PUBLIC_METHODS

	void Update () {
        if (Input.GetKeyUp(KeyCode.H)) {
            EquipItem(EquippableItemType.HELM, headSprite);
        }

        if (Input.GetKeyUp(KeyCode.C)) {
            EquipItem(EquippableItemType.CHESTPIECE, chestSprite);
        }

	}
}
