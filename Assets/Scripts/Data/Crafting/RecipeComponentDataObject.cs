﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class RecipeComponentDataObject : GameItemDataObject {

    [Tooltip("Percentage chance of dropping")]
    public PercentageFloat rarity;

}
