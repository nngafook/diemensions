﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class GameItemDataObject : ScriptableObject {

    public string itemName = "";
    public string id = "";
    public string uiIcon = "";
    public ItemType itemType;

    /*public void SetBaseProperties(string _id, string _itemName, string _uiIcon, ItemType type) {
        id = _id;
        itemName = _itemName;
        uiIcon = _uiIcon;
        itemType = type;
    }*/

}
