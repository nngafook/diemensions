﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class RecipeDataObject : GameItemDataObject {

    public string outputCraftableID = "";
    public List<RecipeInput> inputComponents = new List<RecipeInput>();

}
