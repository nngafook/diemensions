﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class CraftableDataObject : GameItemDataObject {

    public RecipeDataObject recipeDataObject;
    public AttackDataObject attackDataObject;
    public CharacterClass classRequirement;

    [Header("Defensive Stats")]
    public float physicalDefense;
    public float magicDefense;

    [Header("Offensive Stats")]
    public float physicalAttack;
    public float magicAttack;
    public float critDamage;
    [Tooltip("Value is added onto the base focus of the entity")]
    [Range(0, 100)]
    public float focus;

}
