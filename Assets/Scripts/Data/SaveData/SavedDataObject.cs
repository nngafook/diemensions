﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SavedDataObject : ScriptableObject {

    public GameProgressionID currentObjectiveID = GameProgressionID.WELCOME;
    public List<GameProgressionID> completedObjectives = new List<GameProgressionID>();

    public void SetCurrentObjectiveID(GameProgressionID id) {
        currentObjectiveID = id;
    }

}