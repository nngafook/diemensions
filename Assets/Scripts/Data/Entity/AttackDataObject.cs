﻿using UnityEngine;
using System.Collections;

public class AttackDataObject : CommandDataObject {

    public AttackType attackType;
    public AttackVFXType vfxType;
    public DamageType damageType;

    public int range;
    public int aoeRadius;
    public bool blockedByTerrain;
    public RangedFloat baseDamage;

    public static bool operator == (AttackDataObject o1, AttackDataObject o2) {
        return (o1.ID == o2.ID);
    }

    public static bool operator != (AttackDataObject o1, AttackDataObject o2) {
        return !(o1 == o2);
    }

}
