﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyAIDataObject : ScriptableObject {

    public EntityPropertiesDataObject propertiesData;

    public Aggression aggression;
    public DistancePreference distancePreference;

    public List<AttackDataObject> attackDataOrder;

    // Aggressive
    // or
    // Defensive

    // if aggressive {
        // Try to get as close as possible?
        // or
        // Stay as far as possible but still be able to attack
    // }


    // else if defensive {
        // 
    // }


    // Attack priority

    // low health attack (if below x percentage health)
}


public enum Aggression {
    AGGRESSIVE,
    DEFENSIVE
}

public enum DistancePreference {
    NEAR,
    RANGED
}