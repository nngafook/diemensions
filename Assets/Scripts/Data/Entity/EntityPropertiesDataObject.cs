﻿using UnityEngine;
using System.Collections;

public class EntityPropertiesDataObject : ScriptableObject {

    [Tooltip("Type of entity")]
    public EntityType entityType;

    [Tooltip("Name of Entity")]
    public string name;

    [Tooltip("Class of Entity")]
    public CharacterClass classType;

    [Tooltip("If this entity needs a turn in the game")]
    public bool needsTurn;
    [Tooltip("If this entity can move on it's own")]
    public bool hasMobility;
    [Tooltip("If this entity can attack")]
    public bool hasAttack;
    [Tooltip("If this entity has a lifetime duration")]
    public bool hasLifetimeDuration;
    [Tooltip("If this entity's move distance is a 'circle' or a cross. If this becomes more than two options, enum this bitch")]
    public bool hasCircularMoveRange;
    [Tooltip("Used mainly for prop entities. Some props need to occupy the tile they're on (hedges) and some don't (torches)")]
    public bool occupiesTile;
    [Tooltip("Movement Speed in seconds between nodes")]
    public float movementSpeed;

    [Tooltip("Max Health")]
    public int maxHealth;
    [Tooltip("Max Resource")]
    public int maxResource;
    [Tooltip("How far this entity can move")]
    public int moveDistance;
    [Tooltip("How much fog does this entity clear in a circle around it")]
    public int visionRadius;
    [Tooltip("How far the entity can 'see' around it. Used to detect if it can see a player entity or not. This times moveDistance is used to determine validity of movement")]
    public int visionMoveFactor;

    [Header("Offensive")]
    [Tooltip("Physical damage multiplier. Added to any physical attacks")]
    public int physicalAttack;
    [Tooltip("Magical damage multiplier. Added to any magical attacks")]
    public int magicAttack;
    [Tooltip("BaseDamage multiplied by this is used in attacks, if a crit is successful")]
    public float critDamage;
    [Tooltip("Critical attack chance on any kind of attack. Percentage chance 0-100")]
    public PercentageFloat focus;

    [Header("Defensive")]
    [Tooltip("Physical defense. Negates this amount of physical damage from incoming attacks")]
    public RangedFloat physicalDefense;
    [Tooltip("Magic defense. Negates this amount of magical damage from incoming attacks")]
    public RangedFloat magicDefense;

    [Header("Attacks")]
    // Attacks
    public AttackDataObject[] attackDataObjects;

}
