﻿using UnityEngine;
using System.Collections;

public class CommandDataObject : ScriptableObject {

    public string commandName;
    public string ID;
    public CommandType type;

    public int resourceCost;

    // NEEDS A WEIGHTED CHANCE PROPERTY

}
