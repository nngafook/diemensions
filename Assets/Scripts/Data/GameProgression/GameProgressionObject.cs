﻿using UnityEngine;
using System.Collections;

public class GameProgressionObject : ScriptableObject {

    // There might come a day that this cannot be considered a game progression id,
    // cause an objective != game scene != dialog and so on
    public GameProgressionID id;

}
