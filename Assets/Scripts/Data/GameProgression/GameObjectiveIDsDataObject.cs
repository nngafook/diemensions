﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameObjectiveIDsDataObject : ScriptableObject {

    [Header("This is obsolete")]
    public List<string> gameObjectiveIds = new List<string>();

}
