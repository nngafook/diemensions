﻿using UnityEngine;
using System.Collections;

public class GameObjective : GameProgressionObject {

    // can contain the next game objective
    public bool unlocksObjective = false;
    public GameObjective unlockedObjective;

    public DialogModule dialogModule;

}
