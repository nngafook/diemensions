﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DialogModule : ScriptableObject {

    public List<DialogBlock> dialogBlocks;

}
