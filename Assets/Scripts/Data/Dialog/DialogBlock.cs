﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DialogBlock : ScriptableObject {

    public string speakerName;

    public List<DialogLine> dialogList;


}
