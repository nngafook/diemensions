﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DialogTestingGroundController : MonoBehaviour {

    private int currentBlockIndex = 0;
    private int currentLineIndex = 0;

    private DialogBlock currentDialogBlock;

    public DialogModule dialogModule;

    public Text speakerNameText;
    public TextTypewriter dialogText;

	#region PRIVATE_METHODS
	void Awake() {
	
	}
	
	void Start () {
        currentDialogBlock = dialogModule.dialogBlocks[currentBlockIndex];
        speakerNameText.text = currentDialogBlock.speakerName;
        currentBlockIndex++;
	}
	#endregion PRIVATE_METHODS

	#region PUBLIC_METHODS
	
	#endregion PUBLIC_METHODS

	void OnGUI () {
        if (GUILayout.Button("Advance")) {
            if (((currentLineIndex) == currentDialogBlock.dialogList.Count) && ((currentBlockIndex) == dialogModule.dialogBlocks.Count)) {
                Debug.Log("Out of text");
            }
            else {
                speakerNameText.text = currentDialogBlock.speakerName;
                dialogText.SetText(currentDialogBlock.dialogList[currentLineIndex].lineOfDialog);
                currentLineIndex++;
                if (((currentBlockIndex) < dialogModule.dialogBlocks.Count) && ((currentLineIndex) == currentDialogBlock.dialogList.Count)) {
                    currentDialogBlock = dialogModule.dialogBlocks[currentBlockIndex];
                    currentBlockIndex++;
                    currentLineIndex = 0;
                }
            }
        }
	}

}
