﻿using UnityEngine;
using System.Collections;

public class SpineController : MonoBehaviour {

    public Sprite torsoSprite;

    [SpineSlot]
    public string torsoSlot;

    [SpineAttachment(currentSkinOnly: true)]
    public string torsoAttachment;

    public SpineAtlasRegion atlasRegion;

    private SkeletonAnimation skeletonAnimation;
    private SkeletonRenderer skeletonRenderer;

	#region PRIVATE_METHODS
	void Awake() {
        skeletonAnimation = this.GetComponent<SkeletonAnimation>();
        skeletonRenderer = this.GetComponent<SkeletonRenderer>();
	}
	
	void Start () {
        
	}
	#endregion PRIVATE_METHODS


	void Update () {
        if (Input.GetKeyUp(KeyCode.Space)) {
            //skeletonAnimation.AnimationName = attackAnimation;
            skeletonRenderer.skeleton.AttachUnitySprite(torsoSlot, torsoSprite, shaderName:"Sprites/Default");
            //skeletonRenderer.skeleton.SetSlotsToSetupPose();
        }
        else {
            if (Input.GetKey(KeyCode.RightArrow)) {
                //skeletonAnimation.AnimationName = moveAnimation;
                //skeletonAnimation.skeleton.FlipX = false;
                //transform.Translate(moveSpeed * Time.deltaTime, 0, 0);
            }
            else if (Input.GetKey(KeyCode.LeftArrow)) {
                //skeletonAnimation.AnimationName = moveAnimation;
                //skeletonAnimation.skeleton.FlipX = true;
                //transform.Translate(-moveSpeed * Time.deltaTime, 0, 0);
            }
            else {
                //skeletonAnimation.AnimationName = idleAnimation;
            }
        }
	}
}
