﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Recipe : GameItem {

    public string OutputCraftableID;
    public List<RecipeInput> InputComponents;

    public Recipe() {
        InputComponents = new List<RecipeInput>();
    }

    public Recipe(string _id, string _name, string _uiIcon) {
        id = _id;
        name = _name;
        uiIcon = _uiIcon;
        InputComponents = new List<RecipeInput>();
    }

    public Recipe(Recipe recipeCopy) {
        InputComponents = new List<RecipeInput>();
        id = recipeCopy.id;
        name = recipeCopy.name;
        uiIcon = recipeCopy.uiIcon;
        OutputCraftableID = recipeCopy.OutputCraftableID;
        RecipeInput input;
        for (int i = 0; i < recipeCopy.InputComponents.Count; i++) {
            input = new RecipeInput(recipeCopy.InputComponents[i].amount, recipeCopy.InputComponents[i].recipeID);
            InputComponents.Add(input);
        }
    }

    public void Export() {
        string propertiesJSON = JsonUtility.ToJson(this);
        Debug.Log("RECIPE".Bold().Colored(CustomColor.GetColor(ColorName.PURPLE_PLUM)));
        Debug.Log(propertiesJSON);
    }

}