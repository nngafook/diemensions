﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class CraftingUtility : MonoBehaviour {
    
    [HideInInspector]
    public UnityEvent OnRecipeComponentsUpdated;
    [HideInInspector]
    public UnityEvent OnRecipesUpdated;
    [HideInInspector]
    public UnityEvent OnCraftablesUpdated;

    public RecipeComponentDatabase RecipeComponentDatabase { get { return this.GetComponent<RecipeComponentDatabase>(); } }
    public RecipeDatabase RecipeDatabase { get { return this.GetComponent<RecipeDatabase>(); } }
    public CraftableDatabase CraftableDatabase { get { return this.GetComponent<CraftableDatabase>(); } }

    public void RecipeComponentsUpdated() {
        if (OnRecipeComponentsUpdated != null) {
            OnRecipeComponentsUpdated.Invoke();
        }
    }

    public void RecipesUpdated() {
        if (OnRecipesUpdated != null) {
            OnRecipesUpdated.Invoke();
        }
    }

    public void CraftablesUpdated() {
        if (OnCraftablesUpdated != null) {
            OnCraftablesUpdated.Invoke();
        }
    }

}
