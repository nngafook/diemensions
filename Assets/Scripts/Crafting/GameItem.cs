﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class GameItem {

    public string id = "";
    public string name = "";
    public string uiIcon = "";

    public ItemType itemType;

}