﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

public class RecipeComponentDatabase {

    private static RecipeComponentDatabase instance = null;
    public static RecipeComponentDatabase Instance { get { if (instance == null) { instance = new RecipeComponentDatabase(); } return instance; } }

    private string databasePath = "Data/Crafting/RecipeComponents";

    public List<RecipeComponentDataObject> componentsList = new List<RecipeComponentDataObject>();

    public RecipeComponentDatabase() {
        RecipeComponentDataObject[] objects = Resources.LoadAll<RecipeComponentDataObject>(databasePath);
        componentsList = new List<RecipeComponentDataObject>(objects);
    }

    public string GetRandomRecipeComponentID() {
        return componentsList.RandomElement().id;
    }

}