﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class Craftable : GameItem {

    public string recipeID;
    public List<RecipeInput> recipeInputList;

    public Craftable(string _id, string _name, string _uiIcon, string _recipeID) {
        id = _id;
        name = _name;
        uiIcon = _uiIcon;
        recipeID = _recipeID;
    }

}