﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

[Serializable]
public class CraftableDatabase : MonoBehaviour {

    private string textFilePath = "/Data/Crafting/Craftables.txt";

    public List<Craftable> craftablesList;

    public void AddNewCraftable(Craftable craftable) {
        ForceImport();
        craftablesList.Add(craftable);
        Export();
        this.GetComponent<CraftingUtility>().CraftablesUpdated();
    }

    public void DeleteIndex(int index) {
        craftablesList.RemoveAt(index);
        Export();
        this.GetComponent<CraftingUtility>().CraftablesUpdated();
    }

    public void SetRecipeOfCraftable(string craftableID, string recipeID) {
        ForceImport();

        for (int i = 0; i < craftablesList.Count; i++) {
            if (craftablesList[i].id == craftableID) {
                craftablesList[i].recipeID = recipeID;
                break;
            }
        }

        Export();
        this.GetComponent<CraftingUtility>().CraftablesUpdated();
    }

    public void ForceImport() {
        string path = Application.dataPath + textFilePath;
        WWW reader = new WWW("file:///" + path);
        while (!reader.isDone) {

        }
        Import(reader.text);
    }

    public void Import(string json) {
        if (!string.IsNullOrEmpty(json)) {
            craftablesList = new List<Craftable>();
            CraftableDatabaseJsonObject jsonObj = JsonUtility.FromJson<CraftableDatabaseJsonObject>(json);
            for (int i = 0; i < jsonObj.CraftablesList.Count; i++) {
                craftablesList.Add(jsonObj.CraftablesList[i]);
            }
            Debug.Log("Import Complete".Bold().Colored(CustomColor.GetColor(ColorName.ROB_BLUE)));
        }
    }

    public void Export() {
        CraftableDatabaseJsonObject jsonObject = new CraftableDatabaseJsonObject();
        jsonObject.CraftablesList = craftablesList;

        string propertiesJSON = JsonUtility.ToJson(jsonObject);
        Debug.Log(propertiesJSON.Bold().Colored(CustomColor.GetColor(ColorName.PURPLE_PLUM)));
        Debug.Log("Export Complete".Bold().Colored(CustomColor.GetColor(ColorName.PURPLE_PLUM)));
        Debug.Log(propertiesJSON);
        Utility.WriteData(propertiesJSON, textFilePath);
    }

}

[Serializable]
class CraftableDatabaseJsonObject {
    public List<Craftable> CraftablesList;

    public CraftableDatabaseJsonObject() {
        CraftablesList = new List<Craftable>();
    }
}