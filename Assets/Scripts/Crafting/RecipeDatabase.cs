﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

public class RecipeDatabase : MonoBehaviour {

    private string textFilePath = "/Data/Crafting/Recipes.txt";

    public List<Recipe> recipeList;

    public void AddNewRecipe(Recipe recipe) {
        ForceImport();
        recipeList.Add(recipe);
        Export();
    }

    public void SetCraftableOfRecipe(string recipeID, string craftableID) {
        ForceImport();
        for (int i = 0; i < recipeList.Count; i++) {
            if (recipeList[i].id == recipeID) {
                recipeList[i].OutputCraftableID = craftableID;
            }
        }
        Export();
    }

    public void DeleteIndex(int index) {
        recipeList.RemoveAt(index);
        Export();
        this.GetComponent<CraftingUtility>().RecipesUpdated();
    }

    public void ForceImport() {
        string path = Application.dataPath + textFilePath;
        WWW reader = new WWW("file:///" + path);
        while (!reader.isDone) {

        }
        Import(reader.text);
    }

    public void Import(string json) {
        if (!string.IsNullOrEmpty(json)) {
            recipeList = new List<Recipe>();
            RecipeDatabaseJsonObject jsonObject = JsonUtility.FromJson<RecipeDatabaseJsonObject>(json);
            Recipe recipe;
            for (int i = 0; i < jsonObject.RecipeList.Count; i++) {
                recipe = new Recipe(jsonObject.RecipeList[i]);
                recipeList.Add(recipe);
            }
            Debug.Log("Import Complete".Bold().Colored(CustomColor.GetColor(ColorName.ROB_BLUE)));
        }
    }

    public void Export() {
        RecipeDatabaseJsonObject jsonObject = new RecipeDatabaseJsonObject();
        jsonObject.RecipeList = recipeList;

        string propertiesJSON = JsonUtility.ToJson(jsonObject);
        Debug.Log("Export Complete".Bold().Colored(CustomColor.GetColor(ColorName.PURPLE_PLUM)));
        Debug.Log(propertiesJSON);
        Utility.WriteData(propertiesJSON, textFilePath);
    }

}

[Serializable]
class RecipeDatabaseJsonObject {
    public List<Recipe> RecipeList;

    public RecipeDatabaseJsonObject() {
        RecipeList = new List<Recipe>();
    }
}