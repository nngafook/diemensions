﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class RecipeComponent : GameItem {

    public RecipeComponent() {

    }

    public RecipeComponent(string _id, string _name, string _uiIcon) {
        id = _id;
        name = _name;
        uiIcon = _uiIcon;
    }

}
