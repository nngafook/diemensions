﻿using System.Collections.Generic;
using UnityEngine;


#pragma warning disable 0660
#pragma warning disable 0661

public class FogManager : MonoBehaviour {

    #region SINGLETON
    private static FogManager instance = null;
    public static FogManager Instance {
        get {
            return instance;
        }
    }
    #endregion SINGLETON

    [Range(0, 1)]
    public float outlinePercent;
    public float tileSize;
    public GridCoord mapSize = new GridCoord(30, 15);
    public Transform tilePrefab;
    public Color fogColor;
    public bool fogVisible = true;

    private List<GridCoord> allTileCoords;
    private Transform[,] tileMap;

    void Awake() {
        instance = this;
        fogVisible = true;
    }

    public void GenerateMap() {
        tileMap = new Transform[mapSize.column, mapSize.row];

        // Generating coords
        allTileCoords = new List<GridCoord>();
        for (int x = 0; x < mapSize.column; x++) {
            for (int y = 0; y < mapSize.row; y++) {
                allTileCoords.Add(new GridCoord(x, y));
            }
        }

        // Create map holder object
        string holderName = "Fog";
        if (transform.FindChild(holderName)) {
            DestroyImmediate(transform.FindChild(holderName).gameObject);
        }

        Transform mapHolder = new GameObject(holderName).transform;
        mapHolder.SetParent(transform, false);
        Transform newTile = null;
        // Spawning tiles
        for (int x = 0; x < mapSize.column; x++) {
            for (int y = 0; y < mapSize.row; y++) {
                Vector3 tilePosition = CoordToPosition(x, y);
                newTile = Instantiate(tilePrefab, tilePosition, Quaternion.identity) as Transform;
                newTile.GetComponent<FogTile>().SetGridPos(x, y);
                newTile.localScale = Vector3.one * (1 - outlinePercent) * tileSize;
                newTile.SetParent(mapHolder);
                tileMap[x, y] = newTile;


                SpriteRenderer tileRenderer = newTile.GetComponent<SpriteRenderer>();
                //Material tileMaterial = new Material(tileRenderer.sharedMaterial);
                Color c = fogColor;
                c.a = fogVisible.AsInt();
                tileRenderer.color = c;
            }
        }
    }

    private Vector3 CoordToPosition(int x, int y) {
        return new Vector3(-mapSize.column / 2f + 0.5f + x, -mapSize.row / 2f + 0.5f + y, 0) * tileSize;
    }

    public FogTile GetTileFromPosition(Vector3 position) {
        FogTile rVal = null;
        int column = Mathf.RoundToInt(position.x / tileSize + (mapSize.column - 1) / 2f);
        int row = Mathf.RoundToInt(position.y / tileSize + (mapSize.row - 1) / 2f);
        if (Utility.IsWithinGrid(column, row, mapSize.column, mapSize.row)) {
            rVal = tileMap[column, row].GetComponent<FogTile>();
        }
        return rVal;
    }

    public FogTile GetTileFromGridCoord(int coordColumn, int coordRow) {
        FogTile rVal = null;
        if (Utility.IsWithinGrid(coordColumn, coordRow, mapSize.column, mapSize.row)) {
            rVal = GetTileFromPosition(CoordToPosition(coordColumn, coordRow));
        }
        return rVal;
    }

    public FogTile GetTileAtMousePosition() {
        return GetTileFromPosition(Utility.MouseWorldPosition());
    }

    public bool FogRevealedAt(Vector3 pos) {
        bool rVal = false;
        FogTile tile = GetTileFromPosition(pos);
        if (tile != null) {
            return tile.IsFullyRevealed;
        }
        return rVal;
    }

    public void RevealCircle(Vector3 centerPos, int radius) {
        if (radius > 0) {
            FogTile centerTile = GetTileFromPosition(centerPos);

            if (centerTile != null) {
                int iter = 0;
                int currentRow = centerTile.GridPos.row;
                int currentColumn = centerTile.GridPos.column;
                FogTile currentTile = GetTileFromGridCoord(currentColumn, currentRow);

                // Fill the row
                iter = radius;
                for (int i = -iter; i <= iter; i++) {
                    currentColumn = centerTile.GridPos.column + i;
                    currentTile = GetTileFromGridCoord(currentColumn, currentRow);
                    if (currentTile != null) {
                        currentTile.Reveal();
                        //highlightedTiles.Add(currentTile);
                    }
                }

                iter = 0;
                currentRow = centerTile.GridPos.row + radius;
                currentColumn = centerTile.GridPos.column;

                // Top half
                while (currentRow != centerTile.GridPos.row) {
                    for (int i = -iter; i <= iter; i++) {
                        currentColumn = centerTile.GridPos.column + i;
                        currentTile = GetTileFromGridCoord(currentColumn, currentRow);
                        if (currentTile != null) {
                            currentTile.Reveal();
                            //highlightedTiles.Add(currentTile);
                        }
                    }
                    iter++;
                    currentRow--;
                }

                currentRow = centerTile.GridPos.row - radius;
                currentColumn = centerTile.GridPos.column;
                iter = 0;

                // Bottom half
                while (currentRow != centerTile.GridPos.row) {
                    for (int i = -iter; i <= iter; i++) {
                        currentColumn = centerTile.GridPos.column + i;
                        currentTile = GetTileFromGridCoord(currentColumn, currentRow);
                        if (currentTile != null) {
                            currentTile.Reveal();
                            //highlightedTiles.Add(currentTile);
                        }
                    }
                    iter++;
                    currentRow++;
                }
            }
        }
        // Could do something like store all of the tiles that got revealed and THEN check for fade on them.
        Refresh();
    }

    public void Refresh() {
        FogTile fogTile = null;
        for (int column = 0; column < mapSize.column; column++) {
            for (int row = 0; row < mapSize.row; row++) {
                fogTile = GetTileFromGridCoord(column, row);
                SetFade(fogTile);
            }
        }
    }

    private void SetFade(FogTile centerTile) {
        FogTile tileToCheck = GetTileFromGridCoord(centerTile.GridPos.column, centerTile.GridPos.row - 1);
        if (centerTile.IsRevealed) {
            // TOP
            if ((tileToCheck != null) && (!tileToCheck.IsRevealed)) {
                centerTile.SetFaded();
                return;
            }

            // BOTTOM
            tileToCheck = GetTileFromGridCoord(centerTile.GridPos.column, centerTile.GridPos.row + 1);
            if ((tileToCheck != null) && (!tileToCheck.IsRevealed)) {
                centerTile.SetFaded();
                return;
            }

            // LEFT
            tileToCheck = GetTileFromGridCoord(centerTile.GridPos.column - 1, centerTile.GridPos.row);
            if ((tileToCheck != null) && (!tileToCheck.IsRevealed)) {
                centerTile.SetFaded();
                return;
            }

            // RIGHT
            tileToCheck = GetTileFromGridCoord(centerTile.GridPos.column + 1, centerTile.GridPos.row);
            if ((tileToCheck != null) && (!tileToCheck.IsRevealed)) {
                centerTile.SetFaded();
                return;
            }
        }
    }

    #region CHEAT
    public void SetToggleVisible(bool value) {
        if (value) {
            transform.SetY(0);
        }
        else {
            transform.SetY(10000);
        }
    }
    #endregion CHEAT

    // Update is called once per frame
    void Update() {

    }
}
