﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

    private static AudioManager instance = null;
    public static AudioManager Instance { get { return instance; } }

    [Header("Libraries")]
    public SFXLibrary sfxLibrary;

    [Header("Sources")]
    public AudioSource[] sfxSources;
    public AudioSource[] musicSources;

	#region PRIVATE_METHODS
	void Awake() {
        instance = this;
	}
	
	void Start () {
	
	}
	#endregion PRIVATE_METHODS

	#region PUBLIC_METHODS

    public void PlaySFX(SFXName sfxName) {
        sfxSources[0].PlayOneShot(sfxLibrary.GetClipByName(sfxName), 0.5f);
    }

	#endregion PUBLIC_METHODS

	void Update () {
	
	}
}
