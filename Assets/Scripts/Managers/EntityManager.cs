﻿using System.Collections.Generic;
using UnityEngine;

public class EntityManager : MonoBehaviour {

    #region SINGLETON
    private static EntityManager instance = null;
    public static EntityManager Instance {
        get {
            return instance;
        }
    }
    #endregion SINGLETON

    private static int entityFactoryID;
    public static int EntityFactoryID { get { ++entityFactoryID; return entityFactoryID; } }

    private Entity selectedEntity;
    private Entity activeEntity;

    private EntityDatabase entityDatabase = new EntityDatabase();

    public RadialEntityMenu radialEntityMenu;
    public RadialEntityMenuButton SelectedButton { get { return radialEntityMenu.SelectedButton; } }

    public Entity SelectedEntity { get { return selectedEntity; } }
    public Entity ActiveEntity { get { return activeEntity; } set { activeEntity = value; } }
    //public Command SelectedCommand { get { return radialEntityMenu.SubMenu.SelectedCommand; } }
    public Vector3 ActiveEntityPosition { get { return ActiveEntity.Position; } }

    #region PRIVATE_METHODS
    void Awake() {
        instance = this;
    }

    void Start() {

    }

    #region EVENT_CALLBACKS
    private void OnRadialEntityMenuClosed() {
        MSN.Instance.StopListening(EventName.ENTITY_MENU_CLOSE_COMPLETE, OnRadialEntityMenuClosed);
        selectedEntity.SetSelected(false);
        selectedEntity = null;
    }
    #endregion EVENT_CALLBACKS

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    public void RegisterEntity(Entity entity) {
        entityDatabase.RegisterEntity(entity);
    }

    public void UpdateEntityStatuses() {
        entityDatabase.UpdateStatuses();
    }

    public void SetOrderList(ref List<Entity> orderList) {
        // Add players
        for (int i = 0; i < entityDatabase.PlayerEntities.Count; i++) {
            orderList.Add(entityDatabase.PlayerEntities[i]);
        }
        // Add enemies
        for (int i = 0; i < entityDatabase.EnemyEntities.Count; i++) {
            orderList.Add(entityDatabase.EnemyEntities[i]);
        }
        // Add bosses
        for (int i = 0; i < entityDatabase.BossEntities.Count; i++) {
            orderList.Add(entityDatabase.BossEntities[i]);
        }
        // Add neutrals?
    }

    public Entity EntityByUID(UID id) {
        Entity rVal = null;

        for (int i = 0; i < entityDatabase.AllEntities.Count; i++) {
            if (entityDatabase.AllEntities[i].uid == id) {
                rVal = entityDatabase.AllEntities[i];
                break;
            }
        }

        return rVal;
    }

    #region BUTTON_CALLBACKS

    private void MoveButtonPressed(bool btnSelected) {
        if (btnSelected) {
            GameManager.Instance.SwitchGameState(GameState.ENTITY_MOVE);
            //CameraManager.Instance.PanToSelectedEntity(0.25f);

            selectedEntity.GetMoveableTiles();

            radialEntityMenu.Minimize();
        }
        else {
            MapManager.Instance.ClearHighlightedTiles();
            GameManager.Instance.SwitchGameState(GameState.ENTITY_MENU);
        }
    }

    private void MeleeButtonPressed(bool btnSelected) {
        if (btnSelected) {
            //radialEntityMenu.ShowSubMenu(selectedEntity.Properties.attackDataObjects);
            GameManager.Instance.SwitchGameState(GameState.ENTITY_ATTACK);
            MapManager.Instance.HighlightCircle(selectedEntity.transform.position, selectedEntity.GetAttackByCommandType(CommandType.ATTACK).range, CommandType.ATTACK);
            radialEntityMenu.Minimize();
        }
        else {
            MapManager.Instance.ClearHighlightedTiles();
            GameManager.Instance.SwitchGameState(GameState.ENTITY_MENU);
        }
    }

    private void SkillButtonPressed(bool btnSelected) {
        if (btnSelected) {
            GameManager.Instance.SwitchGameState(GameState.ENTITY_ATTACK);
            Debug.Log("Needs to be switched to the equipped skill, not just a random skill in the attacks array");
            MapManager.Instance.HighlightCircle(selectedEntity.transform.position, selectedEntity.GetAttackByCommandType(CommandType.SKILL).range, CommandType.ATTACK);
            radialEntityMenu.Minimize();
        }
        else {
            MapManager.Instance.ClearHighlightedTiles();
            GameManager.Instance.SwitchGameState(GameState.ENTITY_MENU);
        }
    }

    private void ConfirmEndTurn(bool value) {
        YesNoPopup.RemoveEventListener(ConfirmEndTurn);
        if (value) {
            GameManager.Instance.EndTurn();
            radialEntityMenu.CloseMenu();
        }
    }

    #endregion BUTTON_CALLBACKS

    public void OpenMenuOnActive() {
        if (activeEntity != null) {
            EntityClicked(activeEntity);
        }
        else {
            Debug.LogWarning("OpenMenuOnActive() called without an active entity assigned");
        }
    }

    /// <summary>
    /// Assumes that there is a selectedEntity
    /// </summary>
    public void ShowEntityMenu() {
        if (selectedEntity != null) {
            radialEntityMenu.OpenOnEntity(selectedEntity);
        }
        else {
            Debug.LogWarning("Tried calling ShowEntityMenu() but there was no selectedEntity");
        }
    }

    /// <summary>
    /// Temporarily hides the menu. Used while dragging
    /// </summary>
    public void CloseRadialMenu() {
        if (selectedEntity != null) {
            radialEntityMenu.CloseMenu();
        }
    }

    public void SetRadialMenuVisible(bool value) {
        radialEntityMenu.SetVisible(value);
    }

    /// <summary>
    /// When this is called, it should be safe to assume that the entity has the property pressed available
    /// Because the UI buttons are shown based on the entity's properties
    /// </summary>
    /// <param craftableName="state"></param>
    public void EntityButtonPressed(CommandType commandType, bool btnSelected) {
        switch (commandType) {
            case CommandType.ATTACK:
                MeleeButtonPressed(btnSelected);
                break;
            case CommandType.MOVE:
                MoveButtonPressed(btnSelected);
                break;
            case CommandType.SKILL:
                SkillButtonPressed(btnSelected);
                break;
            case CommandType.BACK:
                DeselectCurrentEntity();
                break;
            case CommandType.END:
                YesNoPopup.Show("End Turn?", ConfirmEndTurn);
                break;
            case CommandType.NONE:
                break;
            default:
                break;
        }

        //switch (state) {
        //    case EntityState.ATTACKING:
        //        // TODO:REPLACE
        //        //radialEntityMenu.ShowSubMenu(selectedEntity.Properties.attackDataObjects);
        //        GameManager.Instance.SwitchGameState(GameState.ENTITY_ATTACK);
        //        MapManager.Instance.HighlightCircle(selectedEntity.transform.position, (command as Attack).range, CommandType.MELEE);
        //        break;
        //    case EntityState.MOVING:
        //        //if (toggleOn) {
        //        GameManager.Instance.SwitchGameState(GameState.ENTITY_MOVE);
        //        CameraManager.Instance.PanToActiveEntity(0.25f);

        //        selectedEntity.GetMoveableTiles();

                
        //        //}
        //        //else {
        //        //    GameManager.Instance.SwitchGameState(GameState.ENTITY_MENU);
        //        //}
        //        break;
        //}
    }

    //private void DisableButton(CommandType commandType) {
    //    // TODO:REPLACE
    //    Debug.LogWarning("Need to disable buttons!".Bold().Sized(15).Colored(Color.magenta));
    //    radialEntityMenu.SetButtonEnabled(commandType, false);
    //}

    public void EntityClicked(Entity entity) {
        if (selectedEntity != entity) {
            // If no entity is currently selected, or the clicked entity is not the selected one
            if (selectedEntity != null) {
                DeselectCurrentEntity();
            }

            selectedEntity = entity;
            selectedEntity.SetSelected(true);
            radialEntityMenu.OpenOnEntity(selectedEntity);
        }
    }

    /// <summary>
    /// Sets the selectedEntity to NOT selected (which stops the animation and indicator)
    /// and nulls the variable
    /// </summary>
    public void DeselectCurrentEntity() {
        if (selectedEntity != null) {
            CloseRadialMenu();
            selectedEntity.SetSelected(false);
            selectedEntity = null;
            //MSN.Instance.StartListening(EventName.ENTITY_MENU_CLOSE_COMPLETE, OnRadialEntityMenuClosed);
        }
        // TODO:REPLACE
        //radialEntityMenu.SetMenuActive(false);
    }

    public void MoveSelectedEntityTo(Tile tile) {
        if (selectedEntity.Properties.entityType == EntityType.PLAYER) {
            Vector3 pos = tile.transform.position;
            pos.z = 0;
            if (selectedEntity.MoveTo(pos)) {
                MapManager.Instance.ClearHighlightedTiles();
                CloseRadialMenu();
                //DisableButton(CommandType.MOVE);
            }
        }
    }

    public void GetNearbyPlayerEntities(Entity entity, ref List<Entity> nearbyPlayerEntities) {
        Entity playerEntity = null;
        float distance = 0;
        for (int i = 0; i < entityDatabase.PlayerEntities.Count; i++) {
            playerEntity = entityDatabase.PlayerEntities[i];
            distance = Mathf.Abs(Vector2.Distance(playerEntity.Position, entity.Position));

            if (entityDatabase.ValidateEntity(entity.TargetEntity)) {
                if ((entity.TargetEntity != null) && (entity.TargetEntity == playerEntity)) {
                    nearbyPlayerEntities.Add(playerEntity);
                }
            }
            else {
                entity.TargetEntity = null;
                //if (distance < ((entity.Properties.moveDistance * entity.Properties.visionMoveFactor) * MapManager.Instance.TileSize)) {
                nearbyPlayerEntities.Add(playerEntity);
                //}
            }
        }
    }

    /// <summary>
    /// Returns the targetEntity property of the enemy entity if it's not null. I.E. it'll always chase it's target
    /// </summary>
    /// <param craftableName="entity"></param>
    /// <returns></returns>
    public Entity GetNearestPlayerEntity(Entity entity) {
        Entity nearestPlayer = (entity.TargetEntity != null) ? entity.TargetEntity : null;
        // Not null checking cause it should've been done back in the AIController
        if (nearestPlayer == null) {
            float distance = 10000;
            float tempDistance = 0;
            for (int i = 0; i < entity.NearbyPlayerEntities.Count; i++) {
                tempDistance = Mathf.Abs(Vector3.Distance(entity.Position, entity.NearbyPlayerEntities[i].Position));
                if (tempDistance < distance) {
                    distance = tempDistance;
                    nearestPlayer = entity.NearbyPlayerEntities[i];
                }
            }
            entity.TargetEntity = nearestPlayer;
        }
        return nearestPlayer;
    }

    public bool SelectedIsActive {
        get {
            return (selectedEntity == activeEntity);
        }
    }

    public void KillEntity(Entity entity) {
        entityDatabase.UnregisterEntity(entity);
        GameManager.Instance.RemoveEntityFromTurnList(entity);
        MapManager.Instance.SetTileOccupied(entity, false);
        Destroy(entity.gameObject);
    }

    //public bool IsInRangeOfPlayerEntity(Entity entity) {
    //    bool rVal = false;
    //    Entity playerEntity = null;
    //    for (int i = 0; i < entityDatabase.PlayerEntities.Count; i++) {
    //        playerEntity = entityDatabase.PlayerEntities[i];
    //    }

    //    return rVal;
    //}

    #region CHEAT

    public void ResetAllCooldowns() {
        Entity entity = null;
        for (int i = 0; i < entityDatabase.AllEntities.Count; i++) {
            entity = entityDatabase.AllEntities[i];
            entity.ResetCooldowns();
            // TODO:REPLACE
            //radialEntityMenu.SetButtons(activeEntity);
            // TODO:REPLACE
            //radialEntityMenu.RefreshAll();
        }
    }

    #endregion CHEAT

    #endregion PUBLIC_METHODS

    void Update() {

    }
}
