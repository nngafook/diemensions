﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[RequireComponent(typeof(CameraDragController))]
public class CameraManager : MonoBehaviour {

    // Pixel Perfect Orthographic size formula
    // Camera Size = y / (2 * pixelsPerUnit)
    // y = Screen Height (px)

    #region SINGLETON
    private static CameraManager instance = null;
    public static CameraManager Instance {
        get {
            return instance;
        }
    }
    #endregion SINGLETON

    [HideInInspector]
    public UnityAction TweenToEntityComplete;

    private float panSpeed = 0.5f;
    //private float mapWidth = 0;
    //private float mapHeight = 0;
    private float distanceFromActiveEntity;
    private bool dragIsLocked = false;
    private bool isTweening = false;
    //private bool isDragging = false;
    private bool isFollowingEntity = false;
    //private Vector3 dragStartPosition;
    //private Vector3 targetPos;
    private Entity entityToFollow;
    private CameraDragController dragController;

    public bool DragIsLocked { get { return dragIsLocked; } }
    public bool IsTweening { get { return isTweening; } }

    #region PRIVATE_METHODS
    void Awake() {
        instance = this;
        dragController = this.GetComponent<CameraDragController>();
    }

    void Start() {
        CheckDistanceFromActiveEntity();
    }

    private void SnapToPlayerComplete() {
        MSN.Instance.TriggerEvent(EventName.CAMERA_SNAP_TO_PLAYER_COMPLETE);
        TweenComplete();
        CheckDistanceFromActiveEntity();
    }

    private void CheckDistanceFromActiveEntity() {
        distanceFromActiveEntity = Mathf.Abs(Vector2.Distance(transform.position, EntityManager.Instance.ActiveEntity.transform.position));

        if ((!UIManager.Instance.AlertButtonVisible) && (distanceFromActiveEntity > MapManager.Instance.TileSize)) {
            UIManager.Instance.ShowAlertButton();
        }
        else if ((UIManager.Instance.AlertButtonVisible) && (distanceFromActiveEntity <= MapManager.Instance.TileSize)) {
            UIManager.Instance.HideAlertButton();
        }
    }

    private void TweenComplete() {
        isTweening = false;
        if (TweenToEntityComplete != null) {
            TweenToEntityComplete.Invoke();
        }
    }

    private void ProcessFollow() {
        if (isFollowingEntity) {
            Vector3 pos = entityToFollow.Position;
            pos.z = transform.position.z;
            transform.position = pos;
        }
    }

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    public void SetDraggingLocked(bool value) {
        dragIsLocked = value;
    }

    public void BeginDrag() {
        if ((!dragIsLocked) && (GameManager.Instance.IsInPlayerPhase) && (!GameManager.Instance.EntityMoving)) {
            if (dragController.BeginDrag((int)MapManager.Instance.MapWidth, (int)MapManager.Instance.MapHeight)) {
                //EntityManager.Instance.CloseRadialMenu();
            }
        }
    }

    public void EndDrag() {
        if (GameManager.Instance.IsInPlayerPhase) {
            dragController.EndDrag();
            CheckDistanceFromActiveEntity();
            //Debug.Log("End Drag No Longer showing entity menu");
            //EntityManager.Instance.ShowEntityMenu();
        }
    }

    public void ProcessDrag() {
        if ((GameManager.Instance.IsInPlayerPhase) && (!isFollowingEntity) && (!isTweening) && (dragController.IsDragging)) {
            dragController.ProcessDrag();

            CheckDistanceFromActiveEntity();
        }
    }

    public void SetToFollow(Entity entity) {
        isFollowingEntity = true;
        entityToFollow = entity;
    }

    public void StopFollowing(Vector3 pos) {
        if (isFollowingEntity) {
            isFollowingEntity = false;
            entityToFollow = null;
        }
        transform.position = pos;
    }

    public void PanTo(Vector3 pos, float duration) {
        if ((!isTweening) && (!dragController.IsDragging)) {
            pos.z = transform.position.z;
            float distance = Mathf.Abs(Vector2.Distance(pos, transform.position));
            if (distance > MapManager.Instance.TileSize) {
                transform.DOMove(pos, duration, true).OnComplete(OnPanToPositionComplete);
            }
            else {
                OnPanToPositionComplete();
            }
        }
    }

    private void OnPanToPositionComplete() {
        MSN.Instance.TriggerEvent(EventName.CAMERA_PAN_TO_POSITION_COMPLETE);
    }

    public void SnapToPlayer() {
        if ((!isTweening) && (!dragController.IsDragging)) {
            Vector3 pos = EntityManager.Instance.ActiveEntityPosition;
            pos.z = transform.position.z;
            isTweening = true;
            transform.DOMove(pos, panSpeed).OnComplete(SnapToPlayerComplete).SetEase(Ease.Linear);
        }
    }

    public void PanToActiveEntity(UnityAction callback) {
        if ((!isTweening) && (!dragController.IsDragging)) {
            Vector3 pos = EntityManager.Instance.ActiveEntityPosition;
            pos.z = transform.position.z;
            isTweening = true;
            if (callback != null) {
                TweenToEntityComplete = callback;
            }
            transform.DOMove(pos, panSpeed).OnComplete(TweenComplete);
        }
    }

    public void PanToActiveEntity(float speed = 0) {
        if ((!isTweening) && (!dragController.IsDragging)) {
            CheckDistanceFromActiveEntity();
            if (distanceFromActiveEntity > MapManager.Instance.TileSize) {
                speed = (speed == 0) ? panSpeed : speed;
                Vector3 pos = EntityManager.Instance.ActiveEntityPosition;
                pos.z = transform.position.z;
                isTweening = true;
                transform.DOMove(pos, speed).OnComplete(TweenComplete);
            }
        }
    }

    public void PanToSelectedEntity(float speed = 0) {
        if ((!isTweening) && (!dragController.IsDragging)) {
            speed = (speed == 0) ? panSpeed : speed;
            Vector3 pos = EntityManager.Instance.SelectedEntity.Position;
            pos.z = transform.position.z;
            isTweening = true;
            transform.DOMove(pos, speed).OnComplete(TweenComplete);
        }
    }
    #endregion PUBLIC_METHODS

    void LateUpdate() {
        ProcessDrag();
        ProcessFollow();
    }
}
