﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    #region SINGLETON
    private static UIManager instance = null;
    public static UIManager Instance {
        get {
            return instance;
        }
    }
    #endregion SINGLETON

    private string enemiesCounterPrefix = "ENEMIES LEFT: ";
    private bool alertButtonVisible = true;

    public Button alertButton;
    public Text enemiesCounterText;

    public CanvasGroup toggleableUIGroup;
    public TurnOrderList turnOrderList;

    [Header("Popups")]
    public LevelStartPopup levelStartPopup;

    public bool AlertButtonVisible { get { return alertButtonVisible; } }

    #region PRIVATE_METHODS
    void Awake() {
        instance = this;

        alertButton.onClick.AddListener(OnAlertButtonClicked);
    }

    void Start() {
        GameManager.Instance.EnterStateEvent.AddListener(OnEnterState);
        GameManager.Instance.ExitStateEvent.AddListener(OnExitState);
    }

    private void OnEnterState() {
        GameState gameState = GameManager.Instance.CurrentGameState;
        switch (gameState) {
            case GameState.FREE_ROAM:
                //ShowAlertButton();
                break;
            case GameState.ENTITY_MENU:

                break;
            case GameState.ENTITY_MOVE:

                break;
            case GameState.ENTITY_ATTACK:

                break;
        }
    }

    private void OnExitState() {
        GameState gameState = GameManager.Instance.CurrentGameState;
        switch (gameState) {
            case GameState.FREE_ROAM:
                //HideAlertButton();
                break;
            case GameState.ENTITY_MENU:

                break;
            case GameState.ENTITY_MOVE:

                break;
            case GameState.ENTITY_ATTACK:

                break;
        }
    }

    #region BUTTON_CLICKS

    private void OnAlertButtonClicked() {
        CameraManager.Instance.SnapToPlayer();
    }

    #endregion BUTTON_CLICKS

    #region BUTTON_ENABLERS

    private void SetButtonActive(Button btn, bool value) {
        CanvasGroup cg = btn.GetComponent<CanvasGroup>();
        if (cg == null) {
            Debug.LogWarning(btn.name + " does not have a canvas group");
        }
        cg.alpha = value.AsInt();
        cg.interactable = value;
        cg.blocksRaycasts = value;
    }

    #endregion BUTTON_ENABLERS

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    public void ShowAlertButton() {
        alertButtonVisible = true;
        SetButtonActive(alertButton, alertButtonVisible);
    }

    public void HideAlertButton() {
        alertButtonVisible = false;
        SetButtonActive(alertButton, alertButtonVisible);
    }

    public void SetMainUIVisible(bool value) {
        toggleableUIGroup.alpha = value.AsInt();
        toggleableUIGroup.blocksRaycasts = value;
        toggleableUIGroup.interactable = value;
        EntityManager.Instance.SetRadialMenuVisible(value);
    }

    public void UpdateEnemiesCounter(int current, int total) {
        enemiesCounterText.text = enemiesCounterPrefix + current + " / " + total;
    }

    public void TurnEnded() {
        turnOrderList.TurnEnded();
    }

    public void RemoveEntity(UID uid) {
        turnOrderList.RemoveAt(uid);
    }

    public void AddTurnOrderItem(Entity entity) {
        turnOrderList.AddEntity(entity);
    }

    #endregion PUBLIC_METHODS

    void Update() {

    }

}
