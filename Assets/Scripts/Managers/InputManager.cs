﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputManager : MonoBehaviour {

    private struct MouseProperties {
        public Vector2 downScreenPosition;
        public Vector2 upScreenPosition;
        public Vector2 dragBeginScreenPosition;

        public Vector2 DragBeginPosition { get { return dragBeginScreenPosition; } }
        public Vector2 DragDelta { get { return (Vector2)Input.mousePosition - downScreenPosition; } }
        public float DragDistance { get { return Vector2.Distance((Vector2)Input.mousePosition, downScreenPosition); } }
        public bool DragVertical { get { return (DragDelta.x < DragDelta.y); } }
        public bool DragHorizontal { get { return (DragDelta.x >= DragDelta.y); } }
    }

    private enum MouseState {
        DOWN,
        DRAGGING,
        UP,

        NONE = 99
    }

    private static InputManager instance = null;
    public static InputManager Instance { get { return instance; } }

    private MouseState currentMouseState = MouseState.NONE;
    private MouseProperties mouseProperties = new MouseProperties();

    private int fingerID = -1;
    private float lastMouseUpTime = 0;
    private float doubleClickThreshold = 0.4f;
    private float dragValidThreshold = 10;

    public Vector2 MouseDownPosition { get { return mouseProperties.downScreenPosition; } }

    #region PRIVATE_METHODS
    void Awake() {
        #if !UNITY_EDITOR
            fingerID = 0;
        #endif
        instance = this;
    }

    private void ProcessMouseDown() {
        if ((currentMouseState == MouseState.NONE) && (Input.GetMouseButtonDown(0)) && ((EventSystem.current != null) && (!EventSystem.current.IsPointerOverGameObject(fingerID)))) {
            currentMouseState = MouseState.DOWN;
            mouseProperties.downScreenPosition = Input.mousePosition;
        }
    }

    private void ProcessMouseDrag() {
        if ((currentMouseState == MouseState.DOWN) && (mouseProperties.DragDistance > dragValidThreshold)) {
            currentMouseState = MouseState.DRAGGING;
            mouseProperties.dragBeginScreenPosition = (Vector2)Input.mousePosition;
            CameraManager.Instance.BeginDrag();
        }
    }

    private void ProcessMouseDragUp() {
        if ((currentMouseState == MouseState.DRAGGING) && (Input.GetMouseButtonUp(0))) {
            CameraManager.Instance.EndDrag();
        }
    }

    private void ProcessMouseUp() {
        if (Input.GetMouseButtonUp(0)) {
            if ((!EventSystem.current.IsPointerOverGameObject(fingerID)) && (currentMouseState != MouseState.DRAGGING)) {
                GameState state = GameManager.Instance.CurrentGameState;
                switch (state) {
                    case GameState.FREE_ROAM:

                        break;
                    case GameState.ENTITY_MOVE:
                        if ((currentMouseState == MouseState.DOWN) && (EntityManager.Instance.SelectedIsActive)) {
                            Tile tile = MapManager.Instance.GetTileAtMousePosition();
                            if (tile != null) {
                                EntityManager.Instance.ActiveEntity.TileClicked(tile);
                            }
                        }
                        break;
                    case GameState.ENTITY_ATTACK:
                        // When attackDataObjects become aoe's and targetable on tiles and hit the area around it, 
                        // this needs to change to check the tile and not the entity
                        if ((currentMouseState == MouseState.DOWN) && (EntityManager.Instance.SelectedIsActive)) {
                            Tile clickedTile = MapManager.Instance.GetTileAtMousePosition();
                            if (MapManager.Instance.HighlightedTiles.Contains(clickedTile)) {
                                Entity targetEntity = MapManager.Instance.EntityAtMousePosition();
                                //if ((FogManager.Instance.FogRevealedAt(Utility.MouseWorldPosition())) && (targetEntity != null)) {
                                if (targetEntity != null) {
                                    ConfirmActionPopup.Show();
                                    StartCoroutine(WaitForAttackConfirm(targetEntity));
                                }
                            }
                        }
                        break;
                }
            }

            if ((currentMouseState == MouseState.DOWN)) {
                if ((Time.time - lastMouseUpTime) < doubleClickThreshold) {
                    DoubleClicked();
                    lastMouseUpTime = 0;
                }
                else {
                    lastMouseUpTime = Time.time;
                }
            }
            mouseProperties.upScreenPosition = Input.mousePosition;
            currentMouseState = MouseState.NONE;
        }
    }

    private void DoubleClicked() {
        EventManager.Instance.DoubleClick();
    }

    //private IEnumerator WaitForMoveConfirm(Tile tile) {
    //    while (!ConfirmActionPopup.DecisionMade) {
    //        yield return null;
    //    }

    //    if (ConfirmActionPopup.Result) {
    //        EntityManager.Instance.MoveSelectedEntityTo(tile);
    //    }
    //}

    private IEnumerator WaitForAttackConfirm(Entity targetEntity) {
        Debug.Log("Change this to MSN".Bold().Colored(Color.magenta));
        while (!ConfirmActionPopup.DecisionMade) {
            yield return null;
        }

        if (ConfirmActionPopup.Result) {
            GameManager.Instance.InvokePlayerAttack(targetEntity);
        }
    }

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    public void EntityClickedAsButton(Entity entity) {
        if ((!EventSystem.current.IsPointerOverGameObject(fingerID)) && (currentMouseState != MouseState.DRAGGING) && (GameManager.Instance.IsInPlayerPhase) && (GameManager.Instance.CurrentGameState != GameState.ENTITY_ATTACK)) {
            //FogTile fogTile = FogManager.Instance.GetTileFromPosition(entity.Position);
            //if ((!fogTile.IsFaded) && (fogTile.IsRevealed)) {
                EntityManager.Instance.EntityClicked(entity);
            //}
        }
    }

    #endregion PUBLIC_METHODS

    void Update() {
        ProcessMouseDown();
        ProcessMouseDrag();
        ProcessMouseDragUp();
        ProcessMouseUp();
    }
}
