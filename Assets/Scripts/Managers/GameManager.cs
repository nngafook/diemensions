﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    #region SINGLETON
    private static GameManager instance = null;
    public static GameManager Instance {
        get {
            return instance;
        }
    }
    #endregion SINGLETON

    [HideInInspector]
    public UnityEvent EnterStateEvent;
    [HideInInspector]
    public UnityEvent ExitStateEvent;

    [HideInInspector]
    public UnityEvent EnterPhaseEvent;
    [HideInInspector]
    public UnityEvent ExitPhaseEvent;

    private const string PLAYER_PHASE_STRING = "PLAYER PHASE";
    private const string ENEMY_PHASE_STRING = "ENEMY PHASE";

    private GameState currentGameState = GameState.NONE;
    private GamePhase currentGamePhase = GamePhase.NONE;

    private List<Entity> masterTurnOrderList = new List<Entity>();

    // Index of the current entity whose turn it is.
    private int currentTurnIndex = 0;
    // If a player entity is moving, this is true
    private bool isEntityMoving = false;

    // Total enemies in level.
    private int totalEnemiesSpawned;
    private int totalEnemiesLeft;

    #region ACCESSORS_MUTATORS
    public GameState CurrentGameState { get { return currentGameState; } }
    public GamePhase CurrentGamePhase { get { return currentGamePhase; } }
    public bool IsInPlayerPhase { get { return (currentGamePhase == GamePhase.PLAYER); } }
    public bool EntityMoving { get { return isEntityMoving; } set { isEntityMoving = value; } }
    #endregion ACCESSORS_MUTATORS

    #region PRIVATE_METHODS
    void Awake() {
        instance = this;
    }

    void Start() {
        ScreenFader.SnapToBlack(0.7f);
        UIManager.Instance.SetMainUIVisible(false);
        MapManager.Instance.Init();
        EntityManager.Instance.SetOrderList(ref masterTurnOrderList);
        SetNewActiveEntity();
        totalEnemiesSpawned = MapManager.Instance.TotalEnemiesSpawned;
        totalEnemiesLeft = totalEnemiesSpawned;
        SetOrderIcons();
        UpdateEnemiesCounterText();
        MSN.Instance.StartListening(EventName.START_LEVEL, OnStartLevelClicked);
    }

    private void OnStartLevelClicked() {
        MSN.Instance.StopListening(EventName.START_LEVEL, OnStartLevelClicked);
        UIManager.Instance.SetMainUIVisible(true);
        SwitchGamePhase(GamePhase.PLAYER);
    }

    public void SetOrderIcons() {
        Debug.Log("CHANGE THIS BACK FROM PUBLIC".Bold().Colored(Color.magenta));
        Entity entity = null;
        for (int i = 0; i < masterTurnOrderList.Count; i++) {
            entity = masterTurnOrderList[i];
            
            UIManager.Instance.AddTurnOrderItem(entity);
        }
    }

    private void UpdateEnemiesCounterText() {
        UIManager.Instance.UpdateEnemiesCounter(totalEnemiesLeft, totalEnemiesSpawned);
    }

    private void OnSelectedAttackComplete() {
        MSN.Instance.StopListening(EventName.ATTACK_COMPLETE, OnSelectedAttackComplete);
        EntityManager.Instance.ShowEntityMenu();
    }

    #region STATES
    private void ExitState() {
        switch (currentGameState) {
            case GameState.FREE_ROAM:
                break;
            case GameState.ENTITY_MENU:
                break;
            case GameState.ENTITY_MOVE:
                break;
            case GameState.ENTITY_ATTACK:
                break;
            case GameState.NONE:
                break;
            default:
                break;
        }
        if (ExitStateEvent != null) {
            ExitStateEvent.Invoke();
        }
    }

    private void EnterState() {
        switch (currentGameState) {
            case GameState.FREE_ROAM:
                break;
            case GameState.ENTITY_MENU:
                break;
            case GameState.ENTITY_MOVE:
                break;
            case GameState.ENTITY_ATTACK:
                break;
            case GameState.NONE:
                break;
            default:
                break;
        }
        if (EnterStateEvent != null) {
            EnterStateEvent.Invoke();
        }
    }

    private void EnterPhase() {
        MSN.Instance.StartListening(EventName.PHASE_MESSAGE_COMPLETE, OnPhaseMessageComplete);
        switch (currentGamePhase) {
            case GamePhase.PLAYER:
                DebugPunchTurnMessage(PLAYER_PHASE_STRING);
                DebugPunchTurnMessage(EntityManager.Instance.ActiveEntity.Properties.name + "'s Turn");
                break;
            case GamePhase.ENEMY:
                DebugPunchTurnMessage(ENEMY_PHASE_STRING);
                break;
            case GamePhase.BOSS:
                break;
            case GamePhase.NEUTRAL:
                break;
            case GamePhase.NONE:
                break;
            default:
                break;
        }
        if (EnterPhaseEvent != null) {
            EnterPhaseEvent.Invoke();
        }
    }

    private void ExitPhase() {
        CameraManager.Instance.SetDraggingLocked(true);
        EntityManager.Instance.UpdateEntityStatuses();
        switch (currentGamePhase) {
            case GamePhase.PLAYER:
                break;
            case GamePhase.ENEMY:
                break;
            case GamePhase.BOSS:
                break;
            case GamePhase.NEUTRAL:
                break;
            case GamePhase.NONE:
                break;
            default:
                break;
        }
        if (ExitPhaseEvent != null) {
            ExitPhaseEvent.Invoke();
        }
    }

    #endregion STATES

    #region TURNS
    private void IncrementTurnIndex() {
        currentTurnIndex++;
        if (currentTurnIndex >= masterTurnOrderList.Count) {
            currentTurnIndex = 0;
        }
    }

    private void SetNewActiveEntity() {
        EntityManager.Instance.ActiveEntity = masterTurnOrderList[currentTurnIndex];
        bool phaseChanged = CheckForNewTurnPhase();

        if (currentGamePhase == GamePhase.ENEMY) {
            Entity enemy = EntityManager.Instance.ActiveEntity;
            if (phaseChanged) {
                CameraManager.Instance.PanToActiveEntity();
            }
            else {
                CameraManager.Instance.PanToActiveEntity(PanToActiveEntityComplete);
            }
        }
        else {
            CameraManager.Instance.SnapToPlayer();
        }
    }

    private void PanToActiveEntityComplete() {
        CameraManager.Instance.TweenToEntityComplete -= PanToActiveEntityComplete;

        EntityManager.Instance.ActiveEntity.TurnBegin();
    }

    private void DebugPunchTurnMessage(string message) {
        MessageNotifier.PushMessage(message);
    }

    private bool CheckForNewTurnPhase() {
        bool rVal = false;
        EntityType activeEntityType = EntityManager.Instance.ActiveEntity.Properties.entityType;
        switch (currentGamePhase) {
            case GamePhase.PLAYER:
                if (activeEntityType == EntityType.ENEMY) {
                    SwitchGamePhase(GamePhase.ENEMY);
                    rVal = true;
                }
                break;
            case GamePhase.ENEMY:
                if (activeEntityType == EntityType.PLAYER) {
                    Debug.Log("TODO: Remove this resource refill hack".Colored(Color.magenta));
                    EntityManager.Instance.ActiveEntity.entityStatus.currentResource = EntityManager.Instance.ActiveEntity.Properties.maxResource;
                    SwitchGamePhase(GamePhase.PLAYER);
                    rVal = true;
                }
                break;
            case GamePhase.BOSS:
                Debug.Log("CheckForNewTurnPhase() - Boss Phase is the current Game State");
                break;
        }
        return rVal;
    }
    #endregion TURNS

    #region EVENTS

    private void OnPhaseMessageComplete() {
        MSN.Instance.StopListening(EventName.PHASE_MESSAGE_COMPLETE, OnPhaseMessageComplete);
        CameraManager.Instance.SetDraggingLocked(false);

        if (currentGamePhase == GamePhase.PLAYER) {
            EntityManager.Instance.OpenMenuOnActive();
        }
        else {
            EntityManager.Instance.ActiveEntity.TurnBegin();
        }
    }

    #endregion EVENTS

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS
    public void BackToMenu() {
        SceneManager.LoadScene("MainMenu");
    }

    public void SwitchGameState(GameState state) {
        if (currentGameState != state) {
            ExitState();
            currentGameState = state;
            EnterState();
        }
    }

    public void SwitchGamePhase(GamePhase phase) {
        if (currentGamePhase != phase) {
            ExitPhase();
            currentGamePhase = phase;
            EnterPhase();
        }
    }

    #region COMMANDS

    public void InvokePlayerAttack(Entity targetEntity) {
        Debug.Log("Should this maybe be in EntityManager instead of GameManager? WHO KNOWS!?!".Bold().Colored(CustomColor.GetColor(ColorName.PURPLE_PLUM)));

        CommandType commandType = EntityManager.Instance.SelectedButton.CommandType;
        Attack selectedAttack = EntityManager.Instance.SelectedEntity.GetAttackByCommandType(commandType);
        MapManager.Instance.ClearHighlightedTiles();
        EntityManager.Instance.CloseRadialMenu();

        StartCoroutine(InvokePlayerAttackProcess(targetEntity, selectedAttack));
    }

    private IEnumerator InvokePlayerAttackProcess(Entity targetEntity, Attack attack) {
        Entity attackingEntity = EntityManager.Instance.SelectedEntity;
        MSN.Instance.StartListening(EventName.ATTACK_COMPLETE, OnSelectedAttackComplete);
        if (currentGamePhase != GamePhase.PLAYER) {
            Debug.LogWarning("InvokePlayerAttackProcess was called when it wasn't the PLAYER game phase".Bold().Sized(15).Colored(Color.red));
        }
        AttackDialogNotifier.Show(attackingEntity.Top, attack.commandName);
        yield return StartCoroutine(AttackDialogNotifier.WaitForComplete());
        attack.Invoke(targetEntity);
    }

    #endregion COMMANDS

    /// <summary>
    /// When an entity is killed in the gameplay, it is removed from the entityDatabase, and then removed from here
    /// so the turn order is updated, and they don't get a turn, cause they're dead.
    /// </summary>
    /// <param craftableName="entity"></param>
    public void RemoveEntityFromTurnList(Entity entity) {
        if (entity.entityPropertiesData.entityType != EntityType.PLAYER) {
            totalEnemiesLeft--;
            UpdateEnemiesCounterText();
        }
        masterTurnOrderList.Remove(entity);
        UIManager.Instance.RemoveEntity(entity.uid);
    }

    public void EndTurn() {
        UIManager.Instance.TurnEnded();
        // Resets the active entity's status. Should be a player entity, since the enemy's end turns are handled elsewhere
        EntityManager.Instance.ActiveEntity.TurnEnded();
        // Drops the animation, Resets the scale, sets bool to false
        EntityManager.Instance.DeselectCurrentEntity();

        EntityManager.Instance.UpdateEntityStatuses();

        IncrementTurnIndex();
        SetNewActiveEntity();
    }

    #endregion PUBLIC_METHODS

    void Update() {

    }
}
