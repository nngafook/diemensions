﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InventoryManager : MonoBehaviour {

    private static InventoryManager instance = null;
    public static InventoryManager Instance { get { return instance; } }

    private bool inventoryOpen = false;

    public InventoryWindow inventoryWindow;

    private Dictionary<string, int> componentsInventory;


	#region PRIVATE_METHODS
	void Awake() {
        instance = this;
        componentsInventory = new Dictionary<string, int>();
	}
	
	void Start () {
	
	}
	#endregion PRIVATE_METHODS

	#region PUBLIC_METHODS

    public void AddComponentToInventory(string id, int amount) {
        Debug.Log("Adding id: " + id);
        if (componentsInventory.ContainsKey(id)) {
            componentsInventory[id] += amount;
            inventoryWindow.AddToItem(id, amount);
        }
        else {
            componentsInventory[id] = amount;
            inventoryWindow.AddItem(id, amount);
        }
    }

    public void ToggleInventory() {
        inventoryOpen = !inventoryOpen;

        inventoryWindow.SetOpen(inventoryOpen);
    }
	#endregion PUBLIC_METHODS

	void Update () {
	
	}
}
