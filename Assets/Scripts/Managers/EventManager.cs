﻿using UnityEngine;

public class EventManager : MonoBehaviour {

    #region SINGLETON
    private static EventManager instance = null;
    public static EventManager Instance {
        get {
            return instance;
        }
    }
    #endregion SINGLETON

    #region EVENTS
    public delegate void OnDoubleClickEvent();
    public event OnDoubleClickEvent OnDoubleClick;
    #endregion EVENTS

    #region PRIVATE_METHODS
    void Awake() {
        instance = this;
    }

    void Start() {

    }
    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS
    public void DoubleClick() {
        if (OnDoubleClick != null) {
            OnDoubleClick();
        }
    }
    #endregion PUBLIC_METHODS

    void Update() {

    }
}
