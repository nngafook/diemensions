﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour {

    #region SINGLETON
    private static MapManager instance = null;
    public static MapManager Instance {
        get {
            return instance;
        }
    }
    #endregion SINGLETON

    private List<Tile> highlightedTiles = new List<Tile>();
    private LayerMask wallTileMask;

    //private Tile[] moveableTiles;
    //private Tile[] targetableTiles;

    public MapGenerator mapGenerator;
    public Transform playerPrefab;

    #region ACCESSORS_MUTATORS

    public Map CurrentMap { get { return mapGenerator.CurrentMap; } }
    public List<Tile> HighlightedTiles { get { return highlightedTiles; } set { highlightedTiles = value; } }
    public float TileSize { get { return mapGenerator.TileSize; } }
    public float MapWidth { get { return CurrentMap.mapSize.column * TileSize; } }
    public float MapHeight { get { return CurrentMap.mapSize.row * TileSize; } }
    public int TotalEnemiesSpawned { get { return mapGenerator.TotalEnemiesSpawned; } }
    public List<Tile> AllTiles { get { return mapGenerator.AllTiles; } }
    #endregion ACCESSORS_MUTATORS

    #region PRIVATE_METHODS
    void Awake() {
        instance = this;
        wallTileMask = 1 << LayerMask.NameToLayer("WallTile");
    }

    void Start() {
        // Does nothing cause the Game Manager calls Init
    }

    public void Init() {
        Instantiate(playerPrefab, playerPrefab.position, Quaternion.identity);
        mapGenerator.GenerateMap();
        //FogManager.Instance.mapSize = new GridCoord(CurrentMap.mapSize.column, CurrentMap.mapSize.row);
        //FogManager.Instance.GenerateMap();
    }

    /////////////////////// TODO: THIS IS THE SAME AS HighlightCircle YOU FOOL //////////////////////
    private void GetCircleOfTiles(Tile centerTile, int radius, ref List<Tile> rVal) {
        int iter = 0;
        int currentRow = centerTile.GridPos.row;
        int currentColumn = centerTile.GridPos.column;
        Tile currentTile = GetTileFromGridCoord(currentColumn, currentRow);

        // Fill the row
        iter = radius;
        for (int i = -iter; i <= iter; i++) {
            currentColumn = centerTile.GridPos.column + i;
            currentTile = GetTileFromGridCoord(currentColumn, currentRow);
            if (currentTile != null) {
                rVal.Add(currentTile);
            }
        }

        iter = 0;
        currentRow = centerTile.GridPos.row + radius;
        currentColumn = centerTile.GridPos.column;

        // Top half
        while (currentRow != centerTile.GridPos.row) {
            for (int i = -iter; i <= iter; i++) {
                currentColumn = centerTile.GridPos.column + i;
                currentTile = GetTileFromGridCoord(currentColumn, currentRow);
                if (currentTile != null) {
                    rVal.Add(currentTile);
                }
            }
            iter++;
            currentRow--;
        }

        currentRow = centerTile.GridPos.row - radius;
        currentColumn = centerTile.GridPos.column;
        iter = 0;

        // Bottom half
        while (currentRow != centerTile.GridPos.row) {
            for (int i = -iter; i <= iter; i++) {
                currentColumn = centerTile.GridPos.column + i;
                currentTile = GetTileFromGridCoord(currentColumn, currentRow);
                if (currentTile != null) {
                    rVal.Add(currentTile);
                }
            }
            iter++;
            currentRow++;
        }
    }

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    public Tile GetTileFromPosition(Vector3 pos) {
        return mapGenerator.GetTileFromPosition(pos);
    }

    public Tile GetTileFromGridCoord(GridCoord coord) {
        return mapGenerator.GetTileFromGridCoord(coord.column, coord.row);
    }

    public Tile GetTileFromGridCoord(int column, int row) {
        return mapGenerator.GetTileFromGridCoord(column, row);
    }

    public Tile GetTileAtMousePosition() {
        return GetTileFromPosition(Utility.MouseWorldPosition());
    }

    public Tile GetTileFromNode(Node node) {
        return GetTileFromPosition(node.worldPosition);
    }

    public bool IsMoveableTile(Tile tile) {
        bool rVal = false;
        if (highlightedTiles.Contains(tile)) {
            rVal = true;
        }
        return rVal;
    }

    public void SetTileOccupied(Entity entity, bool value) {
        Tile tile = GetTileFromPosition(entity.Position);
        if (tile != null) {
            tile.SetOccupied(value, entity);
        }
    }

    /// <summary>
    /// Iterate through the moveable tiles and make sure the entity has a path to every tile
    /// Uses the highlighted tiles in the MapManager (exclusive for player entities)
    /// </summary>
    public void ValidateMoveableTilePaths(Entity entity) {
        Tile startTile = GetTileFromPosition(entity.Position);
        Tile endTile = null;
        List<Tile> tempList = new List<Tile>();

        for (int i = 0; i < highlightedTiles.Count; i++) {
            endTile = highlightedTiles[i];
            if (GetPathInHighlighted(startTile, endTile, highlightedTiles).Count > 0) {
                tempList.Add(endTile);
            }
        }

        RefreshNewHighlightedTiles(ref tempList, CommandType.MOVE);
    }

    /// <summary>
    /// Iterate through the moveable tiles and make sure the entity has a path to every tile
    /// Uses the tiles passed in from the AIController (exclusive for ai entities)
    /// </summary>
    public void ValidateMoveableTilePaths(Entity entity, ref List<Tile> tiles) {
        Tile startTile = GetTileFromPosition(entity.Position);
        Tile endTile = null;
        List<Tile> tempList = new List<Tile>();

        for (int i = 0; i < tiles.Count; i++) {
            endTile = tiles[i];
            if (GetPathInHighlighted(startTile, endTile, tiles).Count > 0) {
                tempList.Add(endTile);
            }
        }
        tiles = new List<Tile>(tempList);
    }

    /// <summary>
    /// Called when an entity ends their movement and lands on the destination tile.
    /// Should be used for setting occupied, checking move end actions, like chests or any searching skills (if those exist)
    /// </summary>
    /// <param craftableName="entity"></param>
    public void EntityLandedOnTile(Entity entity) {
        Tile tile = GetTileFromPosition(entity.Position);
        SetTileOccupied(entity, true);
        if (tile.IsInteractiveOccupied) {
            InteractiveEntity interactiveEntity = tile.InteractiveEntity;
            Debug.Log((interactiveEntity.Reward).Bold().Colored(CustomColor.GetColor(ColorName.PURPLE_PLUM)));
            tile.RemoveInteractiveEntity();
        }
    }

    public Entity EntityAtMousePosition() {
        Entity rVal = null;
        Tile tile = GetTileAtMousePosition();
        if ((tile != null) && (tile.IsOccupied)) {
            rVal = tile.EntityOccupying;
        }
        return rVal;
    }

    public Entity EntityAtPosition(Vector3 position) {
        Entity rVal = null;
        Tile tile = GetTileFromPosition(position);
        if ((tile != null) && (tile.IsOccupied)) {
            rVal = tile.EntityOccupying;
        }
        return rVal;
    }

    public List<Tile> GetMoveableTiles(Entity entity) {
        List<Tile> rVal = new List<Tile>();
        Tile centerTile = GetTileFromPosition(entity.Position);
        int moveRange = entity.Properties.moveDistance;
        GetCircleOfTiles(centerTile, moveRange, ref rVal);
        return rVal;
    }

    public List<Tile> GetNeighbours(Tile tile) {
        List<Tile> rVal = new List<Tile>();
        for (int x = -1; x <= 1; x++) {
            for (int y = -1; y <= 1; y++) {
                if (x == 0 && y == 0) {
                    continue;
                }

                int checkColumn = tile.GridPos.column + x;
                int checkRow = tile.GridPos.row + y;
                if (Utility.IsWithinGrid(checkColumn, checkRow, (int)MapWidth, (int)MapHeight)) {
                    rVal.Add(GetTileFromGridCoord(checkColumn, checkRow));
                }
            }
        }
        return rVal;
    }

    public List<Tile> GetPathInRegion(List<Tile> tileRegion, Tile startTile, Tile endTile) {
        return mapGenerator.Pathfinder.GetPathInRegion(tileRegion, startTile, endTile);
    }

    public List<Tile> GetPathInHighlighted(Tile startTile, Tile endTile, List<Tile> tiles) {
        return mapGenerator.Pathfinder.GetPathInRegion(tiles, startTile, endTile);
    }

    public List<Tile> GetPathInHighlighted(Vector3 startPos, Vector3 endPos) {
        return mapGenerator.Pathfinder.GetPathInRegion(highlightedTiles, GetTileFromPosition(startPos), GetTileFromPosition(endPos));
    }

    public List<Tile> GetPathInRegion(Entity entity, Vector3 startPos, Vector3 endPos) {
        List<Tile> tileRegion = GetMoveableTiles(entity);
        Tile startTile = GetTileFromPosition(startPos);
        Tile endTile = GetTileFromPosition(endPos);
        return mapGenerator.Pathfinder.GetPathInRegion(tileRegion, startTile, endTile);
    }

    public List<Tile> GetPathBetweenTiles(Tile startTile, Tile endTile) {
        return mapGenerator.Pathfinder.GetPathBetweenTiles(mapGenerator.AllTiles, startTile, endTile);
    }

    public bool HasLineOfSightToTile(Vector2 posA, Tile tile) {
        bool rVal = true;
        //bool centerVisible = true;

        //int totalPointsHit = 5;
        //int pointsNeededToHit = 3;

        //// center
        //Vector3 direction = tile.Position - posA;
        //RaycastHit2D hit = (Physics2D.Raycast(posA, direction, direction.magnitude, wallTileMask));
        //if (hit.collider != null) {
        //    totalPointsHit--;
        //    centerVisible = false;
        //}

        //if (!centerVisible) {
        //    // topLeft
        //    direction = tile.TopLeft - posA; 
        //    hit = (Physics2D.Raycast(posA, direction, direction.magnitude, wallTileMask));
        //    if (hit.collider != null) {
        //        totalPointsHit--;
        //    }

        //    // topRight
        //    direction = tile.TopRight - posA;
        //    hit = (Physics2D.Raycast(posA, direction, direction.magnitude, wallTileMask));
        //    if (hit.collider != null) {
        //        totalPointsHit--;
        //    }

        //    // bottomLeft
        //    direction = tile.BottomLeft - posA;
        //    hit = (Physics2D.Raycast(posA, direction, direction.magnitude, wallTileMask));
        //    if (hit.collider != null) {
        //        totalPointsHit--;
        //    }

        //    // bottomRight
        //    direction = tile.BottomRight - posA;
        //    hit = (Physics2D.Raycast(posA, direction, direction.magnitude, wallTileMask));
        //    if (hit.collider != null) {
        //        totalPointsHit--;
        //    }
        //}
        

        //rVal = (totalPointsHit >= pointsNeededToHit);


        //// center
        Vector3 direction = tile.Position - posA;
        RaycastHit2D hit = (Physics2D.Raycast(posA, direction, direction.magnitude, wallTileMask));
        if (hit.collider != null) {
            rVal = false;
        }

        return rVal;
    }

    #region TILE_HIGHLIGHTING

    /////////////////////// TODO: THIS IS THE SAME AS GetCircleOfTiles YOU FOOL //////////////////////
    public void HighlightCircle(Vector2 centerPos, int radius, CommandType commandType) {
        Tile centerTile = GetTileFromPosition(centerPos);
        if (centerTile != null) {

            int iter = 0;
            int currentRow = centerTile.GridPos.row;
            int currentColumn = centerTile.GridPos.column;
            Tile currentTile = GetTileFromGridCoord(currentColumn, currentRow);

            // Fill the row
            iter = radius;
            for (int i = -iter; i <= iter; i++) {
                currentColumn = centerTile.GridPos.column + i;
                currentTile = GetTileFromGridCoord(currentColumn, currentRow);
                if (currentTile != null) {
                    if (HasLineOfSightToTile(centerPos, currentTile)) {
                        currentTile.SetHighlight(commandType);
                    }
                    else {
                        currentTile.SetHighlight(CommandType.ATTACK_MISS);
                    }
                    highlightedTiles.Add(currentTile);
                }
            }

            iter = 0;
            currentRow = centerTile.GridPos.row + radius;
            currentColumn = centerTile.GridPos.column;

            // Top half
            while (currentRow != centerTile.GridPos.row) {
                for (int i = -iter; i <= iter; i++) {
                    currentColumn = centerTile.GridPos.column + i;
                    currentTile = GetTileFromGridCoord(currentColumn, currentRow);
                    if (currentTile != null) {
                        if (HasLineOfSightToTile(centerPos, currentTile)) {
                            currentTile.SetHighlight(commandType);
                        }
                        else {
                            currentTile.SetHighlight(CommandType.ATTACK_MISS);
                        }
                        highlightedTiles.Add(currentTile);
                    }
                }
                iter++;
                currentRow--;
            }

            currentRow = centerTile.GridPos.row - radius;
            currentColumn = centerTile.GridPos.column;
            iter = 0;

            // Bottom half
            while (currentRow != centerTile.GridPos.row) {
                for (int i = -iter; i <= iter; i++) {
                    currentColumn = centerTile.GridPos.column + i;
                    currentTile = GetTileFromGridCoord(currentColumn, currentRow);
                    if (currentTile != null) {
                        if (HasLineOfSightToTile(centerPos, currentTile)) {
                            currentTile.SetHighlight(commandType);
                        }
                        else {
                            currentTile.SetHighlight(CommandType.ATTACK_MISS);
                        }
                        highlightedTiles.Add(currentTile);
                    }
                }
                iter++;
                currentRow++;
            }
        }
    }

    public void HighlightCross(Vector3 centerPos, int radius, CommandType commandType) {
        Tile centerTile = GetTileFromPosition(centerPos);

        if (centerTile != null) {
            int iter = 0;
            int currentRow = centerTile.GridPos.row;
            int currentColumn = centerTile.GridPos.column;
            Tile currentTile = GetTileFromGridCoord(currentColumn, currentRow);

            // Fill the row
            iter = radius;
            for (int i = -iter; i <= iter; i++) {
                currentColumn = centerTile.GridPos.column + i;
                currentTile = GetTileFromGridCoord(currentColumn, currentRow);
                if (currentTile != null) {
                    currentTile.SetHighlight(commandType);
                    highlightedTiles.Add(currentTile);
                }
            }

            // I MUST BE ABLE TO PUT THIS FOR LOOP INTO A METHOD SINCE IT'S USED LIKE 5 TIMES
            // Fill the column
            iter = radius;
            currentColumn = centerTile.GridPos.column;
            for (int i = -iter; i <= iter; i++) {
                currentRow = centerTile.GridPos.row + i;
                currentTile = GetTileFromGridCoord(currentColumn, currentRow);
                if (currentTile != null) {
                    currentTile.SetHighlight(commandType);
                    highlightedTiles.Add(currentTile);
                }
            }
        }
    }

    public void RefreshNewHighlightedTiles(ref List<Tile> newTiles, CommandType commandType) {
        ClearHighlightedTiles();
        for (int i = 0; i < newTiles.Count; i++) {
            newTiles[i].SetHighlight(commandType);
            highlightedTiles.Add(newTiles[i]);
        }
    }

    public void ClearHighlightedTiles() {
        for (int i = 0; i < highlightedTiles.Count; i++) {
            highlightedTiles[i].ClearHighlight();
        }

        highlightedTiles.Clear();
    }
    #endregion TILE_HIGHLIGHTING

    #endregion PUBLIC_METHODS

    private bool showRay = false;

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyUp(KeyCode.G)) {
            showRay = !showRay;
        }

        if (showRay) {
            Debug.DrawRay(EntityManager.Instance.ActiveEntity.Position, Utility.MouseWorldPosition());
        }

        if (Input.GetKeyUp(KeyCode.K)) {
        }
    }

}
