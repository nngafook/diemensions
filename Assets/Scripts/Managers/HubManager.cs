﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HubManager : MonoBehaviour {

    private static HubManager instance = null;
    public static HubManager Instance { get { return instance; } }

    public HubMapGenerator hubMap;

    private List<Node> allNodes = new List<Node>();
    private List<Tile> allTiles = new List<Tile>();

    public List<Node> AllNodes { get { return allNodes; } }
    public List<Tile> AllTiles { get { return allTiles; } }

    #region PRIVATE_METHODS
    void Awake() {
        instance = this;
    }

    void Start() {
        hubMap.GenerateMap();
    }
    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS
    public void ReturnToMenu() {
        SceneManager.LoadScene("MainMenu");
    }

    public List<Tile> GetRandomPath(Vector3 fromPos) {
        List<Tile> rVal = new List<Tile>();

        rVal = hubMap.Pathfinder.FindPath(hubMap.AllTiles, hubMap.GetTileFromPosition(fromPos), hubMap.GetRandomOpenTile());
        return rVal;
    }

    public void SetTileOccupied(bool value, Entity entity) {
        Tile tile = hubMap.GetTileFromPosition(entity.Position);
        tile.SetOccupied(value, entity);
    }

    #endregion PUBLIC_METHODS

    void Update() {
        if (Input.GetMouseButtonUp(1)) {
            Debug.Log(hubMap.GetTileFromPosition(Utility.MouseWorldPosition()).IsOccupied);
        }
    }
}
