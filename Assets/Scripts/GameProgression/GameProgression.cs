﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameProgression {

    private static GameProgression instance = null;
    public static GameProgression Instance { get { if (instance == null) { instance = new GameProgression(); } return instance; } }

    // Contains all of the game objectives. So most things can just use id's as references, and get the objectives from this list
    private List<GameObjective> gameObjectiveList = new List<GameObjective>();

    private GameProgressionID currentObjectiveID;

    private string assetPath = "Data/SaveData/SavedDataObject";
    private SavedDataObject savedDataObject;

    public List<GameObjective> GameObjectiveList { get { return gameObjectiveList; } }
    public GameProgressionID CurrentObjectiveID { get { return currentObjectiveID; } set { currentObjectiveID = value; } }

    public GameObjective CurrentObjective {
        get {
            GameObjective rVal = null;
            for (int i = 0; i < gameObjectiveList.Count; i++) {
                if (currentObjectiveID == gameObjectiveList[i].id) {
                    rVal = gameObjectiveList[i];
                    break;
                }
            }
            return rVal;
        }
    }

    public GameProgression() {
        savedDataObject = (SavedDataObject)Resources.Load(assetPath);
        GameObjective[] temp = Resources.LoadAll<GameObjective>("Data/GameProgression");
        gameObjectiveList = new List<GameObjective>(temp);
        LoadSavedData();
    }

    private void LoadSavedData() {
        currentObjectiveID = savedDataObject.currentObjectiveID;
    }

    public bool ObjectiveComplete(GameProgressionID id) {
        bool rVal = savedDataObject.completedObjectives.Contains(id);
        return rVal;
    }

    public void SetObjectiveComplete(GameProgressionID id) {
        if (!ObjectiveComplete(id)) {
            savedDataObject.completedObjectives.Add(id);
        }
    }

}

public class GameScene : GameProgressionObject {

    // dialog modules.
    // rewards?
    // can contain the next game objective

}
public enum GameProgressionID {
    WELCOME,
    VISIT_HUB
}