﻿using UnityEngine;
using System.Collections;

public class GameProgressionTrigger {

    // used to trigger a scene, or flip a game objective to complete
    public GameProgressionID objectiveIDToToggle;

}