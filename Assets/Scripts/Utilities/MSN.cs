﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class MSN {

	#region SINGLETON
    private static MSN instance = null;
    public static MSN Instance { get { if (instance == null) { instance = new MSN(); } return instance; } }
    #endregion SINGLETON

    private Dictionary<string, UnityEvent> eventDictionary;
	
	#region PRIVATE_METHODS
    public MSN() {
		instance = this;
        eventDictionary = new Dictionary<string, UnityEvent>();
	}
	#endregion PRIVATE_METHODS

	
	#region PUBLIC_METHODS

    public void StartListening(string eventName, UnityAction listener) {
        UnityEvent thisEvent = null;
        if (eventDictionary.TryGetValue(eventName, out thisEvent)) {
            thisEvent.AddListener(listener);
        }
        else {
            thisEvent = new UnityEvent();
            thisEvent.AddListener(listener);
            eventDictionary.Add(eventName, thisEvent);
        }
    }

    public void StopListening(string eventName, UnityAction listener) {
        UnityEvent thisEvent = null;
        if (eventDictionary.TryGetValue(eventName, out thisEvent)) {
            thisEvent.RemoveListener(listener);
        }
    }

    public void TriggerEvent(string eventName) {
        UnityEvent thisEvent = null;
        if (eventDictionary.TryGetValue(eventName, out thisEvent)) {
            thisEvent.Invoke();
        }
    }

	#endregion PUBLIC_METHODS
}

public class EventName {
    public static string ATTACK_DIALOG_COMPLETE = "ATTACK_DIALOG_COMPLETE";
    
    public static string RADIAL_DOCK_OPEN_COMPLETE = "RADIAL_DOCK_OPEN_COMPLETE";
    public static string RADIAL_DOCK_CLOSE_COMPLETE = "RADIAL_DOCK_CLOSE_COMPLETE";

    public static string ENTITY_MENU_OPEN_COMPLETE = "ENTITY_MENU_OPEN_COMPLETE";
    public static string ENTITY_MENU_CLOSE_COMPLETE = "ENTITY_MENU_CLOSE_COMPLETE";
    public static string ENTITY_MENU_MINIMIZE_COMPLETE = "ENTITY_MENU_MINIMIZE_COMPLETE";
    public static string ENTITY_MENU_ANIMATING = "ENTITY_MENU_ANIMATING";

    public static string CAMERA_PAN_TO_POSITION_COMPLETE = "CAMERA_PAN_TO_POSITION_COMPLETE";
    public static string CAMERA_SNAP_TO_PLAYER_COMPLETE = "CAMERA_SNAP_TO_PLAYER_COMPLETE";
    
    public static string ATTACK_COMPLETE = "ATTACK_COMPLETE";

    public static string FADE_TO_BLACK_COMPLETE = "FADE_TO_BLACK_COMPLETE";
    public static string FADE_FROM_BLACK_COMPLETE = "FADE_FROM_BLACK_COMPLETE";
    
    public static string START_LEVEL = "START_LEVEL";
    public static string PHASE_MESSAGE_COMPLETE = "PHASE_MESSAGE_COMPLETE";
}