﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Cheater : MonoBehaviour {

    private static Cheater instance = null;
    public static Cheater Instance { get { return instance; } }

    private CanvasGroup canvasGroup;

    private int fingerID = -1;
    private bool wtfMode = false;
    private bool fogVisible = true;
    private bool isShowing = false;

    private bool fourTouchDown = false;

    private Entity entityOne;
    private Entity entityTwo;
    private Tile tileOne;
    private Tile tileTwo;
    private List<Tile> highlightedTiles = new List<Tile>();

    public bool FlareOn { get; set; }
    public bool WTFMode { get { return wtfMode; } set { wtfMode = value; } }

    public Text flareOnText;
    public Text wtfModeOnText;
    [Space(5)]
    public Button[] buttonList;

    #region PRIVATE_METHODS
    void Awake() {
        instance = this;

        #if !UNITY_EDITOR
            fingerID = 0;
        #endif

        canvasGroup = this.GetComponent<CanvasGroup>();
    }

    void Start() {

    }

    public void ButtonClicked(CheatCommand cheat) {
        switch (cheat) {
            case CheatCommand.FLARE:
                FlareOn = !FlareOn;
                flareOnText.text = FlareOn.ToString().ToUpper().Colored(CustomColor.GetColor(ColorName.PURPLE_PLUM));
                break;
            case CheatCommand.RESET_COOLDOWNS:
                EntityManager.Instance.ResetAllCooldowns();
                break;
            case CheatCommand.TOGGLE_FOG:
                fogVisible = !fogVisible;
                //FogManager.Instance.SetToggleVisible(fogVisible);
                break;
            case CheatCommand.GIVE_COMPONENT:
                InventoryManager.Instance.AddComponentToInventory(RecipeComponentDatabase.Instance.GetRandomRecipeComponentID(), Random.Range(1, 20));
                break;
            case CheatCommand.WTF_MODE:
                Debug.Log("WTF Clicked");
                wtfMode = !wtfMode;
                wtfModeOnText.text = WTFMode.ToString().ToUpper().Colored(CustomColor.GetColor(ColorName.SONI_POOP));
                break;
            case CheatCommand.NONE:
                break;
            default:
                break;
        }
    }

    private IEnumerator GetPathBetweenTiles() {
        if ((tileOne != null) && (tileTwo != null)) {
            for (int i = 0; i < highlightedTiles.Count; i++) {
                highlightedTiles[i].ClearHighlight();
            }

            highlightedTiles = MapManager.Instance.GetPathBetweenTiles(tileOne, tileTwo);
            for (int i = 0; i < highlightedTiles.Count; i++) {
                highlightedTiles[i].SetHighlight(CommandType.ATTACK);
                yield return new WaitForSeconds(0.05f);
            }
        }
    }

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    public void Toggle() {
        flareOnText.text = FlareOn.ToString().ToUpper().Colored(CustomColor.GetColor(ColorName.PURPLE_PLUM));
        wtfModeOnText.text = WTFMode.ToString().ToUpper().Colored(CustomColor.GetColor(ColorName.SONI_POOP));
        isShowing = !isShowing;

        canvasGroup.alpha = isShowing.AsInt();
        canvasGroup.interactable = isShowing;
        canvasGroup.blocksRaycasts = isShowing;
    }

    #endregion PUBLIC_METHODS

    void Update() {

        if (fourTouchDown) {
            if (Input.touchCount < 4) {
                fourTouchDown = false;
                Toggle();
            }
        }
        else if (Input.touchCount == 4) {
            fourTouchDown = true;
        }

        if (Input.GetKeyUp(KeyCode.Escape)) {
            Toggle();
        }

        if (!EventSystem.current.IsPointerOverGameObject(fingerID)) {
            if (FlareOn) {
                if (Input.GetMouseButtonUp(0)) {
                    //FogManager.Instance.RevealCircle(Utility.MouseWorldPosition(), 5);
                    EntityManager.Instance.UpdateEntityStatuses();
                }
            }
            else {
                if (Input.GetKey(KeyCode.LeftShift)) {
                    if (Input.GetMouseButtonUp(0)) {
                        tileOne = MapManager.Instance.GetTileAtMousePosition();
                    }
                    else if (Input.GetMouseButtonUp(1)) {
                        tileTwo = MapManager.Instance.GetTileAtMousePosition();
                    }

                    if (Input.GetKeyUp(KeyCode.Space)) {
                        StartCoroutine(GetPathBetweenTiles());
                    }
                }
            }
        }
    }

}
