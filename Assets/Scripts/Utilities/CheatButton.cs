﻿using UnityEngine;
using UnityEngine.UI;

public class CheatButton : MonoBehaviour {

    public CheatCommand cheatCommand;

    private Button button;

    private void Awake() {
        button = this.GetComponent<Button>();
        button.onClick.AddListener(OnButtonClicked);
    }

    private void OnButtonClicked() {
        Cheater.Instance.ButtonClicked(cheatCommand);
    }

}
