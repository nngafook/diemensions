﻿using UnityEngine;

public class Node {

    public bool walkable;
    public Vector2 worldPosition;

    public int gCost;
    public int hCost;

    public int gridX;
    public int gridY;
    public Node parent;
    public Tile tileOwner;

    public Node(Tile tile) {
        tileOwner = tile;
        Update();
    }

    public int fCost {
        get {
            return gCost + hCost;
        }
    }

    public void ClearCosts() {
        gCost = 0;
        hCost = 0;
    }

    public void Update() {
        walkable = !tileOwner.IsOccupied;
        worldPosition = tileOwner.Position;
        gridX = tileOwner.GridPos.column;
        gridY = tileOwner.GridPos.row;
    }

}
