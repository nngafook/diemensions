﻿using System.Collections.Generic;
using UnityEngine;

public class Pathfinder : MonoBehaviour {

    private MapGenerator mapGenerator;

    #region PRIVATE_METHODS
    void Awake() {
        mapGenerator = this.GetComponent<MapGenerator>();
    }

    private bool FiftyFiftyChance() {
        return (Random.Range(0, 2) == 1);
    }

    private int GetDistance(Node nodeA, Node nodeB) {
        int distanceX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int distanceY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

        if (distanceX > distanceY) {
            return 14 * distanceY + 10 * (distanceX - distanceY);
        }
        else {
            return 14 * distanceX + 10 * (distanceY - distanceX);
        }
    }

    private void GetNeighbours(Node currentNode, ref List<Node> neighbours) {
        mapGenerator.GetDirectNeighbours(currentNode, ref neighbours);
    }

    private List<Node> RetracePath(Node startNode, Node targetNode) {
        List<Node> path = new List<Node>();
        //Node currentNode = targetNode;
        Node currentNode = (targetNode.walkable) ? targetNode : targetNode.parent;

        while (currentNode != startNode) {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }

        path.Reverse();
        return path;
    }

    private List<Tile> NodesToTiles(List<Node> nodes) {
        List<Tile> rVal = new List<Tile>();
        Tile tile = null;
        Node node = null;

        for (int i = 0; i < nodes.Count; i++) {
            node = nodes[i];
            tile = GetTileFromNode(node);
            rVal.Add(tile);
        }

        return rVal;
    }

    private List<Node> TilesToNode(List<Tile> tiles) {
        List<Node> rVal = new List<Node>();
        Tile tile = null;
        Node node = null;

        for (int i = 0; i < tiles.Count; i++) {
            tile = tiles[i];
            node = new Node(tile);
            rVal.Add(node);
        }

        return rVal;
    }

    private Tile GetTileFromNode(Node node) {
        return mapGenerator.GetTileFromPosition(node.worldPosition);
    }

    private void ClearTileCosts(ref List<Tile> tiles) {
        for (int i = 0; i < tiles.Count; i++) {
            tiles[i].ClearCosts();
        }
    }

    #region PATHFINDING_FIRST_PASS

    //private IEnumerator GetPath(List<Tile> tileRegion, Tile beginTile, Tile endTile) {
    //    if ((beginTile != null) && (endTile != null)) {
    //        if (pathTiles != null) {
    //            for (int i = 0; i < pathTiles.Count; i++) {
    //                pathTiles[i].ClearHighlight();
    //            }
    //            pathTiles.Clear();
    //        }
    //        pathTiles = new List<Tile>();
    //        locationPin.gameObject.SetActive(false);

    //        GridCoord bCoord = beginTile.GridPos;
    //        GridCoord eCoord = endTile.GridPos;
    //        Tile targetTile = MapManager.Instance.GetTileFromGridCoord(eCoord);
    //        Tile currentTile = beginTile;
    //        Tile previousTile = currentTile;

    //        int rowChange = 0;
    //        int columnChange = 0;
    //        int direction = 0;
    //        bool targetReached = false;
    //        bool forceRowChange = false;
    //        bool forceColumnChange = false;
    //        WaitForSeconds shortWait = new WaitForSeconds(0.15f);
    //        bool goThroughRows = false;

    //        // This needs to be handled with a check if both row and column failed, and THEN use the count as a check to see how many
    //        // times the while loop has iterated
    //        int count = 0;

    //        while ((!targetReached) && (count < 20)) {
    //            bCoord = currentTile.GridPos;

    //            rowChange = eCoord.row - bCoord.row;

    //            // Randomly decide if to start by going to the end row or column
    //            if (!goThroughRows) {
    //                goThroughRows = FiftyFiftyChance();
    //                Debug.Log("Going through the rows first: " + goThroughRows);
    //            }

    //            // Moving vertically
    //            if (goThroughRows) {
    //                if (currentTile.GridPos.row != eCoord.row) {
    //                    direction = rowChange > 0 ? 1 : -1;
    //                    forceColumnChange = false;
    //                    for (int i = bCoord.row; i != (bCoord.row + rowChange) + direction; i += direction) {
    //                        currentTile = MapManager.Instance.GetTileFromGridCoord(bCoord.column, i);
    //                        if ((currentTile.IsOccupied) || (!tileRegion.Contains(currentTile))) {
    //                            currentTile = previousTile;
    //                            forceColumnChange = true;
    //                            break;
    //                        }
    //                        else {
    //                            previousTile = currentTile;
    //                            currentTile.SetAttackRangeHighlight(true);
    //                            pathTiles.Add(currentTile);
    //                            yield return shortWait;
    //                        }
    //                    }
    //                }
    //                else if ((forceRowChange) && (!targetReached)) {
    //                    direction = FiftyFiftyChance().AsInt();
    //                    if (direction == 0) { // Randomly try up
    //                        currentTile = MapManager.Instance.GetTileFromGridCoord(currentTile.GridPos.column, currentTile.GridPos.row + 1);
    //                        if (currentTile.IsOccupied) {
    //                            currentTile = MapManager.Instance.GetTileFromGridCoord(currentTile.GridPos.column, currentTile.GridPos.row - 1);

    //                            //GetTileFromGridCoord(currentTile.GridPos.column, currentTile.GridPos.row + 1).ClearHighlight();
    //                            previousTile = currentTile;
    //                            currentTile.SetAttackRangeHighlight(true);
    //                            pathTiles.Add(currentTile);
    //                            yield return shortWait;
    //                        }
    //                        else {
    //                            //GetTileFromGridCoord(currentTile.GridPos.column, currentTile.GridPos.row - 1).ClearHighlight();
    //                            previousTile = currentTile;
    //                            currentTile.SetAttackRangeHighlight(true);
    //                            pathTiles.Add(currentTile);
    //                            yield return shortWait;
    //                        }
    //                    }
    //                    else { // Randomly try down
    //                        currentTile = MapManager.Instance.GetTileFromGridCoord(currentTile.GridPos.column, currentTile.GridPos.row - 1);
    //                        if (currentTile.IsOccupied) {
    //                            currentTile = MapManager.Instance.GetTileFromGridCoord(currentTile.GridPos.column, currentTile.GridPos.row + 1);

    //                            //GetTileFromGridCoord(currentTile.GridPos.column, currentTile.GridPos.row - 1).ClearHighlight();
    //                            previousTile = currentTile;
    //                            currentTile.SetAttackRangeHighlight(true);
    //                            pathTiles.Add(currentTile);
    //                            yield return shortWait;
    //                        }
    //                        else {
    //                            //GetTileFromGridCoord(currentTile.GridPos.column, currentTile.GridPos.row + 1).ClearHighlight();
    //                            previousTile = currentTile;
    //                            currentTile.SetAttackRangeHighlight(true);
    //                            pathTiles.Add(currentTile);
    //                            yield return shortWait;
    //                        }
    //                    }
    //                }
    //            }

    //            if (currentTile == targetTile) {
    //                locationPin.gameObject.SetActive(true);
    //                locationPin.position = currentTile.Position;
    //                targetReached = true;
    //            }

    //            // Moving horizontally
    //            if (currentTile.GridPos.column != eCoord.column) {
    //                columnChange = eCoord.column - currentTile.GridPos.column;
    //                direction = columnChange > 0 ? 1 : -1;
    //                forceRowChange = false;
    //                for (int i = bCoord.column; i != (bCoord.column + columnChange) + direction; i += direction) {
    //                    currentTile = MapManager.Instance.GetTileFromGridCoord(i, currentTile.GridPos.row);
    //                    if ((currentTile.IsOccupied) || (!tileRegion.Contains(currentTile))) {
    //                        // Cannot move right or left.
    //                        // Check if top or bottom is possible, if not, try opposite horizontal

    //                        // if direction > 0, moving right

    //                        // Set current tile to opposite horizontal direction
    //                        currentTile = MapManager.Instance.GetTileFromGridCoord(i + -(direction * 2), currentTile.GridPos.row);


    //                        if (FiftyFiftyChance()) {

    //                        }

    //                        currentTile = previousTile;
    //                        forceRowChange = true;
    //                        break;
    //                    }
    //                    else {
    //                        previousTile = currentTile;
    //                        currentTile.SetAttackRangeHighlight(true);
    //                        pathTiles.Add(currentTile);
    //                        yield return shortWait;
    //                    }
    //                }
    //            }
    //            else if ((forceColumnChange) && (!targetReached)) {
    //                direction = FiftyFiftyChance().AsInt();
    //                if (direction == 0) { // Randomly try right
    //                    currentTile = MapManager.Instance.GetTileFromGridCoord(currentTile.GridPos.column + 1, currentTile.GridPos.row);
    //                    if (currentTile.IsOccupied) {
    //                        currentTile = MapManager.Instance.GetTileFromGridCoord(currentTile.GridPos.column - 1, currentTile.GridPos.row);

    //                        //GetTileFromGridCoord(currentTile.GridPos.column, currentTile.GridPos.row + 1).ClearHighlight();
    //                        previousTile = currentTile;
    //                        currentTile.SetAttackRangeHighlight(true);
    //                        pathTiles.Add(currentTile);
    //                        yield return shortWait;
    //                    }
    //                    else {
    //                        //GetTileFromGridCoord(currentTile.GridPos.column, currentTile.GridPos.row - 1).ClearHighlight();
    //                        previousTile = currentTile;
    //                        currentTile.SetAttackRangeHighlight(true);
    //                        pathTiles.Add(currentTile);
    //                        yield return shortWait;
    //                    }
    //                }
    //                else { // Randomly try left
    //                    currentTile = MapManager.Instance.GetTileFromGridCoord(currentTile.GridPos.column - 1, currentTile.GridPos.row);
    //                    if (currentTile.IsOccupied) {
    //                        currentTile = MapManager.Instance.GetTileFromGridCoord(currentTile.GridPos.column + 1, currentTile.GridPos.row);

    //                        //GetTileFromGridCoord(currentTile.GridPos.column, currentTile.GridPos.row - 1).ClearHighlight();
    //                        previousTile = currentTile;
    //                        currentTile.SetAttackRangeHighlight(true);
    //                        pathTiles.Add(currentTile);
    //                        yield return shortWait;
    //                    }
    //                    else {
    //                        //GetTileFromGridCoord(currentTile.GridPos.column, currentTile.GridPos.row + 1).ClearHighlight();
    //                        previousTile = currentTile;
    //                        currentTile.SetAttackRangeHighlight(true);
    //                        pathTiles.Add(currentTile);
    //                        yield return shortWait;
    //                    }
    //                }
    //            }


    //            if (currentTile == targetTile) {
    //                locationPin.gameObject.SetActive(true);
    //                locationPin.position = currentTile.Position;
    //                targetReached = true;
    //            }
    //            else if (!goThroughRows) {
    //                // Set this to true so that every time after the first time through the while, it'll go through the rows
    //                goThroughRows = true;
    //            }

    //            count++;
    //            Debug.Log("While Loop Complete".Colored("#006699FF").Bold());
    //        }
    //        Debug.Log("Count: " + count);
    //    }
    //}

    #endregion PATHFINDING_FIRST_PASS

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    public List<Tile> GetPathBetweenTiles(List<Tile> tileRegion, Tile beginTile, Tile endTile) {
        List<Tile> rVal = new List<Tile>();

        rVal = FindPath(tileRegion, beginTile, endTile);
        if (rVal.Count == 0) {
            //Debug.Log("Path Not Found".Bold().Colored(Color.red));
            //Instantiate(Resources.Load("Prefabs/TileUnreachable"), endTile.Position, Quaternion.identity);
        }

        return rVal;
    }

    public List<Tile> GetPathInRegion(List<Tile> tileRegion, Tile beginTile, Tile endTile) {
        List<Tile> rVal = new List<Tile>();

        if (endTile.Node.walkable) {
            rVal = FindPath(tileRegion, beginTile, endTile);
            if (rVal.Count == 0) {
                //Debug.Log("Path Not Found".Bold().Colored(Color.red));
                //Instantiate(Resources.Load("Prefabs/TileUnreachable"), endTile.Position, Quaternion.identity);
            }
            //else {
            //    for (int i = 0; i < rVal.Count; i++) {
            //        rVal[i].SetAttackRangeHighlight(true);
            //    }
            //}
        }

        return rVal;
    }
    public List<Tile> FindPath(List<Tile> tileRegion, Tile startTile, Tile targetTile) {
        List<Tile> rVal = new List<Tile>();
        Node currentNode = null;
        Node startNode = startTile.Node;
        Node targetNode = targetTile.Node;

        List<Node> neighbours = new List<Node>();
        List<Node> openSet = new List<Node>();
        HashSet<Node> closedSet = new HashSet<Node>();

        ClearTileCosts(ref tileRegion);

        openSet.Add(startNode);

        while (openSet.Count > 0) {
            currentNode = openSet[0];
            for (int i = 0; i < openSet.Count; i++) {
                if ((openSet[i].fCost < currentNode.fCost) || ((openSet[i].fCost == currentNode.fCost) && (openSet[i].hCost < currentNode.hCost))) {
                    currentNode = openSet[i];
                }
            }
            openSet.Remove(currentNode);
            closedSet.Add(currentNode);

            if (currentNode == targetNode) {
                rVal = NodesToTiles(RetracePath(startNode, targetNode));
                //Debug.Log("Inside Return".Bold().Colored(Color.cyan));
                return rVal;
            }

            neighbours.Clear();
            GetNeighbours(currentNode, ref neighbours);

            for (int i = 0; i < neighbours.Count; i++) {
                if ((!tileRegion.Contains(neighbours[i].tileOwner)) || ((!neighbours[i].walkable ) && (neighbours[i] != targetNode) || closedSet.Contains(neighbours[i]))) {
                //if ((!tileRegion.Contains(neighbours[i].tileOwner)) || (closedSet.Contains(neighbours[i]))) {
                    continue;
                }

                int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbours[i]);
                if ((newMovementCostToNeighbour < neighbours[i].gCost) || (!openSet.Contains(neighbours[i]))) {
                    neighbours[i].gCost = newMovementCostToNeighbour;
                    neighbours[i].hCost = GetDistance(neighbours[i], targetNode);
                    neighbours[i].parent = currentNode;

                    if (!openSet.Contains(neighbours[i])) {
                        openSet.Add(neighbours[i]);
                    }
                    else {
                        openSet.Remove(neighbours[i]);
                        openSet.Add(neighbours[i]);
                    }
                }
            }
        }
        //Debug.Log("Outside Return".Bold().Colored(Color.green));
        return rVal;
    }

    #endregion PUBLIC_METHODS

    void Update() {
        //if (Input.GetKeyUp(KeyCode.Alpha1)) {
        //    bTile = MapManager.Instance.GetTileAtMousePosition();
        //    Debug.Log(("Begin Tile: " + bTile.GridPos.column + ", " + bTile.GridPos.row).Colored(Color.blue));
        //}
        //if (Input.GetKeyUp(KeyCode.Alpha2)) {
        //    eTile = MapManager.Instance.GetTileAtMousePosition();
        //    Debug.Log(("End Tile: " + eTile.GridPos.column + ", " + eTile.GridPos.row).Colored(Color.red));
        //}

        //if (Input.GetKeyUp(KeyCode.Space)) {
        //    //StartCoroutine(GetPath());
        //    List<Tile> region = MapManager.Instance.GetMoveableTiles(entityOwner);
        //    GetPathInRegion(region, bTile, eTile);
        //}
    }

}
