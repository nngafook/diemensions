﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

#pragma warning disable 0660
#pragma warning disable 0661
public static class Utility {

    public static T[] ShuffleArray<T>(T[] array, int seed) {
        System.Random prng = new System.Random(seed);

        for (int i = 0; i < array.Length - 1; i++) {
            int randomIndex = prng.Next(i, array.Length);
            T tempItem = array[randomIndex];
            array[randomIndex] = array[i];
            array[i] = tempItem;
        }

        return array;
    }

    public static bool IsWithinGrid(int coordColumn, int coordRow, int gridWidth, int gridHeight) {
        return (((coordColumn >= 0) && (coordColumn < gridWidth)) && ((coordRow >= 0) && (coordRow < gridHeight)));
    }

    public static Vector3 MouseWorldPosition() {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    public static void WriteData(string data, string folderPath) {
        if (!string.IsNullOrEmpty(data)) {
            string path = Application.dataPath + folderPath;
            if (!string.IsNullOrEmpty(path)) {
                using (FileStream fs = new FileStream(path, FileMode.Create)) {
                    using (StreamWriter writer = new StreamWriter(fs)) {
                        writer.Write(data);
                    }
                }
                Debug.Log("Export Complete".Bold().Colored(Color.yellow));
            }
        }
    }

    public static string IDToName(string id, string suffixIDTag = "_id") {
        char c;
        List<char> chars = new List<char>();
        chars.Add(char.ToUpper(id[0]));
        for (int i = 1; i < id.Length - suffixIDTag.Length; i++) {
            c = id[i];
            if (char.IsUpper(c)) {
                chars.Add(' ');
                chars.Add(char.ToUpper(c));
            }
            else
                chars.Add(c);
        }

        return new string(chars.ToArray());
    }

}

#region CLASSES_AND_STRUCTS
//[Serializable]
public class UID : IComparable<UID> {
    private static int factoryValue = 1;
    //[SerializeField]
    private int value = 0;

    public int Value { get { return this.value; } }

    public UID() {
        value = factoryValue;
        factoryValue++;
    }

    public int CompareTo(UID other) {
        return other.value.CompareTo(this.value);
    }
}

[Serializable]
public class EntityDatabase {
    public List<Entity> AllEntities { get; set; }
    public List<Entity> PlayerEntities { get; set; }
    public List<Entity> EnemyEntities { get; set; }
    public List<Entity> BossEntities { get; set; }
    public List<Entity> PropEntities { get; set; }
    public List<Entity> InteractiveEntities { get; set; }

    public EntityDatabase() {
        AllEntities = new List<Entity>();
        PlayerEntities = new List<Entity>();
        EnemyEntities = new List<Entity>();
        BossEntities = new List<Entity>();
        PropEntities = new List<Entity>();
        InteractiveEntities = new List<Entity>();
    }

    public void RegisterEntity(Entity entity) {
        EntityType type = entity.entityPropertiesData.entityType;
        switch (type) {
            case EntityType.PLAYER:
                PlayerEntities.Add(entity);
                break;
            case EntityType.ENEMY:
                EnemyEntities.Add(entity);
                break;
            case EntityType.BOSS:
                BossEntities.Add(entity);
                break;
            case EntityType.PROP:
                PropEntities.Add(entity);
                break;
            case EntityType.INTERACTIVE:
                InteractiveEntities.Add(entity);
                break;
            case EntityType.NEUTRAL:

                break;
            default:

                break;
        }
        AllEntities.Add(entity);
    }

    public void UnregisterEntity(Entity entity) {
        EntityType type = entity.entityPropertiesData.entityType;
        switch (type) {
            case EntityType.PLAYER:
                PlayerEntities.Remove(entity);
                break;
            case EntityType.ENEMY:
                Debug.Log("Entity removed: " + entity.name);
                EnemyEntities.Remove(entity);
                break;
            case EntityType.BOSS:
                BossEntities.Remove(entity);
                break;
            case EntityType.PROP:
                PropEntities.Remove(entity);
                break;
            case EntityType.INTERACTIVE:
                InteractiveEntities.Remove(entity);
                break;
            case EntityType.NONE:
                break;
            default:
                break;
        }
        AllEntities.Remove(entity);
    }

    public void UpdateStatuses() {
        Entity entity = null;
        for (int i = 0; i < AllEntities.Count; i++) {
            entity = AllEntities[i];
            entity.UpdateStatus();
        }
    }

    public bool ValidateEntity(Entity entity) {
        return ((entity != null) && (AllEntities.Contains(entity)));
    }

}

[Serializable]
public class Map {
    public GridCoord mapSize;
    [Range(0, 1)]
    public float obstaclePercent;
    [Range(0, 1)]
    public float enemyPercent;
    [Range(0, 1)]
    public float propPercent;
    [Range(0, 1)]
    public float treasurePercent;
    public int seed;
    public float minObstacleHeight;
    public float maxObstacleHeight;
    //public Color foregroundColor;
    //public Color backgroundColor;

    public Map() {
        if (mapSize.row % 2 != 0) {
            Debug.Log("Map Size Row needs to be even");
        }
        if (mapSize.column % 2 != 0) {
            Debug.Log("Map Size Column needs to be even");
        }
    }

    // 1 taken from column and row for 0 index
    public GridCoord mapCenter {
        get {
            return new GridCoord((mapSize.column / 2) - 1, (mapSize.row / 2) - 1);
        }
    }
}

[Serializable]
public struct GridCoord {
    public int column;
    public int row;

    public GridCoord(int _x, int _y) {
        column = _x;
        row = _y;
    }

    public static bool operator ==(GridCoord c1, GridCoord c2) {
        return ((c1.column == c2.column) && (c1.row == c2.row));
    }

    public static bool operator !=(GridCoord c1, GridCoord c2) {
        return !(c1 == c2);
    }
}

[Serializable]
public class RangedFloat {
    public float min = 0;
    public float max = 1;

    public RangedFloat() {
        min = 0;
        max = 1;
    }

    public RangedFloat(float minValue, float maxValue) {
        min = minValue;
        max = maxValue;
    }

    public float Random {
        get {
            return UnityEngine.Random.Range(min, max);
        }
    }

    public int RandomInt {
        get {
            return Mathf.RoundToInt(UnityEngine.Random.Range(min, max));
        }
    }
}

/// <summary>
/// A float used in percentage calculations.
/// </summary>
[Serializable]
public class PercentageFloat {
    [Range(0, 100)]
    public float baseValue;

    public PercentageFloat() {
        baseValue = 50;
    }

    public PercentageFloat(float v) {
        baseValue = v;
    }

    public float BaseValue {
        get {
            return baseValue;
        }
    }

    public bool Success {
        get {
            float randomValue = UnityEngine.Random.Range(0, 100);
            return (randomValue <= baseValue);
        }
    }
}

[Serializable]
public class RecipeInput {
    public string recipeID = "";
    public int amount = 0;

    public RecipeInput() {
    }

    public RecipeInput(int amt, string id) {
        amount = amt;
        recipeID = id;
    }
}

[Serializable]
public struct EntityProperties {
    [Tooltip("Type of entity")]
    public EntityType entityType;

    [Tooltip("Name of Entity")]
    public string name;

    [Tooltip("Class of Entity")]
    public CharacterClass classType;

    [Tooltip("If this entity needs a turn in the game")]
    public bool needsTurn;
    [Tooltip("If this entity can move on it's own")]
    public bool hasMobility;
    [Tooltip("If this entity can attack")]
    public bool hasAttack;
    [Tooltip("If this entity has a lifetime duration")]
    public bool hasLifetimeDuration;
    [Tooltip("If this entity's move distance is a 'circle' or a cross. If this becomes more than two options, enum this bitch")]
    public bool hasCircularMoveRange;
    [Tooltip("Movement Speed in seconds between nodes")]
    public float movementSpeed;

    [Tooltip("Max Health")]
    public int maxHealth;
    [Tooltip("Max Resource")]
    public int maxResource;
    [Tooltip("How far this entity can move")]
    public int moveDistance;
    [Tooltip("How much fog does this entity clear in a circle around it")]
    public int visionRadius;
    [Tooltip("How far the entity can 'see' around it. Used to detect if it can see a player entity or not. This times moveDistance is used to determine validity of movement")]
    public int visionMoveFactor;

    [Header("Offensive")]
    [Tooltip("Physical damage multiplier. Added to any physical attacks")]
    public int physicalAttack;
    [Tooltip("Magical damage multiplier. Added to any magical attacks")]
    public int magicAttack;
    [Tooltip("BaseDamage multiplied by this is used in attacks, if a crit is successful")]
    public float critDamage;
    [Tooltip("Critical attack chance on any kind of attack. Percentage chance 0-100")]
    public PercentageFloat focus;

    [Header("Defensive")]
    [Tooltip("Physical defense. Negates this amount of physical damage from incoming attacks")]
    public RangedFloat physicalDefense;
    [Tooltip("Magic defense. Negates this amount of magical damage from incoming attacks")]
    public RangedFloat magicDefense;

    [Header("Attacks")]
    // Attacks
    public Attack[] attacks;
}

[Serializable]
public struct EntityStatus {
    [Tooltip("Active buffs on the entity")]
    public Buff[] buffs;
    [Tooltip("Active debuffs on the entity")]
    public Debuff[] debuffs;
    [Tooltip("Equipped Armor Set")]
    public CraftableDataObject equippedArmorSet;

    [Tooltip("Entity's current health. (max is set in the entity properties)")]
    public float currentHealth;
    [Tooltip("Entity's current resource. (max is set in the entity properties)")]
    public float currentResource;
    [Tooltip("If the entity has moved this turn")]
    public bool hasMoved;
    [Tooltip("If the entity has attacked this turn")]
    public bool hasAttacked;
    [Tooltip("If the entity is an enemy, and is in range of a player to be able to attack")]
    public bool isInRangeOfPlayerEntity;
    //[Tooltip("If the entity is visible to the player, the entity may have a command that could happen even if not in range of a player")]
    //public bool isVisibleToThePlayer;
    //[Tooltip("If the entity has seen the player, it should always try to chase the player, or move. No longer hidden in the shadows not acting")]
    //public bool hasBeenRevealedToPlayer;
}

public class CustomColor {

    private static Color soniPoop = new Color(0.6f, 0.5f, 0.1f, 1f);
    private static Color errorRed = new Color(1f, 0.1803922f, 0.1803922f, 1f);
    private static Color submitGreen = new Color(0.3f, 0.9f, 0.4f, 1f);
    private static Color robBlue = new Color(0.3f, 0.6f, 0.9f, 1f);
    private static Color purplePlum = new Color(0.5921569f, 0.1568628f, 0.4313726f, 1f);
    private static Color buttonDisabledGray = new Color ( 0.2313726f, 0.2313726f, 0.2313726f, 1f );

    public static Color GetColor(ColorName color) {
        Color rVal = new Color();

        switch (color) {
            case ColorName.SONI_POOP:
                rVal = soniPoop;
                break;
            case ColorName.ERROR_RED:
                rVal = errorRed;
                break;
            case ColorName.SUBMIT_GREEN:
                rVal = submitGreen;
                break;
            case ColorName.ROB_BLUE:
                rVal = robBlue;
                break;
            case ColorName.PURPLE_PLUM:
                rVal = purplePlum;
                break;
            case ColorName.BUTTON_DISABLED_GRAY:
                rVal = buttonDisabledGray;
                break;
        }

        return rVal;
    }
}
#endregion CLASSES_AND_STRUCTS

[Serializable]
public enum SFXName {
    ENTITY_BUTTON_CLICK
}

public enum ColorName {
    SONI_POOP,
    ERROR_RED,
    SUBMIT_GREEN,
    ROB_BLUE,
    PURPLE_PLUM,
    BUTTON_DISABLED_GRAY
}

public enum CheatCommand {
    FLARE,
    RESET_COOLDOWNS,
    TOGGLE_FOG,
    GIVE_COMPONENT,
    WTF_MODE,

    NONE = 99
}

public enum CommandType {
    MOVE,
    ATTACK,
    SKILL,

    ATTACK_MISS = 96,
    BACK = 97,
    END = 98,
    NONE = 99
}

public enum EntityType {
    PLAYER,
    ENEMY,
    BOSS,
    NEUTRAL,
    PROP,
    INTERACTIVE,

    NONE = 99
}

public enum EntityState {
    IDLE,
    MOVING,
    ATTACKING,

    CLOSE = 90,
    END = 99
}

public enum GamePhase {
    PLAYER,
    ENEMY,
    BOSS,
    NEUTRAL,

    NONE = 99
}

public enum GameState {
    FREE_ROAM,
    ENTITY_MENU,
    ENTITY_MOVE,
    ENTITY_ATTACK,

    NONE = 99
}

public enum AttackVFXType {
    FIREBALL = 1,
    EXPLOSION = 2,
    MELEE = 3,

    NONE
}

public enum AttackType {
    RANGED,
    MELEE,
    TARGETED,

    NONE
}

public enum DamageType {
    PHYSICAL,
    MAGICAL,

    NONE
}

// Will probably end up becoming a class
public enum Buff {
    REGEN
}

// Will probably end up becoming a class
public enum Debuff {
    BLEED
}

public enum ItemType {
    ACCESSORY,
    ARMOR_SET,
    CONSUMABLE,
    RECIPE,
    RECIPE_COMPONENT,
    WEAPON
}

public enum EquippableItemType {
    HELM,
    CHESTPIECE,
    LEG,
    MAIN_HAND_WEAPON,
    OFF_HAND_WEAPON,
    TWO_HAND_WEAPON,


    NONE = 99
}

public enum CharacterClass {
    MAGE,
    SOLDIER
}