﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class DebugWindow : MonoBehaviour {

    public Text gameStateText;
    public Text gamePhaseText;
    public Text activeEntityText;
    public Text activeEntityNearbyPlayerText;
    public Text selectedEntityText;
    public Text selectedEntityNearbyPlayerText;
    public Text selectedEntityTargetEntityText;
    public Text selectedTileText;
    public Text tileOccupiedText;
    public Text tileRevealedText;

    [Space(5)]
    public Text tempText;

    private Vector2 shownPosition = new Vector2(-5, -5);
    private Vector2 hiddenPosition = new Vector2(350, -5);
    private RectTransform rectTransform;

    private bool isVisible = false;
    private Tile selectedTile;

    #region PRIVATE_METHODS
    void Awake() {
        rectTransform = this.GetComponent<RectTransform>();
    }

    void Start() {
        UpdateTileData();
    }

    private string GetPrefix(string s) {
        string rVal = "";
        int index = s.IndexOf(":");

        if (index > 0) {
            rVal = s.Substring(0, index + 1);
        }
        return (rVal + " ");
    }

    private void ToggleVisible() {
        isVisible = !isVisible;

        if (isVisible) {
            UpdateGameData();
            rectTransform.DOAnchorPos(shownPosition, 0.5f);
        }
        else {
            rectTransform.DOAnchorPos(hiddenPosition, 0.5f);
        }
    }

    private void ProcessInput() {
        bool ctrlHeld = Input.GetKey(KeyCode.LeftControl);

        if ((ctrlHeld) && (Input.GetKeyUp(KeyCode.Space))) {
            ToggleVisible();
        }

        if ((ctrlHeld) && (Input.GetMouseButtonUp(1))) {
            selectedTile = MapManager.Instance.GetTileAtMousePosition();
            UpdateTileData();
        }

        if (isVisible) {
            if ((ctrlHeld) && (Input.GetKeyUp(KeyCode.R))) {
                UpdateGameData();
            }
        }
    }

    private void UpdateGameData() {
        gameStateText.text = (GetPrefix(gameStateText.text)) + GameManager.Instance.CurrentGameState;
        gamePhaseText.text = (GetPrefix(gamePhaseText.text)) + GameManager.Instance.CurrentGamePhase;
        activeEntityText.text = (GetPrefix(activeEntityText.text)) + ((ActiveEntity() != null) ? ActiveEntity().name : "null");
        SetActiveNearby();
        selectedEntityText.text = (GetPrefix(selectedEntityText.text)) + ((SelectedEntity() != null) ? SelectedEntity().name : "null");
        SetSelectedNearby();

        tempText.text = (GetPrefix(tempText.text)) + ((ActiveEntity() != null && SelectedEntity() != null) ? (Mathf.Abs(Vector3.Distance(SelectedEntity().Position, ActiveEntity().Position))).ToString() : "null");
    }

    private void SetActiveNearby() {
        Entity activeEntity = ActiveEntity();
        string nearbyName = "";
        if (activeEntity == null) {
            nearbyName = "null";
        }
        else {
            if (EntityManager.Instance.GetNearestPlayerEntity(activeEntity) == null) {
                nearbyName = "null";
            }
            else {
                nearbyName = EntityManager.Instance.GetNearestPlayerEntity(activeEntity).entityPropertiesData.name;
            }
        }
        activeEntityNearbyPlayerText.text = (GetPrefix(activeEntityNearbyPlayerText.text)) + nearbyName;
    }

    private void SetSelectedNearby() {
        Entity selectedEntity = SelectedEntity();
        string nearbyName = "";
        string targetName = "";
        if (selectedEntity == null) {
            nearbyName = "null";
            targetName = "null";
        }
        else {
            if (EntityManager.Instance.GetNearestPlayerEntity(selectedEntity) == null) {
                nearbyName = "null";
            }
            else {
                nearbyName = EntityManager.Instance.GetNearestPlayerEntity(selectedEntity).entityPropertiesData.name;
            }
            if (selectedEntity.TargetEntity == null) {
                targetName = "null";
            }
            else {
                targetName = selectedEntity.TargetEntity.Properties.name;
            }
        }
        selectedEntityNearbyPlayerText.text = (GetPrefix(selectedEntityNearbyPlayerText.text)) + nearbyName;
        selectedEntityTargetEntityText.text = (GetPrefix(selectedEntityTargetEntityText.text)) + targetName;
    }

    private void UpdateTileData() {
        if (selectedTile == null) {
            selectedTileText.text = (GetPrefix(selectedTileText.text)) + "null";
            tileOccupiedText.text = (GetPrefix(tileOccupiedText.text)) + "null";
            tileRevealedText.text = (GetPrefix(tileRevealedText.text)) + "null";
        }
        else {
            //FogTile fogTile = FogManager.Instance.GetTileAtMousePosition();
            selectedTileText.text = (GetPrefix(selectedTileText.text)) + selectedTile.name;
            tileOccupiedText.text = (GetPrefix(tileOccupiedText.text)) + selectedTile.IsOccupied;
            //tileRevealedText.text = (GetPrefix(tileRevealedText.text)) + fogTile.IsFullyRevealed;
        }
    }

    private Entity ActiveEntity() {
        return EntityManager.Instance.ActiveEntity;
    }

    private Entity SelectedEntity() {
        return EntityManager.Instance.SelectedEntity;
    }

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    #endregion PUBLIC_METHODS

    void Update() {
        ProcessInput();
        
    }
}
