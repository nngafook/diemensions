﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class SFXScriptable : ScriptableObject {

    public SFXName sfxName;
    public AudioClip clip;

}
