﻿using System;
using UnityEngine;

[Serializable]
public class Command {

    public string commandName;
    public string ID;
    public CommandType type;

    public int resourceCost;

    // NEEDS A WEIGHTED CHANCE PROPERTY

    [HideInInspector]
    public bool onCooldown = false;

}
