﻿using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class AttackCompleteEvent : UnityEvent<Attack> { };

[Serializable]
public class Attack : Command {

    public AttackType attackType;
    public AttackVFXType vfxType;
    public DamageType damageType;

    public int range;
    public int aoeRadius;
    public RangedFloat baseDamage;

    private Vector3 startPosition;
    private Vector3 targetPosition;

    private Entity entityOwner;

    #region PRIVATE_METHODS
    private void InstantiateEffect() {
        switch (vfxType) {
            case AttackVFXType.FIREBALL:
                Fireball();
                break;
            case AttackVFXType.EXPLOSION:
                break;
            case AttackVFXType.MELEE:
                Melee();
                break;
            default:
                break;
        }
    }

    private void Fireball() {
        FastPool fastPool = FastPoolManager.GetPool((int)(vfxType), null, false);
        Projectile projectile = fastPool.FastInstantiate(startPosition, Quaternion.identity).GetComponent<Projectile>();
        projectile.SetTargetPosition(targetPosition);
        projectile.OnAttackComplete += ProjectileReachedTarget;
    }

    private void Melee() {
        FastPool fastPool = FastPoolManager.GetPool((int)(vfxType), null, false);
        Hit hit = fastPool.FastInstantiate(targetPosition, Quaternion.identity).GetComponent<Hit>();
        hit.Invoke(targetPosition);
        ApplyDamage();

        InvokeAttackCompleteEvent();
    }

    private void ProjectileReachedTarget(AttackView projectile) {
        projectile.OnAttackComplete -= ProjectileReachedTarget;
        ApplyDamage();

        InvokeAttackCompleteEvent();
    }

    private void ApplyDamage() {
        Entity entityHit = MapManager.Instance.EntityAtPosition(targetPosition);
        // Check for crit chance. If success, crit damage * max damage * attackType
        bool isCrit = (entityOwner.Properties.focus.Success);
        int damage = (isCrit) ? Mathf.CeilToInt(baseDamage.max * entityOwner.Properties.critDamage) : baseDamage.RandomInt;
        if (entityHit != null) {
            switch (damageType) {
                case DamageType.PHYSICAL:
                    entityHit.TakeDamage(damage * entityOwner.Properties.physicalAttack, damageType, isCrit);
                    break;
                case DamageType.MAGICAL:
                    entityHit.TakeDamage(damage * entityOwner.Properties.magicAttack, damageType, isCrit);
                    break;
                case DamageType.NONE:
                    break;
                default:
                    break;
            }

        }
    }

    private void InvokeAttackCompleteEvent() {
        MSN.Instance.TriggerEvent(EventName.ATTACK_COMPLETE);
    }

    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS
    public void CopyFromDataObject(AttackDataObject dataObject) {
        this.commandName = dataObject.commandName;
        this.ID = dataObject.ID;
        this.type = dataObject.type;
        this.resourceCost = dataObject.resourceCost;
        this.attackType = dataObject.attackType;
        this.vfxType = dataObject.vfxType;
        this.damageType = dataObject.damageType;
        this.range = dataObject.range;
        this.aoeRadius = dataObject.aoeRadius;
        this.baseDamage = dataObject.baseDamage;
    }

    public void Invoke(Entity targetEntity) {
        entityOwner = EntityManager.Instance.ActiveEntity;
        if ((entityOwner.Status.currentResource - resourceCost) >= 0) {
            onCooldown = true;
            entityOwner.entityStatus.hasAttacked = true;

            startPosition = entityOwner.Position;
            targetPosition = targetEntity.Position;

            entityOwner.SpendResource(resourceCost);
            InstantiateEffect();
        }
    }

    public void Invoke(Vector3 startPos, Vector3 endPos) {
        entityOwner = EntityManager.Instance.ActiveEntity;
        if ((entityOwner.Status.currentResource - resourceCost) >= 0) {
            onCooldown = true;
            entityOwner.entityStatus.hasAttacked = true;

            endPos.z = 0;
            startPosition = startPos;
            targetPosition = endPos;

            entityOwner.SpendResource(resourceCost);
            InstantiateEffect();
        }
    }
    #endregion PUBLIC_METHODS

    void Update() {

    }
}
