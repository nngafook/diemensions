﻿using System.Collections.Generic;
using UnityEngine;

public class LevelMapGenerator : MapGenerator {

    [Header("Level Map Generator Variables")]
    public Transform[] enemyEntityPrefabs;
    public Transform[] wallPrefabs;
    public Transform[] propEntityPrefabs;
    public Transform[] treasureChestPrefabs;

    #region PRIVATE_METHODS
    protected override void Awake() {
        base.Awake();
    }

    void Start() {

    }
    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS

    public override void GenerateMap() {
        base.GenerateMap();
        tileMap = new Transform[currentMap.mapSize.column, currentMap.mapSize.row];
        allTiles = new List<Tile>();
        //Random prng = new Random(currentMap.seed);

        GenerateGridCoords();

        GenerateTiles();

        GenerateWalls();

        GenerateEnemies();

        GenerateProps();

        GenerateTreasureChests();

        //shuffledOpenTileCoords = new Queue<GridCoord>(Utilitiy.ShuffleArray(allOpenCoords.ToArray(), currentMap.seed));
    }

    private void GenerateGridCoords() {
        // Generating coords
        allTileCoords = new System.Collections.Generic.List<GridCoord>();
        for (int x = 0; x < currentMap.mapSize.column; x++) {
            for (int y = 0; y < currentMap.mapSize.row; y++) {
                allTileCoords.Add(new GridCoord(x, y));
            }
        }
        shuffledTileCoords = new Queue<GridCoord>(Utility.ShuffleArray(allTileCoords.ToArray(), currentMap.seed));
    }

    private void GenerateTiles() {
        // Create map holder object
        string holderName = "Generated Map";
        if (transform.FindChild(holderName)) {
            DestroyImmediate(transform.FindChild(holderName).gameObject);
        }
        Transform mapHolder = new GameObject(holderName).transform;
        mapHolder.SetParent(transform, false);
        // Spawning tiles
        for (int x = 0; x < currentMap.mapSize.column; x++) {
            for (int y = 0; y < currentMap.mapSize.row; y++) {
                Vector3 tilePosition = CoordToPosition(x, y);
                Transform newTile = Instantiate(tilePrefabs.RandomElement(), tilePosition, Quaternion.identity) as Transform;
                newTile.GetComponent<Tile>().SetGridPos(x, y);
                newTile.localScale = Vector3.one * (1 - outlinePercent);// *tileSize;
                newTile.SetParent(mapHolder);
                tileMap[x, y] = newTile;
                allTiles.Add(newTile.GetComponent<Tile>());
            }
        }
    }

    private void GenerateWalls() {
        string holderName = "Walls";
        if (transform.FindChild(holderName)) {
            DestroyImmediate(transform.FindChild(holderName).gameObject);
        }
        Transform wallHolder = new GameObject(holderName).transform;
        wallHolder.SetParent(transform, false);

        // Spawning enemies
        bool[,] obstacleMap = new bool[(int)currentMap.mapSize.column, (int)currentMap.mapSize.row];

        int wallCount = (int)(currentMap.mapSize.column * currentMap.mapSize.row * currentMap.obstaclePercent);
        int currentSpawnCount = 0;
        List<GridCoord> allOpenCoords = new List<GridCoord>(allTileCoords);
        GridCoord randomCoord;
        Vector3 wallPosition;
        Transform newWall;
        for (int i = 0; i < wallCount; i++) {
            randomCoord = GetRandomCoord();
            obstacleMap[randomCoord.column, randomCoord.row] = true;
            currentSpawnCount++;
            if ((randomCoord != currentMap.mapCenter) && (MapIsFullyAccessible(obstacleMap, currentSpawnCount))) {
            //if (randomCoord != currentMap.mapCenter) {
                //float obstacleHeight = Mathf.Lerp(currentMap.minObstacleHeight, currentMap.maxObstacleHeight, (float)prng.NextDouble());
                wallPosition = CoordToPosition(randomCoord.column, randomCoord.row);

                newWall = Instantiate(wallPrefabs.RandomElement(), wallPosition, Quaternion.identity) as Transform;

                newWall.SetParent(wallHolder);
                GetTileFromGridCoord(randomCoord.column, randomCoord.row).SetAsWall();

                //newObstacle.localScale = new Vector3((1 - outlinePercent) * tileSize, (1 - outlinePercent) * tileSize, 1);
                //newEnemy.localScale = Vector3.one * (1 - outlinePercent) * tileSize;

                allOpenCoords.Remove(randomCoord);
            }
            else {
                obstacleMap[randomCoord.column, randomCoord.row] = false;
                currentSpawnCount--;
            }
        }
    }

    private void GenerateEnemies() {
        string holderName = "Enemy Entities";
        if (transform.FindChild(holderName)) {
            DestroyImmediate(transform.FindChild(holderName).gameObject);
        }
        Transform enemyHolder = new GameObject(holderName).transform;
        enemyHolder.SetParent(transform, false);

        // Spawning enemies
        //bool[,] obstacleMap = new bool[(int)currentMap.mapSize.column, (int)currentMap.mapSize.row];

        int enemyCount = (int)(currentMap.mapSize.column * currentMap.mapSize.row * currentMap.enemyPercent);
        int currentSpawnCount = 0;
        //List<GridCoord> allOpenCoords = new List<GridCoord>(allTileCoords);

        for (int i = 0; i < enemyCount; i++) {
            GridCoord randomCoord = GetRandomCoord();
            //obstacleMap[randomCoord.column, randomCoord.row] = true;
            currentSpawnCount++;
            //if ((randomCoord != currentMap.mapCenter) && (MapIsFullyAccessible(obstacleMap, currentObstacleCount))) {
            if (randomCoord != currentMap.mapCenter) {
                //float obstacleHeight = Mathf.Lerp(currentMap.minObstacleHeight, currentMap.maxObstacleHeight, (float)prng.NextDouble());
                Vector3 enemyPosition = CoordToPosition(randomCoord.column, randomCoord.row);

                Transform newEnemy = Instantiate(enemyEntityPrefabs.RandomElement(), enemyPosition, Quaternion.identity) as Transform;

                newEnemy.SetParent(enemyHolder);

                //newObstacle.localScale = new Vector3((1 - outlinePercent) * tileSize, (1 - outlinePercent) * tileSize, 1);
                //newEnemy.localScale = Vector3.one * (1 - outlinePercent) * tileSize;

                //allOpenCoords.Remove(randomCoord);
            }
            else {
                //obstacleMap[randomCoord.column, randomCoord.row] = false;
                currentSpawnCount--;
            }
        }
        totalEnemiesSpawned = currentSpawnCount;
    }

    private void GenerateProps() {
        string holderName = "Prop Entities";
        if (transform.FindChild(holderName)) {
            DestroyImmediate(transform.FindChild(holderName).gameObject);
        }
        Transform propHolder = new GameObject(holderName).transform;
        propHolder.SetParent(transform, false);

        // Spawning props
        int propCount = (int)(currentMap.mapSize.column * currentMap.mapSize.row * currentMap.propPercent);
        int currentSpawnCount = 0;
        for (int i = 0; i < propCount; i++) {
            GridCoord randomCoord = GetRandomCoord();
            currentSpawnCount++;
            if (randomCoord != currentMap.mapCenter) {
                Vector3 propPosition = CoordToPosition(randomCoord.column, randomCoord.row);
                Transform newProp = Instantiate(propEntityPrefabs.RandomElement(), propPosition, Quaternion.identity) as Transform;

                newProp.SetParent(propHolder);

            }
            else {
                currentSpawnCount--;
            }
        }
    }

    private void GenerateTreasureChests() {
        string holderName = "Treasure";
        if (transform.FindChild(holderName)) {
            DestroyImmediate(transform.FindChild(holderName).gameObject);
        }
        Transform treasureHolder = new GameObject(holderName).transform;
        treasureHolder.SetParent(transform, false);

        // Spawning treasure
        int treasureCount = (int)(currentMap.mapSize.column * currentMap.mapSize.row * currentMap.treasurePercent);
        int currentSpawnCount = 0;
        for (int i = 0; i < treasureCount; i++) {
            GridCoord randomCoord = GetRandomCoord();
            currentSpawnCount++;
            if (randomCoord != currentMap.mapCenter) {
                Vector3 propPosition = CoordToPosition(randomCoord.column, randomCoord.row);
                Transform newProp = Instantiate(treasureChestPrefabs.RandomElement(), propPosition, Quaternion.identity) as Transform;

                newProp.SetParent(treasureHolder);
            }
            else {
                currentSpawnCount--;
            }
        }
    }

    #endregion PUBLIC_METHODS

    void Update() {

    }
}
