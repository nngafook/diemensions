﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class FogTile : MonoBehaviour {

    private SpriteRenderer spriteRenderer;
    private GridCoord gridCoord;
    private float fadedAmount = 0.9f;
    private bool isRevealed = false;
    private bool isFaded = false;
    // Used in the cheater
    private Color baseColor;
    private bool isCheatHidden = false;

    public GridCoord GridPos { get { return gridCoord; } }
    public bool IsRevealed { get { return isRevealed; } }
    public bool IsFaded { get { return isFaded; } }
    public bool IsFullyRevealed { get { return (isRevealed && !isFaded); } }

    #region PRIVATE_METHODS
    void Awake() {
        spriteRenderer = this.GetComponent<SpriteRenderer>();
        baseColor = spriteRenderer.color;
    }

    void Start() {
        this.name = "FogTile (" + (gridCoord.column.ToString() + ", " + gridCoord.row.ToString()) + ")";
    }
    #endregion PRIVATE_METHODS

    #region PUBLIC_METHODS
    public void SetGridPos(int x, int y) {
        gridCoord = new GridCoord(x, y);
    }

    public void Reveal() {
        isRevealed = true;
        Color c = spriteRenderer.color;
        c.a = 0;
        spriteRenderer.color = c;
        isFaded = false;
    }

    public void Hide() {
        isRevealed = false;
        Color c = spriteRenderer.color;
        c.a = 1;
        spriteRenderer.color = c;
    }

    public void SetFaded() {
        Color c = spriteRenderer.color;
        c.a = (isCheatHidden) ? 0 : fadedAmount;
        spriteRenderer.color = c;
        isFaded = true;
    }

    #region CHEAT
    public void SetVisible(bool value) {
        isCheatHidden = !value;
        if (value) {
            spriteRenderer.color = baseColor;
        }
        else {
            Color c = new Color(0, 0, 0, 0);
            spriteRenderer.color = c;
        }
    }
    #endregion CHEAT

    #endregion PUBLIC_METHODS

    void Update() {

    }
}
