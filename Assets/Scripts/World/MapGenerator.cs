﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MapGenerator : MonoBehaviour {

    public Map currentMap;
    public Transform tilePrefab;
    public Vector2 maxMapSize;

    public Transform[] tilePrefabs;

    [Range(0, 1)]
    public float outlinePercent;
    public float tileSize;
    public float TileSize { get { return tileSize; } }

    protected int totalEnemiesSpawned = 0;
    public int TotalEnemiesSpawned { get { return totalEnemiesSpawned; } }

    protected Pathfinder pathfinder;
    protected List<GridCoord> allTileCoords;
    protected Queue<GridCoord> shuffledTileCoords;
    protected List<Tile> allTiles;
    protected Transform[,] tileMap;
    public Pathfinder Pathfinder { get { return pathfinder; } }
    public Map CurrentMap { get { return currentMap; } }
    public List<Tile> AllTiles { get { return allTiles; } }

    protected virtual void Awake() {
        pathfinder = this.GetComponent<Pathfinder>();
    }

    public virtual void GenerateMap() {

    }

    protected bool MapIsFullyAccessible(bool[,] obstacleMap, int currentObstacleCount) {
        bool[,] mapFlags = new bool[obstacleMap.GetLength(0), obstacleMap.GetLength(1)];
        Queue<GridCoord> queue = new Queue<GridCoord>();
        queue.Enqueue(currentMap.mapCenter);
        mapFlags[currentMap.mapCenter.column, currentMap.mapCenter.row] = true;

        int accessibleTileCount = 1;

        while (queue.Count > 0) {
            GridCoord tile = queue.Dequeue();

            for (int x = -1; x <= 1; x++) {
                for (int y = -1; y <= 1; y++) {
                    int neighbourX = tile.column + x;
                    int neighbourY = tile.row + y;

                    if ((x == 0) || (y == 0)) {
                        if ((neighbourX >= 0) && (neighbourX < obstacleMap.GetLength(0)) && (neighbourY >= 0) && (neighbourY < obstacleMap.GetLength(1))) {
                            if ((!mapFlags[neighbourX, neighbourY]) && (!obstacleMap[neighbourX, neighbourY])) {
                                mapFlags[neighbourX, neighbourY] = true;
                                queue.Enqueue(new GridCoord(neighbourX, neighbourY));
                                accessibleTileCount++;
                            }
                        }
                    }
                }
            }
        }
        int targetAccessibleTileCount = (int)(currentMap.mapSize.column * currentMap.mapSize.row - currentObstacleCount);
        return targetAccessibleTileCount == accessibleTileCount;

    }

    // REVIEW THIS, might not be needed. Doing some calculations that were probably only needed when 3d

    protected Vector3 CoordToPosition(int column, int row) {
        return new Vector3(-currentMap.mapSize.column / 2f + 0.5f + column, -currentMap.mapSize.row / 2f + 0.5f + row, 0) * tileSize;
    }

    protected GridCoord GetRandomCoord() {
        GridCoord randomCoord = shuffledTileCoords.Dequeue();
        shuffledTileCoords.Enqueue(randomCoord);
        return randomCoord;
    }

    //private Transform GetRandomOpenTile() {
    //    GridCoord randomCoord = shuffledOpenTileCoords.Dequeue();
    //    shuffledOpenTileCoords.Enqueue(randomCoord);
    //    return tileMap[randomCoord.column, randomCoord.row];
    //}

    public Tile GetTileFromPosition(Vector3 position) {
        Tile rVal = null;

        int column = Mathf.RoundToInt(position.x / TileSize + (currentMap.mapSize.column - 1) / 2f);
        int row = Mathf.RoundToInt(position.y / TileSize + (currentMap.mapSize.row - 1) / 2f);
        if (Utility.IsWithinGrid(column, row, currentMap.mapSize.column, currentMap.mapSize.row)) {
            rVal = tileMap[column, row].GetComponent<Tile>();
        }
        return rVal;
    }

    public Tile GetTileFromGridCoord(int coordColumn, int coordRow) {
        Tile rVal = null;
        if (Utility.IsWithinGrid(coordColumn, coordRow, currentMap.mapSize.column, currentMap.mapSize.row)) {
            rVal = GetTileFromPosition(CoordToPosition(coordColumn, coordRow));
        }
        return rVal;
    }

    public Tile GetRandomOpenTile() {
        Tile rVal = null;
        while (rVal == null) {
            rVal = GetTileFromGridCoord(Random.Range(0, currentMap.mapSize.column), Random.Range(0, currentMap.mapSize.row));
            if (rVal.IsOccupied) {
                rVal = null;
            }
        }
        return rVal;
    }

    /// <summary>
    /// Returns top, below, left and right neighbours
    /// </summary>
    /// <param craftableName="node"></param>
    /// <param craftableName="rVal"></param>
    public void GetDirectNeighbours(Node node, ref List<Node> rVal) {
        Tile tile = GetTileFromPosition(node.worldPosition);

        // left
        int column = tile.GridPos.column - 1;
        int row = tile.GridPos.row;
        if (Utility.IsWithinGrid(column, row, currentMap.mapSize.column, currentMap.mapSize.row)) {
            rVal.Add(GetTileFromGridCoord(column, row).Node);
        }

        // right
        column = tile.GridPos.column + 1;
        row = tile.GridPos.row;
        if (Utility.IsWithinGrid(column, row, currentMap.mapSize.column, currentMap.mapSize.row)) {
            rVal.Add(GetTileFromGridCoord(column, row).Node);
        }

        // up
        column = tile.GridPos.column;
        row = tile.GridPos.row + 1;
        if (Utility.IsWithinGrid(column, row, currentMap.mapSize.column, currentMap.mapSize.row)) {
            rVal.Add(GetTileFromGridCoord(column, row).Node);
        }

        // down
        column = tile.GridPos.column;
        row = tile.GridPos.row - 1;
        if (Utility.IsWithinGrid(column, row, currentMap.mapSize.column, currentMap.mapSize.row)) {
            rVal.Add(GetTileFromGridCoord(column, row).Node);
        }

        // Gets all neighbours around
        //for (int x = -1; x <= 1; x++) {
        //    for (int y = -1; y <= 1; y++) {
        //        if (x == 0 && y == 0) {
        //            continue;
        //        }

        //        int checkColumn = tile.GridPos.column + x;
        //        int checkRow = tile.GridPos.row + y;
        //        if (Utility.IsWithinGrid(checkColumn, checkRow, (int)MapWidth, (int)MapHeight)) {
        //            rVal.Add(GetTileFromGridCoord(checkColumn, checkRow).Node);
        //        }
        //    }
        //}
    }

}
