﻿using System.Collections.Generic;
using UnityEngine;

public class HubMapGenerator : MapGenerator {

    [Header("Hub Map Generator Variables")]
    public Transform[] possiblePropPrefabs;

    protected override void Awake() {
        base.Awake();
    }

    override public void GenerateMap() {
        tileMap = new Transform[currentMap.mapSize.column, currentMap.mapSize.row];
        allTiles = new List<Tile>();

        // Generating coords
        allTileCoords = new List<GridCoord>();
        for (int x = 0; x < currentMap.mapSize.column; x++) {
            for (int y = 0; y < currentMap.mapSize.row; y++) {
                allTileCoords.Add(new GridCoord(x, y));
            }
        }
        shuffledTileCoords = new Queue<GridCoord>(Utility.ShuffleArray(allTileCoords.ToArray(), currentMap.seed));

        // Create map holder object
        string holderName = "Generated Map";
        if (transform.FindChild(holderName)) {
            DestroyImmediate(transform.FindChild(holderName).gameObject);
        }

        Transform mapHolder = new GameObject(holderName).transform;
        mapHolder.SetParent(transform, false);

        // Spawning tiles
        for (int column = 0; column < currentMap.mapSize.column; column++) {
            for (int row = 0; row < currentMap.mapSize.row; row++) {
                Vector3 tilePosition = CoordToPosition(column, row);
                Transform newTile = Instantiate(tilePrefab, tilePosition, Quaternion.identity) as Transform;
                newTile.GetComponent<Tile>().SetGridPos(column, row);
                newTile.localScale = Vector3.one * (1 - outlinePercent) * tileSize;
                newTile.SetParent(mapHolder);
                tileMap[column, row] = newTile;
                allTiles.Add(newTile.GetComponent<Tile>());
            }
        }

        holderName = "Prop Entities";
        if (transform.FindChild(holderName)) {
            DestroyImmediate(transform.FindChild(holderName).gameObject);
        }
        Transform propHolder = new GameObject(holderName).transform;
        propHolder.SetParent(transform, false);

        // Spawning props
        int propCount = (int)(currentMap.mapSize.column * currentMap.mapSize.row * currentMap.propPercent);
        int currentPropCount = 0;
        for (int i = 0; i < propCount; i++) {
            GridCoord randomCoord = GetRandomCoord();
            currentPropCount++;
            if (randomCoord != currentMap.mapCenter) {
                Vector3 propPosition = CoordToPosition(randomCoord.column, randomCoord.row);
                Transform newProp = Instantiate(possiblePropPrefabs.RandomElement(), propPosition, Quaternion.identity) as Transform;

                newProp.SetParent(propHolder);
            }
            else {
                currentPropCount--;
            }
        }

    }

}
