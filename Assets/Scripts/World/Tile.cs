﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Tile : MonoBehaviour {

    private SpriteRenderer spriteRenderer;
    private Color baseColor;
    private Color currentColor;
    private Color previousColor;
    private GridCoord gridCoord;
    private Entity entityOccupying;
    private InteractiveEntity interactiveEntityOccupying;
    private Node node;
    private float tileSize = 200;
    // Used for any kind of highlighting. (movement, range or whatever else)
    private bool isHighlighted = false;
    private bool isOccupied = false;
    private bool isInteractiveOccupied = false;

    [Header("Colors")]
    public Color movementHighlightColor;
    public Color attackRangeHighlightColor;
    public Color attackMissHighlightColor;
    public Color occupiedColor;

    #region ACCESSORS_MUTATORS
    public GridCoord GridPos { get { return gridCoord; } }
    public Node Node { get { node.Update(); return node; } }
    public bool IsHighlighted { get { return isHighlighted; } }
    public bool IsOccupied { get { return isOccupied; } }
    public bool IsInteractiveOccupied { get { return isInteractiveOccupied; } }
    public Entity EntityOccupying { get { return entityOccupying; } }
    public InteractiveEntity InteractiveEntity { get { return interactiveEntityOccupying; } }
    public Vector2 Position { get { return (Vector2)transform.position; } }
    public Vector2 TopLeft { get { return Position.AddXY(-tileSize, tileSize); } }
    public Vector2 TopRight { get { return Position.AddXY(tileSize, tileSize); } }
    public Vector2 BottomLeft { get { return Position.AddXY(-tileSize, -tileSize); } }
    public Vector2 BottomRight { get { return Position.AddXY(tileSize, -tileSize); } }
    #endregion ACCESSORS_MUTATORS

    #region PRIVATE_METHODS
    void Awake() {
        spriteRenderer = this.GetComponent<SpriteRenderer>();
        baseColor = spriteRenderer.color;
        currentColor = baseColor;
        node = new Node(this);

        if (tileSize != MapManager.Instance.TileSize) {
            Debug.LogError("The tileSize set in Tile.cs is not the same as the tileSize in MapManager".Bold().Sized(15));
        }
    }

    void Start() {
        this.name = "Tile (" + (gridCoord.column.ToString() + ", " + gridCoord.row.ToString()) + ")";
    }

    private void SetColor(Color c) {
        previousColor = currentColor;
        currentColor = c;
        spriteRenderer.color = currentColor;
    }
    #endregion PRIVATE_METHODS


    #region PUBLIC_METHODS
    public void SetGridPos(int x, int y) {
        node = new Node(this);
        gridCoord = new GridCoord(x, y);
        node.Update();
    }

    public void ClearCosts() {
        node.ClearCosts();
    }

    /// <summary>
    /// Reverts the current color to the previous
    /// </summary>
    public void RevertHighlight() {
        SetColor(previousColor);
    }

    public void SetHighlight(CommandType commandType) {
        isHighlighted = true;
        Color c = baseColor;

        if (isHighlighted) {
            switch (commandType) {
                case CommandType.ATTACK:
                    c = attackRangeHighlightColor;
                    break;
                case CommandType.ATTACK_MISS:
                    c = attackMissHighlightColor;
                    break;
                case CommandType.MOVE:
                    c = movementHighlightColor;
                    break;
                case CommandType.NONE:
                    isHighlighted = false;
                    previousColor = baseColor;
                    c = baseColor;
                    break;
                default:
                    break;
            }
        }
        SetColor(c);
    }

    public void SetAttackRangeHighlight(bool value) {
        isHighlighted = value;
        if (isHighlighted) {
            SetColor(attackRangeHighlightColor);
        }
        else {
            SetColor(baseColor);
        }
    }

    public void ClearHighlight() {
        if (isOccupied) {
            previousColor = baseColor;
            SetColor(occupiedColor);
        }
        else {
            SetHighlight(CommandType.NONE);
        }
    }

    /// <summary>
    /// If the entity is interactive, then isOccupied remains false
    /// </summary>
    /// <param craftableName="value"></param>
    /// <param craftableName="entity"></param>
    public void SetOccupied(bool value, Entity entity) {
        if (value) {
            if (entity.entityPropertiesData.entityType == EntityType.INTERACTIVE) {
                isInteractiveOccupied = true;
                interactiveEntityOccupying = entity.GetComponent<InteractiveEntity>();
            }
            else if (entity.entityPropertiesData.entityType == EntityType.PROP) {
                // Do props do anything to tiles when registered?
                if (entity.entityPropertiesData.occupiesTile) {
                    isOccupied = true;
                    entityOccupying = entity;
                    SetColor(occupiedColor);
                }
            }
            else {
                isOccupied = true;
                entityOccupying = entity;
                SetColor(occupiedColor);
            }
        }
        else {
            if (entity.entityPropertiesData.entityType == EntityType.INTERACTIVE) {
                isInteractiveOccupied = false;
                interactiveEntityOccupying = null;
            }
            else {
                isOccupied = false;
                entityOccupying = null;
                SetColor(baseColor);
            }
        }
        //isOccupied = (isInteractiveOccupied) ? false : value;
        //if (isOccupied) {
        //    entityOccupying = entity;
        //    SetColor(occupiedColor);
        //}
        //else {
        //    entityOccupying = null;
        //    SetColor(baseColor);
        //}
    }

    public void SetAsWall() {
        isOccupied = true;
    }

    /// <summary>
    /// Removes the entity and returns the InteractiveEntity component
    /// </summary>
    public void RemoveInteractiveEntity() {
        interactiveEntityOccupying.GetGot();
        SetOccupied(false, interactiveEntityOccupying.EntityComponent);
    }

    #endregion PUBLIC_METHODS

    // Update is called once per frame
    void Update() {
        
    }
}
