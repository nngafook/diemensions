
Dummy.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
Dummy/Dummy_arm_lower_far
  rotate: false
  xy: 946, 564
  size: 72, 114
  orig: 72, 114
  offset: 0, 0
  index: -1
Dummy/Dummy_arm_upper_far
  rotate: false
  xy: 173, 6
  size: 99, 180
  orig: 99, 180
  offset: 0, 0
  index: -1
Dummy/Dummy_arm_upper_near
  rotate: false
  xy: 274, 6
  size: 99, 180
  orig: 99, 180
  offset: 0, 0
  index: -1
Dummy/Dummy_foot_near_1
  rotate: true
  xy: 874, 14
  size: 168, 80
  orig: 168, 80
  offset: 0, 0
  index: -1
Dummy/Dummy_gun
  rotate: false
  xy: 618, 184
  size: 402, 160
  orig: 402, 160
  offset: 0, 0
  index: -1
Dummy/Dummy_hand_1_fistBack
  rotate: true
  xy: 946, 680
  size: 94, 73
  orig: 94, 73
  offset: 0, 0
  index: -1
Dummy/Dummy_leg_lower_far
  rotate: true
  xy: 438, 213
  size: 131, 178
  orig: 131, 178
  offset: 0, 0
  index: -1
Dummy/Dummy_leg_lower_near_1
  rotate: false
  xy: 375, 2
  size: 131, 204
  orig: 131, 204
  offset: 0, 0
  index: -1
Dummy/Dummy_leg_lower_near_2
  rotate: false
  xy: 508, 4
  size: 131, 178
  orig: 131, 178
  offset: 0, 0
  index: -1
Dummy/Dummy_leg_upper_far
  rotate: false
  xy: 946, 776
  size: 76, 163
  orig: 76, 163
  offset: 0, 0
  index: -1
Dummy/Dummy_leg_upper_near
  rotate: false
  xy: 946, 776
  size: 76, 163
  orig: 76, 163
  offset: 0, 0
  index: -1
Dummy/Dummy_neck
  rotate: true
  xy: 959, 943
  size: 70, 62
  orig: 70, 62
  offset: 0, 0
  index: -1
Dummy/Dummy_pistol
  rotate: false
  xy: 641, 7
  size: 231, 175
  orig: 231, 175
  offset: 0, 0
  index: -1
Dummy/Dummy_pistol_shoot
  rotate: false
  xy: 2, 188
  size: 297, 187
  orig: 297, 187
  offset: 0, 0
  index: -1
Dummy/Dummy_sword_1
  rotate: true
  xy: 331, 941
  size: 72, 626
  orig: 72, 626
  offset: 0, 0
  index: -1
Dummy/Dummy_sword_2_swing
  rotate: false
  xy: 2, 377
  size: 327, 636
  orig: 327, 636
  offset: 0, 0
  index: -1
Dummy/Dummy_sword_3_bigSwing
  rotate: false
  xy: 331, 346
  size: 613, 593
  orig: 613, 593
  offset: 0, 0
  index: -1
Dummy/Dummy_torso
  rotate: false
  xy: 2, 5
  size: 169, 181
  orig: 169, 181
  offset: 0, 0
  index: -1
Dummy/Dummy_waist
  rotate: false
  xy: 301, 208
  size: 135, 136
  orig: 135, 136
  offset: 0, 0
  index: -1

Dummy2.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
Dummy/Dummy_arm_lower_near
  rotate: true
  xy: 800, 750
  size: 79, 126
  orig: 79, 126
  offset: 0, 0
  index: -1
Dummy/Dummy_foot_far_1
  rotate: false
  xy: 631, 748
  size: 167, 81
  orig: 167, 81
  offset: 0, 0
  index: -1
Dummy/Dummy_foot_far_2_bent
  rotate: false
  xy: 2, 3
  size: 157, 109
  orig: 157, 109
  offset: 0, 0
  index: -1
Dummy/Dummy_foot_near_2_bent
  rotate: false
  xy: 472, 720
  size: 157, 109
  orig: 157, 109
  offset: 0, 0
  index: -1
Dummy/Dummy_gun_shoot
  rotate: false
  xy: 2, 652
  size: 468, 177
  orig: 468, 177
  offset: 0, 0
  index: -1
Dummy/Dummy_hand_2_fistPalm
  rotate: false
  xy: 366, 39
  size: 83, 77
  orig: 83, 77
  offset: 0, 0
  index: -1
Dummy/Dummy_hand_3_open
  rotate: false
  xy: 161, 2
  size: 113, 110
  orig: 113, 110
  offset: 0, 0
  index: -1
Dummy/Dummy_head_1
  rotate: false
  xy: 2, 114
  size: 265, 265
  orig: 265, 265
  offset: 0, 0
  index: -1
Dummy/Dummy_head_2_injured
  rotate: false
  xy: 269, 385
  size: 265, 265
  orig: 265, 265
  offset: 0, 0
  index: -1
Dummy/Dummy_head_3_happy
  rotate: false
  xy: 2, 381
  size: 265, 269
  orig: 265, 269
  offset: 0, 0
  index: -1
Dummy/Dummy_head_4_wideEyes
  rotate: false
  xy: 269, 118
  size: 265, 265
  orig: 265, 265
  offset: 0, 0
  index: -1
Dummy/Dummy_machineGun
  rotate: false
  xy: 540, 831
  size: 437, 161
  orig: 437, 161
  offset: 0, 0
  index: -1
Dummy/Dummy_machineGun_clip
  rotate: false
  xy: 276, 23
  size: 88, 93
  orig: 88, 93
  offset: 0, 0
  index: -1
Dummy/Dummy_machineGun_shoot
  rotate: false
  xy: 2, 831
  size: 536, 161
  orig: 536, 161
  offset: 0, 0
  index: -1
