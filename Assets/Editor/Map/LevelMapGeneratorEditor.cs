﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(LevelMapGenerator))]
public class LevelMapGeneratorEditor : Editor {

    public override void OnInspectorGUI() {
        LevelMapGenerator mapGenerator = target as LevelMapGenerator;
        if (mapGenerator.currentMap.mapSize.column % 2 != 0) {
            EditorGUILayout.HelpBox("The number of columns in the map MUST be even!", MessageType.Warning);
        }
        
        if (mapGenerator.currentMap.mapSize.row % 2 != 0) {
            EditorGUILayout.HelpBox("The number of rows in the map MUST be even!", MessageType.Warning);
        }

        base.DrawDefaultInspector();
    }

}
