﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(FogManager))]
public class FogManagerEditor : Editor {

    public override void OnInspectorGUI() {
        FogManager map = target as FogManager;
        if (DrawDefaultInspector()) {
            map.GenerateMap();
        }

        if (GUILayout.Button("Generate Map")) {
            map.GenerateMap();
        }

    }

}
