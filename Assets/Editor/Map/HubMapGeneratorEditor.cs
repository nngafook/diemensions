﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(HubMapGenerator))]
public class HubMapGeneratorEditor : Editor {

    public override void OnInspectorGUI() {
        HubMapGenerator map = target as HubMapGenerator;
        if (DrawDefaultInspector()) {
            map.GenerateMap();
        }

        if (GUILayout.Button("Generate Map")) {
            map.GenerateMap();
        }

    }

}
