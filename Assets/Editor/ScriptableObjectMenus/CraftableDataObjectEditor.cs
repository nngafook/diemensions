﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(CraftableDataObject))]
public class CraftableDataObjectEditor : Editor {

    private CraftableDataObject dataObject;

    public override void OnInspectorGUI() {
        dataObject = target as CraftableDataObject;

        dataObject.itemName = EditorGUILayout.TextField("Name", dataObject.itemName);
        dataObject.id = EditorGUILayout.TextField("ID", dataObject.id);
        dataObject.uiIcon = EditorGUILayout.TextField("UI Icon", dataObject.uiIcon);
        dataObject.recipeDataObject = (RecipeDataObject)EditorGUILayout.ObjectField("Recipe Data Object", dataObject.recipeDataObject, typeof(RecipeDataObject), true);

        dataObject.itemType = (ItemType)EditorGUILayout.EnumPopup("Item Type", dataObject.itemType);

        if (dataObject.itemType == ItemType.ARMOR_SET) {
            GUILayout.Label("Offensive Stats", EditorStyles.boldLabel);
            dataObject.attackDataObject = (AttackDataObject)EditorGUILayout.ObjectField("Attack Data Object", dataObject.attackDataObject, typeof(AttackDataObject), true);
            dataObject.classRequirement = (CharacterClass)EditorGUILayout.EnumPopup("Required Class", dataObject.classRequirement);

            GUILayout.Label("Defensive Stats", EditorStyles.boldLabel);
            dataObject.physicalDefense = EditorGUILayout.FloatField("Physical Defense", dataObject.physicalDefense);
            dataObject.magicDefense = EditorGUILayout.FloatField("Magic Defense", dataObject.magicDefense);

            dataObject.physicalAttack = EditorGUILayout.FloatField("Physical Attack", dataObject.physicalAttack);
            dataObject.magicAttack = EditorGUILayout.FloatField("Magic Attack", dataObject.magicAttack);
            dataObject.critDamage = EditorGUILayout.FloatField("Crit Damage", dataObject.critDamage);
            dataObject.focus = EditorGUILayout.Slider(new GUIContent("Focus", "Value is added onto the base focus of the entity"), dataObject.focus, 0, 100);
        }
        else {

        }

        serializedObject.ApplyModifiedProperties();
    }

}
