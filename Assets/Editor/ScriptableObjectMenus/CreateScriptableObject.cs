﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class CreateScriptableObject {


    [MenuItem("Assets/Create/ScriptableObjects/AI")]
    public static void CreateAIData() {
        int suffix = Random.Range(1, 6000);
        EnemyAIDataObject asset = ScriptableObject.CreateInstance<EnemyAIDataObject>();
        AssetDatabase.CreateAsset(asset, "Assets/Resources/Data/AI/EnemyAI_" + suffix.ToString() + ".asset");
        AssetDatabase.SaveAssets();
        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/ScriptableObjects/RangedAttack")]
    public static void CreateRangedAttack() {
        int suffix = Random.Range(1, 6000);
        AttackDataObject asset = ScriptableObject.CreateInstance<AttackDataObject>();
        AssetDatabase.CreateAsset(asset, "Assets/Resources/Data/Attacks/Ranged/Attack_" + suffix.ToString() + ".asset");
        asset.attackType = AttackType.RANGED;
        AssetDatabase.SaveAssets();
        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/ScriptableObjects/MeleeAttack")]
    public static void CreateMeleeAttack() {
        int suffix = Random.Range(1, 6000);
        AttackDataObject asset = ScriptableObject.CreateInstance<AttackDataObject>();
        AssetDatabase.CreateAsset(asset, "Assets/Resources/Data/Attacks/Melee/Attack_" + suffix.ToString() + ".asset");
        asset.attackType = AttackType.MELEE;
        AssetDatabase.SaveAssets();
        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/ScriptableObjects/EntityProperties")]
    public static void CreateEntityProperties() {
        int suffix = Random.Range(1, 6000);
        EntityPropertiesDataObject asset = ScriptableObject.CreateInstance<EntityPropertiesDataObject>();
        AssetDatabase.CreateAsset(asset, "Assets/Resources/Data/Entities/EntityProperties/EntityProperties_" + suffix.ToString() + ".asset");
        AssetDatabase.SaveAssets();
        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/ScriptableObjects/SFX")]
    public static void CreateSFX() {
        SFXScriptable asset = ScriptableObject.CreateInstance<SFXScriptable>();
        AssetDatabase.CreateAsset(asset, "Assets/Data/ScriptableObjects/Audio/SFX.asset");
        AssetDatabase.SaveAssets();
        Selection.activeObject = asset;
    }

    

}
