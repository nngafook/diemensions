﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(EnemyAIDataObject))]
public class EnemyAIDataObjectEditor : Editor {

    private float buttonPadding = 12;
    private EnemyAIDataObject dataObject;

    public override void OnInspectorGUI() {
        dataObject = target as EnemyAIDataObject;

        dataObject.propertiesData = (EntityPropertiesDataObject)EditorGUILayout.ObjectField("Entity Properties Data Object", dataObject.propertiesData, typeof(EntityPropertiesDataObject), true);

        GUILayout.Space(5);

        dataObject.aggression = (Aggression)EditorGUILayout.EnumPopup("Aggression", dataObject.aggression);
        dataObject.distancePreference = (DistancePreference)EditorGUILayout.EnumPopup("Distance Preference", dataObject.distancePreference);
        
        GUILayout.Label("Attack IDs Priority", EditorStyles.boldLabel);
        for (int i = 0; i < dataObject.attackDataOrder.Count; i++) {
            EditorGUILayout.BeginHorizontal();

            GUILayout.Label(dataObject.attackDataOrder[i].name, GUILayout.Width((Screen.width / 2) - buttonPadding));
            if (GUILayout.Button("Remove", GUILayout.Width((Screen.width / 2) - buttonPadding))) {
                dataObject.attackDataOrder.RemoveAt(i);
                break;
            }

            EditorGUILayout.EndHorizontal();
        }

        GUILayout.Space(10);
        GUILayout.Label("Available Attacks", EditorStyles.boldLabel);
        if((dataObject.propertiesData != null) && (dataObject.propertiesData.attackDataObjects != null)) {
            List<AttackDataObject> attacks = new List<AttackDataObject>(dataObject.propertiesData.attackDataObjects);
            bool idAvailable = true;

            for (int i = 0; i < attacks.Count; i++) {
                idAvailable = true;

                for (int j = 0; j < dataObject.attackDataOrder.Count; j++) {
                    if (dataObject.attackDataOrder[j] == attacks[i]) {
                        idAvailable = false;
                        continue;
                    }
                }

                if (idAvailable) {
                    EditorGUILayout.BeginHorizontal();

                    GUILayout.Label(attacks[i].commandName, GUILayout.Width((Screen.width / 2) - buttonPadding));
                    if (GUILayout.Button("Add", GUILayout.Width((Screen.width / 2) - buttonPadding))) {
                        dataObject.attackDataOrder.Add(attacks[i]);
                    }

                    EditorGUILayout.EndHorizontal();
                }
            }
        }

        serializedObject.ApplyModifiedProperties();
    }

}
