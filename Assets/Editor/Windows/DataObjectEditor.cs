﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEditor.SceneManagement;
using System.IO;
using System.Collections.Generic;

public class DataObjectEditor : EditorWindow {

    private static DataObjectEditor instance;

    private int selectedCategoryIndex = -1;
    private int selectedItemIndex = -1;

    private Texture2D windowBGTexture;
    private Texture2D bodyPanelBGTexture;
    private Texture2D toolbarBGTexture;

    private Vector2 fileScrollPosition;
    private Vector2 inspectorScrollPosition;

    private List<List<FileInfo>> masterFileList;

    private string selectedObjectPath;
    private ScriptableObject selectedDataObject;
    private Editor editor;

    private float WindowWidth { get { return position.width; } }

    void Awake() {
        this.titleContent = new GUIContent("Data Object Editor");
        this.minSize = new Vector2(1136, 640);
    }

    void OnEnable() {
        instance = this;

        InitTextures();
        GetResourceDatabase();
    }

    private void InitTextures() {
        windowBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        windowBGTexture.SetPixel(0, 0, new Color(0.172549f, 0.2784314f, 0.4392157f, 1f));
        windowBGTexture.Apply();

        bodyPanelBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        bodyPanelBGTexture.SetPixel(0, 0, new Color(0.4588235f, 0.5411765f, 0.6588235f, 1f));
        bodyPanelBGTexture.Apply();

        toolbarBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        toolbarBGTexture.SetPixel(0, 0, new Color(0.07843138f, 0.1843137f, 0.3294118f, 1f));
        toolbarBGTexture.Apply();
    }

    private void GetResourceDatabase() {
        masterFileList = new List<List<FileInfo>>();

        string resourcesFolderPath = Application.dataPath + "/Resources/Data";
        DirectoryInfo levelDirectoryPath = new DirectoryInfo(resourcesFolderPath);
        FileInfo[] temp;// = levelDirectoryPath.GetFiles("*.*", SearchOption.AllDirectories);
        DirectoryInfo[] directoryInfos = levelDirectoryPath.GetDirectories();
        List<FileInfo> fileInfos;

        for (int i = 0; i < directoryInfos.Length; i++) {
            temp = directoryInfos[i].GetFiles("*.*", SearchOption.AllDirectories);
            fileInfos = new List<FileInfo>();
            for (int j = 0; j < temp.Length; j++) {
                if (temp[j].Extension == ".asset") {
                    fileInfos.Add(temp[j]);
                }
            }
            //fileInfos.Sort((x, y) => string.Compare(x.Name, y.Name));
            masterFileList.Add(fileInfos);
        }
    }

    #region EDITOR_MENU_METHODS
    [MenuItem("Window/Data Object Editor %&o")]
    static void Init() {
        // Get existing open window or if none, make a new one:
        DataObjectEditor window = (DataObjectEditor)EditorWindow.GetWindow(typeof(DataObjectEditor));
        window.Show();
    }
    #endregion EDITOR_MENU_METHODS

    /// <summary>
    /// Also returns the path without the extensions
    /// </summary>
    /// <param name="fullPath"></param>
    /// <returns></returns>
    private string StripResourcesPath(string fullPath) {
        fullPath = RemoveExtension(fullPath);
        string rVal = fullPath.Substring(fullPath.IndexOf("Data"));
        return rVal;
    }

    private string RemoveExtension(string filename) {
        return filename.Substring(0, filename.LastIndexOf("."));
    }

    private Texture2D MakeTexture(int width, int height, Color col) {
        Color[] pix = new Color[width * height];
        for (int i = 0; i < pix.Length; i++) {
            pix[i] = col;
        }
        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();
        return result;
    }

    private void DrawFolderNames() {
        GUILayout.BeginHorizontal(GUILayout.Width(WindowWidth / 2));

        GUIStyle normalStyle = new GUIStyle(GUI.skin.GetStyle("whiteLabel"));
        normalStyle.normal.background = MakeTexture((int)(WindowWidth / 2), (int)EditorGUIUtility.singleLineHeight, new Color(1.0f, 1.0f, 1.0f, 0.1f));
        GUIStyle selectedStyle = new GUIStyle(GUI.skin.GetStyle("Label"));
        selectedStyle.normal.background = MakeTexture((int)(WindowWidth / 2), (int)EditorGUIUtility.singleLineHeight, new Color(1.0f, 1.0f, 1.0f, 0.5f));
        selectedStyle.normal.textColor = Color.blue;
        string categoryName = string.Empty;
        string subCategoryName = string.Empty;

        GUILayout.BeginVertical();
        fileScrollPosition = GUILayout.BeginScrollView(fileScrollPosition);
        for (int i = 0; i < masterFileList.Count; i++) {
            categoryName = masterFileList[i][0].Directory.Name;
            GUILayout.Label(categoryName, EditorStyles.boldLabel);
            for (int j = 0; j < masterFileList[i].Count; j++) {
                if ((categoryName != masterFileList[i][j].Directory.Name) && (subCategoryName != masterFileList[i][j].Directory.Name)) {
                    subCategoryName = masterFileList[i][j].Directory.Name;
                    GUILayout.Label(subCategoryName, EditorStyles.boldLabel);
                }
                if (GUILayout.Button(RemoveExtension(masterFileList[i][j].Name), (((i == selectedCategoryIndex) && (j == selectedItemIndex)) ? selectedStyle : normalStyle))) {
                    if ((i != selectedCategoryIndex) || (j != selectedItemIndex)) {
                        selectedDataObject = (ScriptableObject)Resources.Load(StripResourcesPath(masterFileList[i][j].FullName));
                        editor = Editor.CreateEditor(selectedDataObject);
                        selectedCategoryIndex = i;
                        selectedItemIndex = j;
                    }
                }
            }
        }
        GUILayout.EndScrollView();
        GUILayout.EndVertical();

        GUILayout.EndHorizontal();
    }

    private void DrawInspector() {
        GUILayout.BeginHorizontal(GUILayout.Width(WindowWidth / 2));
        GUILayout.BeginArea(new Rect((WindowWidth / 2) + 10, 10, ((WindowWidth / 2) - 20), position.height));
        GUILayout.BeginVertical();

        inspectorScrollPosition = GUILayout.BeginScrollView(inspectorScrollPosition);
        if (selectedDataObject != null) {
            editor.OnInspectorGUI();
        }
        GUILayout.EndScrollView();

        GUILayout.EndVertical();
        GUILayout.EndArea();
        GUILayout.EndHorizontal();
    }

    private void ProcessInput() {
        if (Event.current.isKey && Event.current.type == EventType.KeyUp) {
            switch (Event.current.keyCode) {
                case KeyCode.F1:
                    if (selectedDataObject != null) {
                        //string path = AssetDatabase.GetAssetPath(selectedDataObject.GetInstanceID());
                        Selection.activeObject = selectedDataObject;
                        EditorGUIUtility.PingObject(Selection.activeObject);
                    }
                    break;
                case KeyCode.F5:
                    GetResourceDatabase();
                    break;
            }
        }
    }



    void OnGUI() {
        GUI.skin.button.wordWrap = true;
        GUI.DrawTexture(new Rect(0, 0, (position.width / 2), position.height), windowBGTexture, ScaleMode.StretchToFill);
        GUI.DrawTexture(new Rect((WindowWidth / 2), 0, position.width, position.height), bodyPanelBGTexture, ScaleMode.StretchToFill);

        GUILayout.BeginHorizontal();
        DrawFolderNames();
        DrawInspector();
        GUILayout.EndHorizontal();

        ProcessInput();

        if (GUI.changed) {
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }
    }


}
