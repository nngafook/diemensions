﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class ShortcutToolbox : EditorWindow {

    //private string RuntimeLevelEditorPath = "Assets/RuntimeLevelEditor/RTLevelEditor.unity";

    private Texture2D windowBGTexture;

    void OnEnable() {
        InitTextures();
    }

    private void InitTextures() {
        windowBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        windowBGTexture.SetPixel(0, 0, new Color(0.172549f, 0.2784314f, 0.4392157f, 1f));
        windowBGTexture.Apply();
    }

    static void LoadMainMenu() {
        EditorSceneManager.SaveOpenScenes();
        EditorSceneManager.OpenScene("Assets/Scenes/MainMenu.unity");
    }

    //[MenuItem("Shortcuts/Main Menu")]
    static void LoadMain() {
        EditorSceneManager.SaveOpenScenes();
        EditorSceneManager.OpenScene("Assets/Scenes/Main.unity");
    }

    static void LoadHub() {
        EditorSceneManager.SaveOpenScenes();
        EditorSceneManager.OpenScene("Assets/Scenes/Hub.unity");
    }

    static void LoadPlayground() {
        EditorSceneManager.SaveOpenScenes();
        EditorSceneManager.OpenScene("Assets/Scenes/Playground.unity");
    }

    static void LoadDialogTestingGround() {
        EditorSceneManager.SaveOpenScenes();
        EditorSceneManager.OpenScene("Assets/Scenes/DialogTestingGround.unity");
    }

    static void SelectCraftablesFolder() {
        Selection.activeObject = AssetDatabase.LoadMainAssetAtPath("Assets/Resources/Data/Crafting/Craftables");
    }

    static void SelectDataObjectScriptsFolder() {
        Selection.activeObject = AssetDatabase.LoadMainAssetAtPath("Assets/Scripts/Data/Crafting");
    }

    //[MenuItem("Shortcuts/Select Scripts")]
    //static void SelectScripts() {
    //    Selection.activeObject = AssetDatabase.LoadMainAssetAtPath("Assets/Scripts");
    //    EditorGUIUtility.PingObject(Selection.activeObject);
    //}

    //[MenuItem("Shortcuts/Levels")]
    //static void SelectLevels() {
    //    //Selection.activeObject = AssetDatabase.LoadMainAssetAtPath("Assets/RuntimeLevelEditor/Resources/Levels/Classic.xml");
    //}

    [MenuItem("Window/Shortcut Toolbox")]
    public static void ShowWindow() {
        EditorWindow.GetWindow(typeof(ShortcutToolbox));
    }

    void OnGUI() {
        GUI.DrawTexture(new Rect(0, 0, maxSize.x - 4, maxSize.y - 4), windowBGTexture, ScaleMode.StretchToFill);
        GUILayout.Space(10);

        GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        //GUILayout.BeginVertical();
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Load Main Menu")) {
            LoadMainMenu();
        }
        if (GUILayout.Button("Load Hub")) {
            LoadHub();
        }
        if (GUILayout.Button("Load Main")) {
            LoadMain();
        }
        if (GUILayout.Button("Load Playground")) {
            LoadPlayground();
        }
        GUILayout.EndHorizontal();
        
        GUILayout.Space(10);
        
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Load Dialog Testing")) {
            LoadDialogTestingGround();
        }
        //if (GUILayout.Button("Open Main")) {
        //    LoadMain();
        //}
        GUI.color = Color.white;
        GUILayout.EndHorizontal();

        GUILayout.Space(10);
        
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Select Craftable Data Objects")) {
            SelectCraftablesFolder();
        }

        if (GUILayout.Button("Select Data Object Scripts")) {
            SelectDataObjectScriptsFolder();
        }

        GUILayout.EndHorizontal();

        //GUILayout.BeginHorizontal();
        //if (GUILayout.Button("Select Scripts")) {
        //    SelectScripts();
        //}
        //GUILayout.EndHorizontal();

        //GUILayout.BeginHorizontal();
        //if (GUILayout.Button("Select Levels")) {
        //    //SelectLevels();
        //}
        //GUILayout.EndHorizontal();
        //GUILayout.EndVertical();
    }
}