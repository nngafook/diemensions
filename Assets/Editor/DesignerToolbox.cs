﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using UnityEditor.SceneManagement;
using Object = UnityEngine.Object;
using System;

public class DesignerToolbox : EditorWindow {

    private static DesignerToolbox instance;

    private enum SelectedOption {
        MELEE_ATTACK,
        RANGED_ATTACK
    }

    private Texture2D meleeAttackTexture;
    private Texture2D rangedAttackTexture;
    private Texture2D windowBGTexture;
    private Texture2D bodyPanelBGTexture;
    private Texture2D toolbarBGTexture;

    private SelectedOption currentSelectedOption = SelectedOption.MELEE_ATTACK;

    private float windowEdgePadding = 5;
    private float padding = 10;
    private float buttonPanelHeight = 70;
    private float buttonDimension = 60;


    #region ATTACK_DATA_OBJECT_VARIABLES
    private CommandType attackDataCommandType = CommandType.NONE;
    private AttackType attackDataAttackType = AttackType.MELEE;
    private AttackVFXType attackDataVFXType = AttackVFXType.NONE;
    private DamageType attackDataDamageType = DamageType.NONE;
    private string attackDataCommandName;
    private string attackDataID;
    private  int attackDataResourceCost;
    private int attackDataRange;
    private int attackDataAOERadius;
    private bool attackDataBlockedByTerrain;
    private RangedFloat attackDataBaseDamage = new RangedFloat();
    #endregion ATTACK_DATA_OBJECT_VARIABLES

    private float WindowWidth { get { return (position.width - (windowEdgePadding * 2)); } }
    private float BodyHeight { get { return (position.height - buttonPanelHeight - (windowEdgePadding * 3)); } }
    private float BodyY { get { return (windowEdgePadding * 2) + buttonPanelHeight; } }

    void Awake() {
        this.titleContent = new GUIContent("Designer Toolbox");
        this.minSize = new Vector2(800, 460);
    }

    void OnEnable() {
        instance = this;

        InitTextures();
    }

    private void InitTextures() {
        meleeAttackTexture = AssetDatabase.LoadAssetAtPath("Assets/Art/2D/UI/Icons/Meleeicon.png", typeof(Texture2D)) as Texture2D;
        rangedAttackTexture = AssetDatabase.LoadAssetAtPath("Assets/Art/2D/UI/Icons/Rangedicon.png", typeof(Texture2D)) as Texture2D;

        windowBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        windowBGTexture.SetPixel(0, 0, new Color(0.172549f, 0.2784314f, 0.4392157f, 1f));
        windowBGTexture.Apply();

        bodyPanelBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        bodyPanelBGTexture.SetPixel(0, 0, new Color(0.4588235f, 0.5411765f, 0.6588235f, 1f));
        bodyPanelBGTexture.Apply();

        toolbarBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        toolbarBGTexture.SetPixel(0, 0, new Color(0.07843138f, 0.1843137f, 0.3294118f, 1f));
        toolbarBGTexture.Apply();
    }

    #region EDITOR_MENU_METHODS
    [MenuItem("Window/Designer Toolbox %&e")]
    static void Init() {
        // Get existing open window or if none, make a new one:
        DesignerToolbox window = (DesignerToolbox)EditorWindow.GetWindow(typeof(DesignerToolbox));
        window.Show();
    }
    #endregion EDITOR_MENU_METHODS

    //////////////////////////////////////////////////////////////////////

    #region HORIZONTAL
    private T DrawHorizontalLabeledObjectField<T>(string label, Object obj) where T : Object {
        T rVal;
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        rVal = (T)EditorGUILayout.ObjectField(obj, obj.GetType());
        GUILayout.EndHorizontal();
        return rVal;
    }

    private T DrawHorizontalLabeledEnumField<T>(string label, Enum enumType) {
        T rVal;
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        rVal = (T)(object)EditorGUILayout.EnumPopup(enumType);
        GUILayout.EndHorizontal();
        return rVal;
    }

    private T DrawHorizontalLabeledEnumField<T>(string label, Enum enumType, GUILayoutOption labelWidth, GUILayoutOption enumWidth) {
        T rVal;
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label, labelWidth);
        rVal = (T)(object)EditorGUILayout.EnumPopup(enumType, enumWidth);
        GUILayout.EndHorizontal();
        return rVal;
    }

    private void DrawHorizontalLabeledTextField(string label, ref string value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.TextField(value);
        GUILayout.EndHorizontal();
    }

    private void DrawHorizontalLabeledTextField(string label, ref string value, GUILayoutOption labelWidth, GUILayoutOption valueWidth) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label, labelWidth);
        value = EditorGUILayout.TextField(value, valueWidth);
        GUILayout.EndHorizontal();
    }

    private void DrawHorizontalLabeledLabelField(string label, string value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        EditorGUILayout.LabelField(value);
        GUILayout.EndHorizontal();
    }

    private void DrawHorizontalLabeledFloatField(string label, ref float value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.FloatField(value);
        GUILayout.EndHorizontal();
    }

    private void DrawHorizontalLabeledIntField(string label, ref int value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.IntField(value);
        GUILayout.EndHorizontal();
    }

    private void DrawHorizontalLabeledBoolField(string label, ref bool value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.Toggle(value);
        GUILayout.EndHorizontal();
    }

    private void DrawHorizontalLabeledBoolField(string label, ref bool value, GUILayoutOption labelWidth, GUILayoutOption valueWidth) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label, labelWidth);
        value = EditorGUILayout.Toggle(value, valueWidth);
        GUILayout.EndHorizontal();
    }

    private void DrawHorizontalLabeledRangedFloatField(string label, ref RangedFloat value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);

        GUIStyle rightAlignedStyle = new GUIStyle(GUI.skin.GetStyle("Label"));
        rightAlignedStyle.alignment = TextAnchor.MiddleRight;

        EditorGUILayout.LabelField("Min", rightAlignedStyle);
        value.min = EditorGUILayout.FloatField(value.min);
        EditorGUILayout.LabelField("Max", rightAlignedStyle);
        value.max = EditorGUILayout.FloatField(value.max);

        GUILayout.EndHorizontal();
    }

    private void DrawHorizontalLabeledRangedFloatField(string label, ref RangedFloat value, GUILayoutOption labelWidth, GUILayoutOption valueWidth) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label, labelWidth);

        GUIStyle rightAlignedStyle = new GUIStyle(GUI.skin.GetStyle("Label"));
        rightAlignedStyle.alignment = TextAnchor.MiddleRight;

        EditorGUILayout.LabelField("Min", rightAlignedStyle, valueWidth);
        value.min = EditorGUILayout.FloatField(value.min);
        EditorGUILayout.LabelField("Max", rightAlignedStyle, valueWidth);
        value.max = EditorGUILayout.FloatField(value.max);

        GUILayout.EndHorizontal();
    }
    #endregion HORIZONTAL

    #region VERTICAL
    private void DrawVerticalLabeledTextField(string label, ref string value) {
        GUILayout.BeginVertical();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.TextField(value);
        GUILayout.EndVertical();
    }

    private void DrawVerticalLabeledTextField(string label, ref string value, GUILayoutOption labelWidth, GUILayoutOption valueWidth) {
        GUILayout.BeginVertical();
        EditorGUILayout.LabelField(label, labelWidth);
        value = EditorGUILayout.TextField(value, valueWidth);
        GUILayout.EndVertical();
    }

    private void DrawVerticalLabeledFloatField(string label, ref float value) {
        GUILayout.BeginVertical();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.FloatField(value);
        GUILayout.EndVertical();
    }

    private void DrawVerticalLabeledIntField(string label, ref int value) {
        GUILayout.BeginVertical();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.IntField(value);
        GUILayout.EndVertical();
    }

    private void DrawVerticalLabeledIntField(string label, ref int value, GUILayoutOption labelWidth, GUILayoutOption intWidth) {
        GUILayout.BeginVertical();
        EditorGUILayout.LabelField(label, labelWidth);
        value = EditorGUILayout.IntField(value, intWidth);
        GUILayout.EndVertical();
    }

    private T DrawVerticalLabeledEnumField<T>(string label, Enum enumType) {
        T rVal;
        GUILayout.BeginVertical();
        EditorGUILayout.LabelField(label);
        rVal = (T)(object)EditorGUILayout.EnumPopup(enumType);
        GUILayout.EndVertical();
        return rVal;
    }

    private T DrawVerticalLabeledEnumField<T>(string label, Enum enumType, GUILayoutOption labelWidth, GUILayoutOption enumWidth) {
        T rVal;
        GUILayout.BeginVertical();
        EditorGUILayout.LabelField(label, labelWidth);
        rVal = (T)(object)EditorGUILayout.EnumPopup(enumType, enumWidth);
        GUILayout.EndVertical();
        return rVal;
    }
    #endregion VERTICAL

    //////////////////////////////////////////////////////////////////////

    private void SetCurrentSelectedOption(SelectedOption option) {
        currentSelectedOption = option;
    }

    private void CreateAttackAsset() {
        string path = "";
        string assetName = "";
        switch (currentSelectedOption) {
            case SelectedOption.MELEE_ATTACK:
                assetName = (!string.IsNullOrEmpty(attackDataCommandName)) ? attackDataCommandName : "MeleeAttackDataObject_" + UnityEngine.Random.Range(0, 6000);
                path = "Assets/Resources/Data/Attacks/Melee/" + assetName + ".asset";
                break;
            case SelectedOption.RANGED_ATTACK:
                assetName = (!string.IsNullOrEmpty(attackDataCommandName)) ? attackDataCommandName : "RangedAttackDataObject_" + UnityEngine.Random.Range(0, 6000);
                path = "Assets/Resources/Data/Attacks/Ranged/" + assetName + ".asset";
                break;
            default:
                break;
        }
        AttackDataObject asset = ScriptableObject.CreateInstance<AttackDataObject>();
        AssetDatabase.CreateAsset(asset, path);
        SaveAttackObjectValues(ref asset);
        AssetDatabase.SaveAssets();
        Selection.activeObject = asset;
    }

    private void SaveAttackObjectValues(ref AttackDataObject asset) {
        asset.commandName = attackDataCommandName;
        asset.type = attackDataCommandType;
        asset.attackType = attackDataAttackType;
        asset.vfxType = attackDataVFXType;
        asset.damageType = attackDataDamageType;
        asset.ID = attackDataID;
        asset.resourceCost = attackDataResourceCost;
        asset.range = attackDataRange;
        asset.aoeRadius = attackDataAOERadius;
        asset.blockedByTerrain = attackDataBlockedByTerrain;
        asset.baseDamage = attackDataBaseDamage;

        attackDataCommandName = string.Empty;
        attackDataResourceCost = 0;
        attackDataRange = 0;
        attackDataBlockedByTerrain = false;
        attackDataVFXType = AttackVFXType.NONE;
        attackDataDamageType = DamageType.NONE;
        attackDataCommandType = CommandType.NONE ;
        attackDataBaseDamage = new RangedFloat();
    }

    //////////////////////////////////////////////////////////////////////

    #region DRAW_METHODS
    
    private void DrawButtons() {
        GUILayout.BeginArea(new Rect(windowEdgePadding, windowEdgePadding, WindowWidth, buttonPanelHeight), GUI.skin.box);
        GUI.DrawTexture(new Rect(2, 2, WindowWidth - 4, buttonPanelHeight - 4), toolbarBGTexture, ScaleMode.StretchToFill);
        GUILayout.BeginArea(new Rect((padding / 2), (padding / 2), WindowWidth - (padding * 2), buttonPanelHeight));

        GUILayout.BeginHorizontal();
        GUI.color = (currentSelectedOption == SelectedOption.MELEE_ATTACK) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
        if (GUILayout.Button(meleeAttackTexture, GUILayout.Width(buttonDimension), GUILayout.Height(buttonDimension))) {
            SetCurrentSelectedOption(SelectedOption.MELEE_ATTACK);
            attackDataAttackType = AttackType.MELEE;
        }

        GUI.color = (currentSelectedOption == SelectedOption.RANGED_ATTACK) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
        if (GUILayout.Button(rangedAttackTexture, GUILayout.Width(buttonDimension), GUILayout.Height(buttonDimension))) {
            SetCurrentSelectedOption(SelectedOption.RANGED_ATTACK);
            attackDataAttackType = AttackType.RANGED;
        }

        GUI.color = CustomColor.GetColor(ColorName.PURPLE_PLUM);
        if (GUILayout.Button("Game Objective", GUILayout.Width(buttonDimension), GUILayout.Height(buttonDimension))) {
            GameObjective asset = ScriptableObject.CreateInstance<GameObjective>();
            string path = "Assets/Resources/Data/GameProgression/GameObjectiveDataObject.asset";
            AssetDatabase.CreateAsset(asset, path);
            AssetDatabase.SaveAssets();
            Selection.activeObject = asset;
        }
        GUI.color = Color.white;
        GUILayout.EndHorizontal();

        GUILayout.EndArea();
        GUILayout.EndArea();
    }

    private void DrawBody() {
        GUILayout.BeginArea(new Rect((padding / 2), BodyY, WindowWidth, BodyHeight), GUI.skin.box);
        GUI.DrawTexture(new Rect(2, 2, WindowWidth - 4, BodyHeight - 4), bodyPanelBGTexture, ScaleMode.StretchToFill);
        GUILayout.BeginArea(new Rect((windowEdgePadding), (windowEdgePadding), WindowWidth - (windowEdgePadding * 3), BodyHeight));

        GUILayout.BeginVertical();

        attackDataID = (!string.IsNullOrEmpty(attackDataCommandName)) ? (attackDataCommandName.ToLower() + "_id") : "";
        DrawHorizontalLabeledLabelField("ID", attackDataID);
        DrawHorizontalLabeledTextField("Command Name", ref attackDataCommandName);
        DrawHorizontalLabeledIntField("Resource Cost", ref attackDataResourceCost);

        attackDataCommandType = DrawHorizontalLabeledEnumField<CommandType>("Command Type", attackDataCommandType);
        attackDataAttackType = DrawHorizontalLabeledEnumField<AttackType>("Attack Type", attackDataAttackType);
        attackDataDamageType = DrawHorizontalLabeledEnumField<DamageType>("Damage Type", attackDataDamageType);
        attackDataVFXType = DrawHorizontalLabeledEnumField<AttackVFXType>("VFX Type", attackDataVFXType);

        DrawHorizontalLabeledIntField("Attack Range", ref attackDataRange);
        DrawHorizontalLabeledIntField("AOE Radius", ref attackDataAOERadius);
        DrawHorizontalLabeledRangedFloatField("Base Damage", ref attackDataBaseDamage, GUILayout.Width((WindowWidth / 2) - 15), GUILayout.Width(30));
        // For some reason, the width of the toggle is 15, and the label was overlapping the area for the toggle, so half window width - half of the toggle width (15 / 2)
        DrawHorizontalLabeledBoolField("Blocked By Terrain", ref attackDataBlockedByTerrain, GUILayout.Width((WindowWidth / 2) - 7), GUILayout.Width(WindowWidth / 2));

        DrawFooter();

        GUILayout.EndVertical();

        GUILayout.EndArea();
        GUILayout.EndArea();
    }

    private void DrawFooter() {
        if ((currentSelectedOption == SelectedOption.MELEE_ATTACK) || (currentSelectedOption == SelectedOption.RANGED_ATTACK)) {
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
            if (GUILayout.Button("Create", GUILayout.Width(100))) {
                CreateAttackAsset();
            }
            GUI.color = Color.white;
            GUILayout.EndHorizontal();
        }
    }

    #endregion DRAW_METHODS




    void OnGUI() {
        GUI.skin.button.wordWrap = true;
        GUI.DrawTexture(new Rect(0, 0, maxSize.x - 4, maxSize.y - 4), windowBGTexture, ScaleMode.StretchToFill);
        
        DrawButtons();
        DrawBody();

        if (GUI.changed) {
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }
    }
}
