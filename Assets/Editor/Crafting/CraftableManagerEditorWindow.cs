using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using System;
using UnityEditor.SceneManagement;
using Object = UnityEngine.Object;

public class CraftableManagerEditorWindow : EditorWindow {

    private static CraftableManagerEditorWindow instance;

    private enum PanelState {
        CRAFTABLE,
        RECIPE,
        COMPONENT
    }

    private const string SEARCH_BAR_CONTROL_NAME = "SearchBarControl";
    private Texture2D recipeComponentsIcon = AssetDatabase.LoadAssetAtPath("Assets/Art/2D/UI/Editor/ComponentsIcon.png", typeof(Texture2D)) as Texture2D;
    private Texture2D gameItemIcon = AssetDatabase.LoadAssetAtPath("Assets/Art/2D/UI/Editor/GameItemIcon.png", typeof(Texture2D)) as Texture2D;
    private Texture2D recipeIcon = AssetDatabase.LoadAssetAtPath("Assets/Art/2D/UI/Editor/RecipeIcon.png", typeof(Texture2D)) as Texture2D;

    private Texture2D windowBGTexture;
    private Texture2D toolbarBGTexture;
    private Texture2D bodyPanelBGTexture;

    #region DIMENSION_VALUES
    private float padding = 10;
    private float buttonHeight = 40;
    private float smallButtonWidth = 40;
    private float buttonWidth = 100;
    private float topBarHeight = 50;
    private float miniButtonSize = 25;
    private float BodyFirstRowY { get { return topBarHeight + (padding * 2); } }
    private float BodyHeight { get { return (Screen.height - (topBarHeight * 2)); } }
    private float WindowWidth { get { return (Screen.width - (padding * 2)); } }
    private float BodyColumnWidth { get { return (WindowWidth / 2) - (padding / 2); } }
    private float RightColumnX { get { return BodyColumnWidth + (padding * 2); } }
    private float ItemPropertiesHeight { get { return (BodyHeight / 2) - (padding / 2); } }
    private float BodySecondRowY { get { return BodyFirstRowY + ItemPropertiesHeight + padding; } }
    #endregion DIMENSIONS_VALUES

    private PanelState previousPanelState = PanelState.CRAFTABLE;
    private PanelState currentNewPanelState = PanelState.CRAFTABLE;
    private PanelState loadedDatabase = PanelState.CRAFTABLE;
    private CraftableDataObject craftableAsset;
    private RecipeComponentDataObject recipeComponentAsset;
    private RecipeDataObject recipeAsset;
    private SerializedObject craftableAssetSerializedObject;
    //private SerializedObject recipeAssetSerializedObject;
    private Vector2 recipeInputScrollPosition;
    private ReorderableList recipeInputReorderableList;

    //private List<string> recipeNames;
    private List<string> recipeComponentNames;

    #region NEW_RECIPE
    private bool promptToAssignNewRecipe = false;
    private string newRecipeName = "";
    private string newRecipeID = "";
    private string newRecipeUIIcon = "";
    private string newRecipeOutputCraftableID = "";
    private List<RecipeInput> newRecipeInputComponents;
    private Vector2 newRecipeScrollPosition;
    #endregion NEW_RECIPE

    #region NEW_RECIPE_COMPONENT
    private string newComponentName = "";
    private string newComponentID = "";
    private string newComponentUIIcon = "";
    private float newComponentRarity;
    #endregion NEW_RECIPE_COMPONENT

    private int databaseSize = 0;
    private string craftableSearchText = "";
    private string defaultCraftableObjectName = "CraftableDataObject";
    private string editLabel = "";
    private bool errorInFilenames = false;
    private bool promptForConfirmDeleteCraftable = false;

    #region POPUP_WINDOWS
    private bool editingCraftableDatabase = true;
    private bool editingRecipeComponentDatabase = false;
    private bool editingRecipeDatabase = false;
    public Rect windowRect = new Rect(100, 100, 300, 400);
    #endregion POPUP_WINDOWS

    #region TOP_BAR
    private List<CraftableDataObject> craftablesList;
    private List<RecipeDataObject> recipesList;
    private List<RecipeComponentDataObject> recipeComponentsList;
    private int currentListIndex = 0;
    #endregion TOP_BAR

    #region CRAFTABLE
    private bool recipeInputListExpanded = false;
    #endregion CRAFTABLE

    // ============= METHODS ============= //

    #region EDITOR_MENU_METHODS
    [MenuItem("Window/Close Window %w")]
    static void CloseWindow() {
        if (focusedWindow != null) {
            if ((focusedWindow.GetType() == typeof(DesignerToolbox)) || (focusedWindow.GetType() == typeof(CraftableManagerEditorWindow)) || (focusedWindow.GetType() == typeof(DialogManagerWindow)) || (focusedWindow.GetType() == typeof(DataObjectEditor))) {
                focusedWindow.Close();
            }
        }
    }

    [MenuItem("Window/Craftable Manager %&c")]
    static void Init() {
        // Get existing open window or if none, make a new one:
        CraftableManagerEditorWindow window = (CraftableManagerEditorWindow)EditorWindow.GetWindow(typeof(CraftableManagerEditorWindow));
        window.Show();
    }
    #endregion EDITOR_MENU_METHODS

    #region INIT_METHODS
    void Awake() {
        this.titleContent = new GUIContent("Craftable Manager");
        this.minSize = new Vector2(1136, 640);
    }

    void OnEnable() {
        instance = this;

        InitTextures();
        OnLoadedDatabaseChanged();
    }

    private void InitTextures() {
        windowBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        windowBGTexture.SetPixel(0, 0, new Color(0.172549f, 0.2784314f, 0.4392157f, 1f));
        windowBGTexture.Apply();

        toolbarBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        toolbarBGTexture.SetPixel(0, 0, new Color(0.07843138f, 0.1843137f, 0.3294118f, 1f));
        toolbarBGTexture.Apply();

        bodyPanelBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        bodyPanelBGTexture.SetPixel(0, 0, new Color(0.4588235f, 0.5411765f, 0.6588235f, 1f));
        bodyPanelBGTexture.Apply();
    }

    private void CheckForFilenameErrors() {
        errorInFilenames = false;
        for (int i = 0; i < craftablesList.Count; i++) {
            if (craftablesList[i].name == defaultCraftableObjectName) {
                errorInFilenames = true;
                break;
            }
        }
    }

    //private void GetRecipeNames() {
    //    recipeNames = new List<string>();

    //    for (int i = 0; i < recipesList.Count; i++) {
    //        recipeNames.Add(recipesList[i].itemName);
    //    }
    //}

    private void GetRecipeComponentNames() {
        recipeComponentNames = new List<string>();

        for (int i = 0; i < recipeComponentsList.Count; i++) {
            recipeComponentNames.Add(recipeComponentsList[i].itemName);
        }

        recipeComponentNames.Sort(delegate(string nameOne, string nameTwo) { return nameOne.CompareTo(nameTwo); });
    }

    private void UpdateSelectedAsset() {
        GUI.FocusControl("");
        SetCurrentListCount();
        promptForConfirmDeleteCraftable = false;

        switch (loadedDatabase) {
            case PanelState.CRAFTABLE:
                SetNewPanelState(PanelState.CRAFTABLE);
                craftableAsset = craftablesList[currentListIndex];
                craftableAssetSerializedObject = new SerializedObject(craftableAsset);
                break;
            case PanelState.RECIPE:
                recipeAsset = recipesList[currentListIndex];
                break;
            case PanelState.COMPONENT:
                recipeComponentAsset = recipeComponentsList[currentListIndex];
                break;
        }
        this.Repaint();
    }

    //private void SetupReorderableRecipeInputList() {
    //    if (recipeAssetSerializedObject != null) {
    //        recipeInputReorderableList = new ReorderableList(recipeAssetSerializedObject, recipeAssetSerializedObject.FindProperty("inputComponents"), true, true, true, true);
    //        recipeInputReorderableList.elementHeight = EditorGUIUtility.singleLineHeight * 3f;

    //        recipeInputReorderableList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
    //            SerializedProperty element = recipeInputReorderableList.serializedProperty.GetArrayElementAtIndex(index);

    //            rect.y += 2;

    //            EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width / 2, EditorGUIUtility.singleLineHeight), "Amount");
    //            EditorGUI.PropertyField(new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight, rect.width / 2, EditorGUIUtility.singleLineHeight),
    //                element.FindPropertyRelative("amount"), GUIContent.none);

    //            EditorGUI.LabelField(new Rect(rect.x + rect.width / 2, rect.y, rect.width / 2, EditorGUIUtility.singleLineHeight), "Recipe ID");
    //            EditorGUI.PropertyField(new Rect(rect.x + rect.width / 2, rect.y + EditorGUIUtility.singleLineHeight, rect.width / 2, EditorGUIUtility.singleLineHeight),
    //                element.FindPropertyRelative("recipeID"), GUIContent.none);
    //        };

    //        recipeInputReorderableList.drawHeaderCallback = (Rect rect) => {
    //            EditorGUI.LabelField(rect, "Recipe Input List");
    //        };
    //    }
    //}

    private void LoadDataObjects() {
        CraftableDataObject[] craftableDataArray = Resources.LoadAll<CraftableDataObject>("Data/Crafting/Craftables");
        craftablesList = new List<CraftableDataObject>(craftableDataArray);
        RecipeDataObject[] recipeDataArray = Resources.LoadAll<RecipeDataObject>("Data/Crafting/Recipes");
        recipesList = new List<RecipeDataObject>(recipeDataArray);
        RecipeComponentDataObject[] recipeComponentDataArray = Resources.LoadAll<RecipeComponentDataObject>("Data/Crafting/RecipeComponents");
        recipeComponentsList = new List<RecipeComponentDataObject>(recipeComponentDataArray);
    }

    private void LoadCraftableDatabase() {
        CheckForFilenameErrors();
        //GetRecipeNames();
        GetRecipeComponentNames();
        UpdateSelectedAsset();
    }

    private void LoadComponentDatabase() {
        UpdateSelectedAsset();
    }

    private void LoadRecipeDatabase() {
        UpdateSelectedAsset();
    }

    private void OnLoadedDatabaseChanged() {
        LoadDataObjects();
        currentListIndex = 0;

        switch (loadedDatabase) {
            case PanelState.CRAFTABLE:
                LoadCraftableDatabase();
                break;
            case PanelState.RECIPE:
                LoadRecipeDatabase();
                break;
            case PanelState.COMPONENT:
                LoadComponentDatabase();
                break;
        }
    }
    #endregion INIT_METHODS

    #region UTILITIES
    private int GetIndexOfRecipeComponent(string id) {
        int rVal = 0;
        for (int i = 0; i < recipeComponentsList.Count; i++) {
            if (recipeComponentsList[i].id == id) {
                rVal = i;
                break;
            }
        }
        return rVal;
    }

    private void SearchForCraftable() {
        if (!string.IsNullOrEmpty(craftableSearchText)) {
            int indexToSelect = currentListIndex;
            List<CraftableDataObject> shortListOfHits = new List<CraftableDataObject>();
            string objName = "";
            string objID = "";
            string lowercaseSearchText = craftableSearchText.ToLower();

            for (int i = 0; i < craftablesList.Count; i++) {
                objID = craftablesList[i].id.ToLower();
                objName = craftablesList[i].itemName.ToLower();
                if (!string.IsNullOrEmpty(objID)) {
                    for (int j = 0; j < objID.Length; j++) {
                        if (objID[j] == lowercaseSearchText[0]) {
                            shortListOfHits.Add(craftablesList[i]);
                            break;
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(objName)) {
                    for (int j = 0; j < objName.Length; j++) {
                        if (objName[j] == lowercaseSearchText[0]) {
                            shortListOfHits.Add(craftablesList[i]);
                            break;
                        }
                    }
                }
            }

            int charsMatched = 0;
            int charCounter = 0;
            int searchCharIter = 0;
            int nameSearchStartIndex = 0;
            for (int i = 0; i < shortListOfHits.Count; i++) {
                charCounter = 0;
                searchCharIter = 0;
                objName = shortListOfHits[i].itemName.ToLower();
                objID = shortListOfHits[i].id.ToLower();
                for (int j = 0; j < objName.Length; j++) {
                    if (objName[j] == lowercaseSearchText[0]) {
                        nameSearchStartIndex = j;
                        break;
                    }
                }

                for (int j = nameSearchStartIndex; j < objName.Length; j++) {
                    if (objName[j] == lowercaseSearchText[searchCharIter]) {
                        charCounter++;
                        searchCharIter++;
                        if (searchCharIter >= lowercaseSearchText.Length) {
                            break;
                        }
                    }
                }

                if (charCounter > charsMatched) {
                    charsMatched = charCounter;
                    indexToSelect = i;
                }
            }

            if (currentListIndex != indexToSelect) {
                currentListIndex = GetIndexOfCraftable(shortListOfHits[indexToSelect].id);
                UpdateSelectedAsset();
            }
        }
    }

    private int GetIndexOfCraftable(string id) {
        int rVal = currentListIndex;
        for (int i = 0; i < craftablesList.Count; i++) {
            if (craftablesList[i].id == id) {
                rVal = i;
                break;
            }
        }
        return rVal;
    }

    private void SetNewPanelState(PanelState state) {
        previousPanelState = currentNewPanelState;
        currentNewPanelState = state;
    }

    private void BackToPreviousPanelState() {
        currentNewPanelState = previousPanelState;
        // Defaulting to this works cause there are only three states to cycle through
        previousPanelState = PanelState.CRAFTABLE;
    }

    private void SetCreatingRecipe(bool value) {
        SetNewPanelState((value) ? PanelState.RECIPE : PanelState.CRAFTABLE);
        newRecipeInputComponents = new List<RecipeInput>();
        newRecipeID = (value) ? craftableAsset.id + "_recipe" : "";
        newRecipeUIIcon = (value) ? craftableAsset.id + "_recipe_icon" : "";
        newRecipeName = (value) ? craftableAsset.itemName + " Recipe" : "";
        newRecipeOutputCraftableID = (value) ? craftableAsset.id : "";
        GUI.FocusControl("");
    }

    private void SetCreatingRecipeComponent(bool value) {
        SetNewPanelState((value) ? PanelState.COMPONENT : PanelState.CRAFTABLE);
        newComponentName = "";
        newComponentID = "";
        newComponentUIIcon = "";
        newComponentRarity = 50;
        GUI.FocusControl("");
    }

    private void SetCurrentListCount() {
        switch (loadedDatabase) {
            case PanelState.CRAFTABLE:
                databaseSize = craftablesList.Count;
                break;
            case PanelState.RECIPE:
                databaseSize = recipesList.Count;
                break;
            case PanelState.COMPONENT:
                databaseSize = recipeComponentsList.Count;
                break;
        }
    }

    private void RenameAsset(ScriptableObject asset, string newName) {
        string assetPath = AssetDatabase.GetAssetPath(asset.GetInstanceID());
        string trimmedName = newName.Replace(" ", string.Empty);
        AssetDatabase.RenameAsset(assetPath, trimmedName);
        AssetDatabase.SaveAssets();
    }

    private bool NewRecipeValid() {
        return ((newRecipeInputComponents.Count > 0) && (!string.IsNullOrEmpty(newRecipeID)) && (!string.IsNullOrEmpty(newRecipeUIIcon)) && (!string.IsNullOrEmpty(newRecipeName)) && (!string.IsNullOrEmpty(newRecipeOutputCraftableID)));
    }

    private bool NewRecipeComponentValid() {
        return ((!string.IsNullOrEmpty(newComponentID)) && (!string.IsNullOrEmpty(newComponentName)) && (!string.IsNullOrEmpty(newComponentUIIcon)) && (newComponentRarity != 0));
    }

    private void SetCurrentlyEditing(PanelState state) {
        if (loadedDatabase != state) {
            loadedDatabase = state;

            editingCraftableDatabase = false;
            editingRecipeComponentDatabase = false;
            editingRecipeDatabase = false;

            switch (loadedDatabase) {
                case PanelState.CRAFTABLE:
                    editingCraftableDatabase = true;
                    break;
                case PanelState.RECIPE:
                    editingRecipeDatabase = true;
                    break;
                case PanelState.COMPONENT:
                    editingRecipeComponentDatabase = true;
                    break;
            }

            OnLoadedDatabaseChanged();
        }
    }

    #region HORIZONTAL
    private T DrawHorizontalLabeledObjectField<T>(string label, Object obj) where T : Object {
        T rVal;
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        rVal = (T)EditorGUILayout.ObjectField(obj, obj.GetType());
        GUILayout.EndHorizontal();
        return rVal;
    }

    private T DrawHorizontalLabeledEnumField<T>(string label, Enum enumType) {
        T rVal;
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        rVal = (T)(object)EditorGUILayout.EnumPopup(enumType);
        GUILayout.EndHorizontal();
        return rVal;
    }

    private T DrawHorizontalLabeledEnumField<T>(string label, Enum enumType, GUILayoutOption labelWidth, GUILayoutOption enumWidth) {
        T rVal;
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label, labelWidth);
        rVal = (T)(object)EditorGUILayout.EnumPopup(enumType, enumWidth);
        GUILayout.EndHorizontal();
        return rVal;
    }

    private void DrawHorizontalLabeledTextField(string label, ref string value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.TextField(value);
        GUILayout.EndHorizontal();
    }

    private void DrawHorizontalLabeledTextField(string label, ref string value, GUILayoutOption labelWidth, GUILayoutOption valueWidth) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label, labelWidth);
        value = EditorGUILayout.TextField(value, valueWidth);
        GUILayout.EndHorizontal();
    }

    private void DrawHorizontalLabeledFloatField(string label, ref float value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.FloatField(value);
        GUILayout.EndHorizontal();
    }
    #endregion HORIZONTAL

    #region VERTICAL
    private void DrawVerticalLabeledTextField(string label, ref string value) {
        GUILayout.BeginVertical();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.TextField(value);
        GUILayout.EndVertical();
    }

    private void DrawVerticalLabeledTextField(string label, ref string value, GUILayoutOption labelWidth, GUILayoutOption valueWidth) {
        GUILayout.BeginVertical();
        EditorGUILayout.LabelField(label, labelWidth);
        value = EditorGUILayout.TextField(value, valueWidth);
        GUILayout.EndVertical();
    }

    private void DrawVerticalLabeledFloatField(string label, ref float value) {
        GUILayout.BeginVertical();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.FloatField(value);
        GUILayout.EndVertical();
    }

    private void DrawVerticalLabeledIntField(string label, ref int value) {
        GUILayout.BeginVertical();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.IntField(value);
        GUILayout.EndVertical();
    }

    private void DrawVerticalLabeledIntField(string label, ref int value, GUILayoutOption labelWidth, GUILayoutOption intWidth) {
        GUILayout.BeginVertical();
        EditorGUILayout.LabelField(label, labelWidth);
        value = EditorGUILayout.IntField(value, intWidth);
        GUILayout.EndVertical();
    }

    private T DrawVerticalLabeledEnumField<T>(string label, Enum enumType) {
        T rVal;
        GUILayout.BeginVertical();
        EditorGUILayout.LabelField(label);
        rVal = (T)(object)EditorGUILayout.EnumPopup(enumType);
        GUILayout.EndVertical();
        return rVal;
    }

    private T DrawVerticalLabeledEnumField<T>(string label, Enum enumType, GUILayoutOption labelWidth, GUILayoutOption enumWidth) {
        T rVal;
        GUILayout.BeginVertical();
        EditorGUILayout.LabelField(label, labelWidth);
        rVal = (T)(object)EditorGUILayout.EnumPopup(enumType, enumWidth);
        GUILayout.EndVertical();
        return rVal;
    }
    #endregion VERTICAL

    #endregion UTILITIES

    #region ACTIONS

    private CraftableDataObject CreateCraftable() {
        CraftableDataObject item = ScriptableObject.CreateInstance<CraftableDataObject>();

        AssetDatabase.CreateAsset(item, "Assets/Resources/Data/Crafting/Craftables/" + defaultCraftableObjectName + ".asset");

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        return item;
    }

    private void CreateRecipeComponent() {
        RecipeComponentDataObject item = ScriptableObject.CreateInstance<RecipeComponentDataObject>();

        item.itemName = newComponentName;
        item.id = newComponentID;
        item.uiIcon = newComponentUIIcon;
        item.rarity = new PercentageFloat(newComponentRarity);

        recipeComponentsList.Add(item);

        string trimmedName = newComponentName.Replace(" ", string.Empty);
        AssetDatabase.CreateAsset(item, "Assets/Resources/Data/Crafting/RecipeComponents/" + trimmedName + "_DataObject.asset");

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    private void CreateRecipeDataObject(bool assignToCraftable) {
        RecipeDataObject item = ScriptableObject.CreateInstance<RecipeDataObject>();
        item.itemName = newRecipeName;
        item.id = newRecipeID;
        item.uiIcon = newRecipeUIIcon;
        item.outputCraftableID = newRecipeOutputCraftableID;
        item.inputComponents = newRecipeInputComponents;

        string trimmedName = newRecipeName.Replace(" ", string.Empty);
        AssetDatabase.CreateAsset(item, "Assets/Resources/Data/Crafting/Recipes/" + trimmedName + "_DataObject.asset");

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        if ((craftableAsset != null) && (assignToCraftable)) {
            craftableAsset.recipeDataObject = item;
        }
        SetNewPanelState(PanelState.CRAFTABLE);
    }

    private void AddCraftable() {
        CraftableDataObject craftableDataObject = CreateCraftable();

        craftablesList.Add(craftableDataObject);
        currentListIndex = craftablesList.Count - 1;
        UpdateSelectedAsset();

        EditorGUI.FocusTextInControl("NewCraftableNameTextField");
    }

    private void AddRecipeComponent() {
        CreateRecipeComponent();
        GetRecipeComponentNames();
    }
    #endregion ACTIONS

    #region GUI_DRAWING

    #region TOP_BAR
    private void DrawTopToolbar() {
        GUILayout.BeginArea(new Rect(padding, padding, WindowWidth, topBarHeight), GUI.skin.box);
        GUI.DrawTexture(new Rect(2, 2, WindowWidth - 4, topBarHeight - 4), toolbarBGTexture, ScaleMode.StretchToFill);
        //GUILayout.Space(padding / 2);
        GUILayout.BeginHorizontal();

        DrawCraftablesListControls();

        GUILayout.FlexibleSpace();

        // EDIT DATABASE BUTTONS
        GUILayout.BeginHorizontal();
        //GUI.enabled = !editingCraftableDatabase;
        GUI.color = (editingCraftableDatabase) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
        if (GUILayout.Button("Craftables", GUILayout.Width(buttonWidth), GUILayout.Height(buttonHeight))) {
            SetCurrentlyEditing(PanelState.CRAFTABLE);
        }

        //GUI.enabled = !editingRecipeComponentDatabase;
        GUI.color = (editingRecipeComponentDatabase) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
        if (GUILayout.Button("Recipe Components", GUILayout.Width(buttonWidth), GUILayout.Height(buttonHeight))) {
            SetCurrentlyEditing(PanelState.COMPONENT);
        }

        //GUI.enabled = !editingRecipeDatabase;
        GUI.color = (editingRecipeDatabase) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
        if (GUILayout.Button("Recipes", GUILayout.Width(buttonWidth), GUILayout.Height(buttonHeight))) {
            SetCurrentlyEditing(PanelState.RECIPE);
        }
        GUI.enabled = true;
        GUILayout.EndHorizontal();

        GUILayout.FlexibleSpace();

        CheckForFilenameErrors();
        GUI.enabled = (!errorInFilenames);
        GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
        if (GUILayout.Button("New\nCraftable", GUILayout.Width(buttonWidth), GUILayout.Height(buttonHeight))) {
            AddCraftable();
        }
        GUI.enabled = true;

        GUI.color = Color.yellow;
        if (GUILayout.Button("New\nComponent", GUILayout.Width(buttonWidth), GUILayout.Height(buttonHeight))) {
            SetCreatingRecipeComponent(true);
        }
        GUI.color = Color.white;

        GUI.enabled = (craftableAsset != null);
        if (GUILayout.Button("Select\nAsset", GUILayout.Width(buttonWidth), GUILayout.Height(buttonHeight))) {
            Selection.activeObject = craftableAsset;
        }
        GUI.enabled = true;

        //if (GUILayout.Button("Edit Script", GUILayout.Width(40))) {
        //    //System.Diagnostics.Process.Start("devenv.exe", Application.dataPath + "/Editor/Crafting/CraftableManagerEditorWindow.cs");
        //    Debug.Log(Utility.IDToName("woodenStaff"));
        //}

        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    private void DrawCraftablesListControls() {
        GUILayout.BeginVertical();
        GUILayout.Space(4);
        GUI.SetNextControlName(SEARCH_BAR_CONTROL_NAME);
        craftableSearchText = GUILayout.TextField(craftableSearchText, GUILayout.Width((smallButtonWidth * 3) + (8)));
        GUILayout.Space(1);

        GUILayout.BeginHorizontal();
        // Prev Button
        GUI.enabled = (currentListIndex > 0);
        if (GUILayout.Button("<<", GUILayout.Width(smallButtonWidth))) {
            currentListIndex--;
            UpdateSelectedAsset();
        }
        GUI.enabled = true;

        DrawListCycleControls();

        // Next Button
        GUI.enabled = (currentListIndex < (databaseSize - 1));
        if (GUILayout.Button(">>", GUILayout.Width(smallButtonWidth))) {
            currentListIndex++;
            UpdateSelectedAsset();
        }
        GUI.enabled = true;
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();

        GUILayout.BeginVertical();
        GUI.color = (promptForConfirmDeleteCraftable) ? Color.green : Color.white;
        if (GUILayout.Button((promptForConfirmDeleteCraftable) ? "Confirm" : "Search", GUILayout.Width(buttonWidth))) {
            if (promptForConfirmDeleteCraftable) {
                DeleteSelectedObject();
            }
            else {
                SearchForCraftable();
            }
        }


        GUI.color = Color.red;
        if (GUILayout.Button((promptForConfirmDeleteCraftable) ? "Cancel" : "Delete", GUILayout.Width(buttonWidth))) {
            promptForConfirmDeleteCraftable = !promptForConfirmDeleteCraftable;
        }
        GUI.color = Color.white;

        GUILayout.EndVertical();
    }

    private void DeleteSelectedObject() {
        string assetPath = "";
        switch (loadedDatabase) {
            case PanelState.CRAFTABLE:
                assetPath = AssetDatabase.GetAssetPath(craftableAsset.GetInstanceID());
                craftablesList.RemoveAt(currentListIndex);
                currentListIndex = Mathf.Clamp(currentListIndex--, 0, craftablesList.Count - 1);
                break;
            case PanelState.RECIPE:
                assetPath = AssetDatabase.GetAssetPath(recipeAsset.GetInstanceID());
                recipesList.RemoveAt(currentListIndex);
                currentListIndex = Mathf.Clamp(currentListIndex--, 0, recipesList.Count - 1);
                break;
            case PanelState.COMPONENT:
                assetPath = AssetDatabase.GetAssetPath(recipeComponentAsset.GetInstanceID());
                recipeComponentsList.RemoveAt(currentListIndex);
                currentListIndex = Mathf.Clamp(currentListIndex--, 0, recipeComponentsList.Count - 1);
                break;
        }

        AssetDatabase.DeleteAsset(assetPath);

        UpdateSelectedAsset();
    }

    private void DrawListCycleControls() {
        GUI.skin.label.alignment = TextAnchor.MiddleCenter;

        GUI.skin.label.normal.textColor = Color.white;
        GUILayout.Label((currentListIndex + 1) + "/" + databaseSize, GUILayout.Width(smallButtonWidth));
        GUI.skin.label.normal.textColor = Color.black;

        //switch (loadedDatabase) {
        //    case PanelState.CRAFTABLE:
        //        GUILayout.Label((currentListIndex + 1) + "/" + craftablesList.Count, GUILayout.Width(smallButtonWidth));
        //        break;
        //    case PanelState.RECIPE:
        //        GUILayout.Label((currentListIndex + 1) + "/" + craftablesList.Count, GUILayout.Width(smallButtonWidth));
        //        break;
        //    case PanelState.COMPONENT:
        //        GUILayout.Label((currentListIndex + 1) + "/" + recipeComponentsList.Count, GUILayout.Width(smallButtonWidth));
        //        break;
        //}
        GUI.skin.label.alignment = TextAnchor.MiddleLeft;
    }

    #endregion TOP_BAR

    private void DrawItemProperties() {
        if (craftablesList[currentListIndex] != null) {
            GUI.DrawTexture(new Rect(2, 2, BodyColumnWidth - 4, ItemPropertiesHeight - 4), bodyPanelBGTexture, ScaleMode.StretchToFill);
            GUILayout.Label("Item Properties", EditorStyles.boldLabel);

            GUILayout.BeginVertical();

            //GUILayout.BeginHorizontal();
            //GUI.SetNextControlName("NewCraftableNameTextField");
            //EditorGUILayout.LabelField("Name");
            //craftableAsset.itemName = EditorGUILayout.TextField(craftableAsset.itemName);
            //GUILayout.EndHorizontal();

            GUI.SetNextControlName("NewCraftableNameTextField");
            DrawHorizontalLabeledTextField("Name", ref craftableAsset.itemName);

            //GUILayout.BeginHorizontal();
            //EditorGUILayout.LabelField("ID");
            //craftableAsset.id = EditorGUILayout.TextField(craftableAsset.id);
            //GUILayout.EndHorizontal();
            DrawHorizontalLabeledTextField("ID", ref craftableAsset.id);

            //GUILayout.BeginHorizontal();
            //EditorGUILayout.LabelField("UI Icon");
            //craftableAsset.uiIcon = EditorGUILayout.TextField(craftableAsset.uiIcon);
            //GUILayout.EndHorizontal();
            DrawHorizontalLabeledTextField("UI Icon", ref craftableAsset.uiIcon);

            //GUILayout.BeginHorizontal();
            //EditorGUILayout.LabelField("Recipe ID");
            //craftableAsset.recipeDataObject = (RecipeDataObject)EditorGUILayout.ObjectField(craftableAsset.recipeDataObject, typeof(RecipeDataObject));
            //GUILayout.EndHorizontal();
            craftableAsset.recipeDataObject = DrawHorizontalLabeledObjectField<RecipeDataObject>("Recipe ID", craftableAsset.recipeDataObject);

            if ((currentNewPanelState != PanelState.RECIPE) && (craftableAsset.recipeDataObject == null)) {
                if (GUILayout.Button("New Recipe")) {
                    SetCreatingRecipe(true);
                }
            }

            GUILayout.FlexibleSpace();
            if (craftableAsset.name == defaultCraftableObjectName) {
                GUILayout.BeginVertical();
                GUIStyle style = new GUIStyle();
                style.normal.textColor = Color.red;
                style.alignment = TextAnchor.MiddleCenter;
                GUILayout.Label("ERROR IN ASSET FILENAME: " + craftableAsset.name, style);
                GUI.color = Color.yellow;
                if (GUILayout.Button("Fix Now")) {
                    if (!string.IsNullOrEmpty(craftableAsset.itemName)) {
                        RenameAsset(craftableAsset, craftableAsset.itemName + "_DataObject");
                    }
                }
                GUI.color = Color.white;
                GUILayout.EndVertical();
                GUILayout.Space(5);
            }

            GUILayout.EndVertical();
            //GUILayout.EndArea();
        }
    }

    private void DrawRecipeInputs() {
        GUI.DrawTexture(new Rect(2, 2, BodyColumnWidth - 4, ItemPropertiesHeight - 4), bodyPanelBGTexture, ScaleMode.StretchToFill);
        GUILayout.BeginHorizontal();
        GUILayout.Label("Recipe", EditorStyles.boldLabel);

        GUILayout.FlexibleSpace();
        //if (craftableAsset.recipeDataObject != null) {
        //    editLabel = (recipeInputListEditing) ? "Back" : "Edit";
        //    if (GUILayout.Button(editLabel, GUILayout.Width(buttonWidth))) {
        //        recipeInputListEditing = !recipeInputListEditing;
        //    }
        //}

        if ((craftableAsset.recipeDataObject != null) && (craftableAsset.recipeDataObject.inputComponents.Count == 0)) {
            GUI.color = Color.green;
            if (GUILayout.Button("+", GUILayout.Width(miniButtonSize))) {
                craftableAsset.recipeDataObject.inputComponents.Add(new RecipeInput());
            }
            GUI.color = Color.white;
        }

        GUILayout.EndHorizontal();

        recipeInputScrollPosition = GUILayout.BeginScrollView(recipeInputScrollPosition);

        DrawRecipeInputList();

        GUILayout.EndScrollView();
    }

    private void DrawNewRecipePanel() {
        GUILayout.Space((padding / 2));

        GUILayout.BeginHorizontal();
        GUILayout.Label("New Recipe", EditorStyles.boldLabel);
        if (promptToAssignNewRecipe) {
            GUILayout.FlexibleSpace();
            GUILayout.Label("Assign To Selected Craftable?", EditorStyles.boldLabel);
        }
        GUILayout.EndHorizontal();

        if (promptToAssignNewRecipe) {
            GUI.color = Color.red;
            if (GUILayout.Button("No")) {
                promptToAssignNewRecipe = false;
                CreateRecipeDataObject(false);
            }

            GUI.color = Color.green;
            if (GUILayout.Button("Yes")) {
                promptToAssignNewRecipe = false;
                CreateRecipeDataObject(true);
            }
            GUI.color = Color.white;
        }
        else {
            if (GUILayout.Button("<< Back")) {
                //SetCreatingRecipe(false);
                BackToPreviousPanelState();
                return;
            }

            GUI.color = Color.green;
            if (GUILayout.Button("Create")) {
                if (NewRecipeValid()) {
                    promptToAssignNewRecipe = true;
                }
            }
            GUI.color = Color.white;
        }

        newRecipeScrollPosition = GUILayout.BeginScrollView(newRecipeScrollPosition);
        DrawHorizontalLabeledTextField("Recipe Name", ref newRecipeName);
        DrawHorizontalLabeledTextField("Recipe ID", ref newRecipeID);
        DrawHorizontalLabeledTextField("UI Icon", ref newRecipeUIIcon);
        DrawHorizontalLabeledTextField("Output Craftable ID", ref newRecipeOutputCraftableID);

        GUILayout.Space((padding / 2));

        GUILayout.Label("Recipe Components", EditorStyles.boldLabel);
        if (newRecipeInputComponents.Count == 0) {
            GUI.color = Color.green;
            if (GUILayout.Button("+", GUILayout.Width(miniButtonSize))) {
                newRecipeInputComponents.Add(new RecipeInput());
            }
            GUI.color = Color.white;
        }

        RecipeInput recipeInput;
        for (int i = 0; i < newRecipeInputComponents.Count; i++) {
            recipeInput = newRecipeInputComponents[i];
            GUILayout.BeginHorizontal();
            DrawVerticalLabeledIntField("Amount", ref recipeInput.amount);

            GUILayout.BeginVertical();
            EditorGUILayout.LabelField("Recipe Component");
            recipeInput.recipeID = recipeComponentsList[EditorGUILayout.Popup(GetIndexOfRecipeComponent(recipeInput.recipeID), recipeComponentNames.ToArray())].id;
            GUILayout.EndVertical();

            GUI.color = Color.red;
            if (GUILayout.Button("-", GUILayout.Width(miniButtonSize))) {
                newRecipeInputComponents.RemoveAt(i);
            }
            GUI.color = Color.white;

            GUI.color = Color.green;
            if (GUILayout.Button("+", GUILayout.Width(miniButtonSize))) {
                newRecipeInputComponents.Insert(i + 1, new RecipeInput());
            }
            GUI.color = Color.white;

            GUILayout.EndHorizontal();
        }
        GUILayout.EndScrollView();
    }

    private void DrawNewComponentPanel() {
        GUILayout.Space((padding / 2));

        GUILayout.Label("New Recipe Component", EditorStyles.boldLabel);

        if (GUILayout.Button("<< Back")) {
            BackToPreviousPanelState();
            return;
        }
        GUI.color = Color.green;
        if (GUILayout.Button("Create")) {
            if (NewRecipeComponentValid()) {
                AddRecipeComponent();
                BackToPreviousPanelState();
            }
        }
        GUI.color = Color.white;

        DrawHorizontalLabeledTextField("Recipe Component Name", ref newComponentName);
        DrawHorizontalLabeledTextField("Recipe Component ID", ref newComponentID);
        DrawHorizontalLabeledTextField("UI Icon", ref newComponentUIIcon);

        newComponentRarity = EditorGUILayout.Slider("Rarity", newComponentRarity, 0, 100);
    }

    private void DrawRecipeInputList() {
        GUILayout.BeginVertical();

        EditorGUILayout.LabelField("Recipe Components");
        if (craftableAsset.recipeDataObject != null) {
            List<RecipeInput> recipeInputs = craftableAsset.recipeDataObject.inputComponents;
            RecipeInput recipeInput;
            for (int i = 0; i < recipeInputs.Count; i++) {
                GUILayout.BeginHorizontal();

                recipeInput = recipeInputs[i];

                DrawVerticalLabeledIntField("Amount", ref recipeInput.amount);

                GUILayout.BeginVertical();
                EditorGUILayout.LabelField("Recipe Component");
                recipeInput.recipeID = recipeComponentsList[EditorGUILayout.Popup(GetIndexOfRecipeComponent(recipeInput.recipeID), recipeComponentNames.ToArray())].id;
                GUILayout.EndVertical();

                GUI.color = Color.red;
                if (GUILayout.Button("-", GUILayout.Width(miniButtonSize))) {
                    recipeInputs.RemoveAt(i);
                }
                GUI.color = Color.white;

                GUI.color = Color.green;
                if (GUILayout.Button("+", GUILayout.Width(miniButtonSize))) {
                    recipeInputs.Insert(i + 1, new RecipeInput());
                }
                GUI.color = Color.white;

                GUILayout.EndHorizontal();
                GUILayout.Space((padding / 2));
            }
        }
        GUILayout.EndVertical();
    }

    private void DrawItemStats() {

        GUILayout.BeginVertical();
        GUI.DrawTexture(new Rect(2, 2, BodyColumnWidth - 4, BodyHeight - 4), bodyPanelBGTexture, ScaleMode.StretchToFill);
        GUI.DrawTexture(new Rect(BodyColumnWidth - 106, BodyHeight - 106, 96, 96), gameItemIcon);

        GUILayout.Label("Item Stats", EditorStyles.boldLabel);
        GUILayout.Space(padding);

        //GUILayout.BeginHorizontal();
        //EditorGUILayout.LabelField("Item Type");
        //craftableAsset.itemType = (ItemType)EditorGUILayout.EnumPopup(craftableAsset.itemType);
        //GUILayout.EndHorizontal();

        craftableAsset.itemType = DrawHorizontalLabeledEnumField<ItemType>("Item Type", craftableAsset.itemType);

        // Armor related section
        if (craftableAsset.itemType == ItemType.ARMOR_SET) {           
            craftableAsset.attackDataObject = DrawHorizontalLabeledObjectField<AttackDataObject>("Attack Skill", craftableAsset.attackDataObject);
            craftableAsset.classRequirement = DrawHorizontalLabeledEnumField<CharacterClass>("Class Requirement", craftableAsset.classRequirement);

            EditorGUILayout.LabelField("Offensive", EditorStyles.boldLabel);
            DrawHorizontalLabeledFloatField("Physical Defense", ref craftableAsset.physicalDefense);
            DrawHorizontalLabeledFloatField("Magic Defense", ref craftableAsset.magicDefense);

            EditorGUILayout.LabelField("Defensive", EditorStyles.boldLabel);
            DrawHorizontalLabeledFloatField("Physical Attack", ref craftableAsset.physicalAttack);
            DrawHorizontalLabeledFloatField("Magic Defense", ref craftableAsset.magicAttack);
            DrawHorizontalLabeledFloatField("Crit Damage", ref craftableAsset.critDamage);

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Focus");
            craftableAsset.focus = EditorGUILayout.Slider(craftableAsset.focus, 0, 100, GUILayout.Width((BodyColumnWidth / 2) - (padding / 2)));
            GUILayout.EndHorizontal();
        }

        GUILayout.EndVertical();
    }

    private void DrawCraftableProperties() {
        GUILayout.BeginArea(new Rect(padding, BodyFirstRowY, BodyColumnWidth, ItemPropertiesHeight), GUI.skin.box);
        DrawItemProperties();
        GUILayout.EndArea();

        GUILayout.BeginArea(new Rect(padding, BodySecondRowY, BodyColumnWidth, ItemPropertiesHeight), GUI.skin.box);
        if (currentNewPanelState == PanelState.RECIPE) {
            DrawNewRecipePanel();
        }
        else if (currentNewPanelState == PanelState.COMPONENT) {
            DrawNewComponentPanel();
        }
        else {
            DrawRecipeInputs();
        }
        GUILayout.EndArea();

        GUILayout.BeginArea(new Rect(RightColumnX, BodyFirstRowY, BodyColumnWidth, BodyHeight), GUI.skin.box);
        DrawItemStats();
        GUILayout.EndArea();


        if (Event.current.isKey && Event.current.type == EventType.KeyUp && Event.current.keyCode == KeyCode.Return) {
            if (GUI.GetNameOfFocusedControl() == SEARCH_BAR_CONTROL_NAME) {
                SearchForCraftable();
            }
        }
    }

    private void DrawRecipeComponentProperties() {
        GUI.DrawTexture(new Rect(2, 2, WindowWidth - 4, BodyHeight - 4), bodyPanelBGTexture, ScaleMode.StretchToFill);
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        GUILayout.BeginVertical();
        GUILayout.FlexibleSpace();

        //GUILayout.BeginVertical();
        //EditorGUILayout.LabelField("Recipe Component Name", GUILayout.Width((BodyColumnWidth - (padding * 2))));
        //recipeComponentAsset.itemName = EditorGUILayout.TextField(recipeComponentAsset.itemName, GUILayout.Width((BodyColumnWidth - (padding * 2))));
        //GUILayout.EndVertical();
        DrawVerticalLabeledTextField("Recipe Component Name", ref recipeComponentAsset.itemName, GUILayout.Width((BodyColumnWidth - (padding * 2))), GUILayout.Width((BodyColumnWidth - (padding * 2))));

        GUILayout.Space(padding);

        //GUILayout.BeginVertical();
        //EditorGUILayout.LabelField("Recipe Component ID", GUILayout.Width((BodyColumnWidth - (padding * 2))));
        //recipeComponentAsset.id = EditorGUILayout.TextField(recipeComponentAsset.id, GUILayout.Width((BodyColumnWidth - (padding * 2))));
        //GUILayout.EndVertical();
        DrawVerticalLabeledTextField("Recipe Component ID", ref recipeComponentAsset.id, GUILayout.Width((BodyColumnWidth - (padding * 2))), GUILayout.Width((BodyColumnWidth - (padding * 2))));

        GUILayout.Space(padding);

        //GUILayout.BeginVertical();
        //EditorGUILayout.LabelField("UI Icon", GUILayout.Width((BodyColumnWidth - (padding * 2))));
        //recipeComponentAsset.uiIcon = EditorGUILayout.TextField(recipeComponentAsset.uiIcon, GUILayout.Width((BodyColumnWidth - (padding * 2))));
        //GUILayout.EndVertical();
        DrawVerticalLabeledTextField("UI Icon", ref recipeComponentAsset.uiIcon, GUILayout.Width((BodyColumnWidth - (padding * 2))), GUILayout.Width((BodyColumnWidth - (padding * 2))));

        GUILayout.Space(padding);

        //GUILayout.BeginVertical();
        //EditorGUILayout.LabelField("Item Type", GUILayout.Width((BodyColumnWidth - (padding * 2))));
        //recipeComponentAsset.itemType = (ItemType)EditorGUILayout.EnumPopup(recipeComponentAsset.itemType, GUILayout.Width((BodyColumnWidth - (padding * 2))));
        //GUILayout.EndVertical();

        recipeComponentAsset.itemType = DrawVerticalLabeledEnumField<ItemType>("Item Type", recipeComponentAsset.itemType, GUILayout.Width((BodyColumnWidth - (padding * 2))), GUILayout.Width((BodyColumnWidth - (padding * 2))));

        GUILayout.Space(padding);
        EditorGUILayout.LabelField("Rarity", GUILayout.Width((BodyColumnWidth - (padding * 2))));
        recipeComponentAsset.rarity.baseValue = EditorGUILayout.Slider(recipeComponentAsset.rarity.baseValue, 0, 100, GUILayout.Width((BodyColumnWidth - (padding * 2))));

        GUILayout.FlexibleSpace();
        GUILayout.EndVertical();

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label(recipeComponentsIcon, GUILayout.MaxWidth(96), GUILayout.MaxHeight(96));
        GUILayout.EndHorizontal();
    }

    private void DrawRecipeProperties() {
        GUI.DrawTexture(new Rect(2, 2, WindowWidth - 4, BodyHeight - 4), bodyPanelBGTexture, ScaleMode.StretchToFill);
        GUILayout.Space(96);

        newRecipeScrollPosition = GUILayout.BeginScrollView(newRecipeScrollPosition);

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        GUILayout.BeginVertical();
        GUILayout.FlexibleSpace();

        //GUILayout.BeginHorizontal();
        //EditorGUILayout.LabelField("Recipe Name", GUILayout.Width((BodyColumnWidth / 2)));
        //recipeAsset.itemName = EditorGUILayout.TextField(recipeAsset.itemName, GUILayout.Width((BodyColumnWidth / 2)));
        //GUILayout.EndHorizontal();
        DrawHorizontalLabeledTextField("Recipe Name", ref recipeAsset.itemName, GUILayout.Width((BodyColumnWidth / 2)), GUILayout.Width((BodyColumnWidth / 2)));

        //GUILayout.BeginHorizontal();
        //EditorGUILayout.LabelField("Recipe ID", GUILayout.Width((BodyColumnWidth / 2)));
        //recipeAsset.id = EditorGUILayout.TextField(recipeAsset.id, GUILayout.Width((BodyColumnWidth / 2)));
        //GUILayout.EndHorizontal();
        DrawHorizontalLabeledTextField("Recipe ID", ref recipeAsset.id, GUILayout.Width((BodyColumnWidth / 2)), GUILayout.Width((BodyColumnWidth / 2)));

        //GUILayout.BeginHorizontal();
        //EditorGUILayout.LabelField("UI Icon", GUILayout.Width((BodyColumnWidth / 2)));
        //recipeAsset.uiIcon = EditorGUILayout.TextField(recipeAsset.uiIcon, GUILayout.Width((BodyColumnWidth / 2)));
        //GUILayout.EndHorizontal();
        DrawHorizontalLabeledTextField("UI Icon", ref recipeAsset.uiIcon, GUILayout.Width((BodyColumnWidth / 2)), GUILayout.Width((BodyColumnWidth / 2)));

        //GUILayout.BeginHorizontal();
        //EditorGUILayout.LabelField("Ouput Craftable ID", GUILayout.Width((BodyColumnWidth / 2)));
        //recipeAsset.outputCraftableID = EditorGUILayout.TextField(recipeAsset.outputCraftableID, GUILayout.Width((BodyColumnWidth / 2)));
        //GUILayout.EndHorizontal();
        DrawHorizontalLabeledTextField("Output Craftable ID", ref recipeAsset.outputCraftableID, GUILayout.Width((BodyColumnWidth / 2)), GUILayout.Width((BodyColumnWidth / 2)));

        //GUILayout.BeginHorizontal();
        //EditorGUILayout.LabelField("Item Type", GUILayout.Width((BodyColumnWidth / 2)));
        //recipeAsset.itemType = (ItemType)EditorGUILayout.EnumPopup(recipeAsset.itemType, GUILayout.Width((BodyColumnWidth / 2)));
        //GUILayout.EndHorizontal();

        recipeAsset.itemType = DrawHorizontalLabeledEnumField<ItemType>("Item Type", recipeAsset.itemType, GUILayout.Width((BodyColumnWidth / 2)), GUILayout.Width((BodyColumnWidth / 2)));

        GUILayout.Space((padding / 2));

        GUILayout.Label("Recipe Components", EditorStyles.boldLabel, GUILayout.Width((BodyColumnWidth / 2)));
        if (recipeAsset.inputComponents.Count == 0) {
            GUI.color = Color.green;
            if (GUILayout.Button("+", GUILayout.Width(miniButtonSize))) {
                recipeAsset.inputComponents.Add(new RecipeInput());
            }
            GUI.color = Color.white;
        }

        RecipeInput recipeInput;
        for (int i = 0; i < recipeAsset.inputComponents.Count; i++) {
            recipeInput = recipeAsset.inputComponents[i];
            GUILayout.BeginHorizontal();

            //GUILayout.BeginVertical();
            //EditorGUILayout.LabelField("Amount", GUILayout.Width((BodyColumnWidth / 2) - ((miniButtonSize * 2) / 2) - (padding / 2)));
            //recipeInput.amount = EditorGUILayout.IntField(recipeInput.amount, GUILayout.Width((BodyColumnWidth / 2) - ((miniButtonSize * 2) / 2) - (padding / 2)));
            //GUILayout.EndVertical();

            DrawVerticalLabeledIntField("Amount", ref recipeInput.amount, GUILayout.Width((BodyColumnWidth / 2) - ((miniButtonSize * 2) / 2) - (padding / 2)), GUILayout.Width((BodyColumnWidth / 2) - ((miniButtonSize * 2) / 2) - (padding / 2)));

            GUILayout.BeginVertical();
            EditorGUILayout.LabelField("Recipe Component", GUILayout.Width((BodyColumnWidth / 2) - ((miniButtonSize * 2) / 2) - (padding / 2)));
            recipeInput.recipeID = recipeComponentsList[EditorGUILayout.Popup(GetIndexOfRecipeComponent(recipeInput.recipeID), recipeComponentNames.ToArray(), GUILayout.Width((BodyColumnWidth / 2) - ((miniButtonSize * 2) / 2) - (padding / 2)))].id;
            GUILayout.EndVertical();

            GUI.color = Color.red;
            if (GUILayout.Button("-", GUILayout.Width(miniButtonSize))) {
                recipeAsset.inputComponents.RemoveAt(i);
            }
            GUI.color = Color.white;

            GUI.color = Color.green;
            if (GUILayout.Button("+", GUILayout.Width(miniButtonSize))) {
                recipeAsset.inputComponents.Insert(i + 1, new RecipeInput());
            }
            GUI.color = Color.white;

            GUILayout.EndHorizontal();
            GUILayout.Space(padding);
        }

        GUILayout.FlexibleSpace();
        GUILayout.EndVertical();

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.EndScrollView();

        GUILayout.FlexibleSpace();
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label(recipeIcon, GUILayout.MaxWidth(96), GUILayout.MaxHeight(96));
        GUILayout.EndHorizontal();
    }

    #endregion GUI_DRAWING

    void OnGUI() {
        GUI.skin.button.wordWrap = true;
        GUI.DrawTexture(new Rect(0, 0, maxSize.x, maxSize.y), windowBGTexture, ScaleMode.StretchToFill);
        DrawTopToolbar();

        if (loadedDatabase == PanelState.CRAFTABLE) {
            DrawCraftableProperties();
        }
        else if (loadedDatabase == PanelState.COMPONENT) {
            GUILayout.BeginArea(new Rect(padding, BodyFirstRowY, WindowWidth, BodyHeight), GUI.skin.box);
            DrawRecipeComponentProperties();
            GUILayout.EndArea();
        }
        else if (loadedDatabase == PanelState.RECIPE) {
            GUILayout.BeginArea(new Rect(padding, BodyFirstRowY, WindowWidth, BodyHeight), GUI.skin.box);
            DrawRecipeProperties();
            GUILayout.EndArea();
        }

        if (GUI.changed) {
            if (craftableAsset != null) {
                EditorUtility.SetDirty(craftableAsset);
                if (craftableAsset.recipeDataObject != null) {
                    EditorUtility.SetDirty(craftableAsset.recipeDataObject);
                }
                //if (craftableAsset.attackDataObject != null) {
                //    EditorUtility.SetDirty(craftableAsset.attackDataObject);
                //}
            }
            if (recipeComponentAsset != null) {
                EditorUtility.SetDirty(recipeComponentAsset);
            }
            if (recipeAsset != null) {
                EditorUtility.SetDirty(recipeAsset);
            }
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }
    }

}