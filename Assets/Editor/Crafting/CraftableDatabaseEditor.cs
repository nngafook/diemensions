﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

[CustomEditor(typeof(CraftableDatabase))]
public class CraftableDatabaseEditor : Editor {

    private static List<bool> editModeFlags = new List<bool>();
    private static List<bool> expandedFlags = new List<bool>();
    private static bool listExpanded = false;

    private CraftableDatabase craftableDatabase;
    private Craftable craftable;

    private string buttonLabel;

    void OnEnable() {
        craftableDatabase = (target as CraftableDatabase);
        craftableDatabase.ForceImport();
    }

    private void DrawReadOnlyProperties() {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("ID", EditorStyles.boldLabel);
        EditorGUILayout.SelectableLabel(craftable.id, EditorStyles.textField, GUILayout.Height(EditorGUIUtility.singleLineHeight));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Name", EditorStyles.boldLabel);
        EditorGUILayout.SelectableLabel(craftable.name, EditorStyles.textField, GUILayout.Height(EditorGUIUtility.singleLineHeight));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Recipe ID", EditorStyles.boldLabel);
        EditorGUILayout.SelectableLabel(craftable.recipeID, EditorStyles.textField, GUILayout.Height(EditorGUIUtility.singleLineHeight));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("UI Icon", EditorStyles.boldLabel);
        EditorGUILayout.SelectableLabel(craftable.uiIcon, EditorStyles.textField, GUILayout.Height(EditorGUIUtility.singleLineHeight));
        EditorGUILayout.EndHorizontal();
    }

    public override void OnInspectorGUI() {
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Craftables List", EditorStyles.boldLabel);
        
        string label = "";

        EditorGUI.indentLevel = 1;
        while (expandedFlags.Count < craftableDatabase.craftablesList.Count) {
            expandedFlags.Add(false); // expand by default
            editModeFlags.Add(false);
        }

        while (expandedFlags.Count > craftableDatabase.craftablesList.Count) {
            expandedFlags.RemoveAt(expandedFlags.Count - 1);
            editModeFlags.RemoveAt(editModeFlags.Count - 1);
        }

        listExpanded = EditorGUILayout.Foldout(listExpanded, "Craftables");

        if (listExpanded) {
            EditorGUI.indentLevel++;
            for (int i = 0; i < craftableDatabase.craftablesList.Count; i++) {
                craftable = craftableDatabase.craftablesList[i];
                label = (craftable.name != string.Empty) ? craftable.name : "Craftable " + i.ToString();
                expandedFlags[i] = EditorGUILayout.Foldout(expandedFlags[i], label);

                if (expandedFlags[i]) {
                    if (editModeFlags[i]) {
                        GUI.color = Color.green;
                        craftable.id = EditorGUILayout.TextField("ID", craftable.id);
                        craftable.name = EditorGUILayout.TextField("Name", craftable.name);
                        craftable.recipeID = EditorGUILayout.TextField("Recipe ID", craftable.recipeID);
                        craftable.uiIcon = EditorGUILayout.TextField("Icon Path", craftable.uiIcon);
                        GUI.color = Color.white;
                    }
                    else {
                        DrawReadOnlyProperties();
                    }
                    GUILayout.Space(10);

                    EditorGUILayout.BeginHorizontal();
                    GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
                    buttonLabel = (editModeFlags[i]) ? "Save" : "Edit";
                    if (GUILayout.Button(buttonLabel)) {
                        editModeFlags[i] = !editModeFlags[i];
                    }
                    GUI.color = Color.white;

                    GUI.color = Color.red;
                    if (GUILayout.Button("Delete")) {
                        craftableDatabase.DeleteIndex(i);
                        //recipeComponentDatabase.DeleteIndex(i);
                        GUI.FocusControl("");
                    }
                    GUI.color = Color.white;
                    EditorGUILayout.EndHorizontal();

                    GUILayout.Space(10);
                }
                else {
                    editModeFlags[i] = false;
                }
            }
            EditorGUI.indentLevel = 1;
        }

        DrawJsonButtons();

        if (GUI.changed)
            EditorUtility.SetDirty(target);
        serializedObject.ApplyModifiedProperties();
    }

    private void DrawJsonButtons() {
        EditorGUILayout.Space();

        CraftableDatabase craftableDatabase = target as CraftableDatabase;

        EditorGUILayout.BeginHorizontal();
        GUI.color = Color.cyan;
        if (GUILayout.Button("Import", GUILayout.Height(30))) {
            craftableDatabase.Import(ReadDataFromFile());
        }

        EditorGUILayout.Space();

        GUI.color = Color.red;
        if (GUILayout.Button("Export", GUILayout.Height(30))) {
            craftableDatabase.Export();
        }

        GUI.color = Color.white;
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();
    }

    private string ReadDataFromFile() {
        string path = EditorUtility.OpenFilePanel("Load Data", "/Assets/Data/Crafting", "txt");

        WWW reader = new WWW("file:///" + path);
        while (!reader.isDone) {

        }
        return reader.text;
    }
}
