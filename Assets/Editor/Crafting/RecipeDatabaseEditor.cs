﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

[CustomEditor(typeof(RecipeDatabase))]
public class RecipeDatabaseEditor : Editor {

    private SerializedProperty recipeSerializedProperty;
    private SerializedProperty inputComponentsProperty;
    private SerializedProperty serializedRecipeList;
    private RecipeDatabase recipeDatabase;
    private RecipeComponentDatabase recipeComponentDatabase;
    private List<string> componentListOptions;
    //private List<Recipe> recipeList;
    private List<RecipeInput> recipeInputs;
    private Recipe recipe;
    private RecipeInput recipeInput;
    private string label = "";

    private static List<bool> expandedFlags = new List<bool>();
    private static List<bool> editModeFlags = new List<bool>();
    private static List<bool> inputComponentExpandedFlags = new List<bool>();
    private static bool listExpanded = false;

    private string buttonLabel = "";

    void OnEnable() {
        recipeDatabase = (target as RecipeDatabase);
        recipeComponentDatabase = recipeDatabase.GetComponent<RecipeComponentDatabase>();
        //recipeList = recipeDatabase.recipeList;
        SetComponentListOptions();
        recipeDatabase.ForceImport();
    }

    private int GetIndexOfComponent(string id) {
        int rVal = 0;
        for (int i = 0; i < recipeComponentDatabase.componentsList.Count; i++) {
            if (recipeComponentDatabase.componentsList[i].id == id) {
                rVal = i;
                break;
            }
        }
        return rVal;
    }

    private void SetComponentListOptions() {
        
        componentListOptions = new List<string>();
        for (int i = 0; i < recipeComponentDatabase.componentsList.Count; i++) {
            componentListOptions.Add(recipeComponentDatabase.componentsList[i].name);
        }
    }

    private void DrawReadOnlyProperties() {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("ID", EditorStyles.boldLabel);
        EditorGUILayout.SelectableLabel(recipe.id, EditorStyles.textField, GUILayout.Height(EditorGUIUtility.singleLineHeight));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Name", EditorStyles.boldLabel);
        EditorGUILayout.SelectableLabel(recipe.name, EditorStyles.textField, GUILayout.Height(EditorGUIUtility.singleLineHeight));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("UI Icon", EditorStyles.boldLabel);
        EditorGUILayout.SelectableLabel(recipe.uiIcon, EditorStyles.textField, GUILayout.Height(EditorGUIUtility.singleLineHeight));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Output Craftable ID", EditorStyles.boldLabel);
        EditorGUILayout.SelectableLabel(recipe.OutputCraftableID, EditorStyles.textField, GUILayout.Height(EditorGUIUtility.singleLineHeight));
        EditorGUILayout.EndHorizontal();
    }

    public override void OnInspectorGUI() {
        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Recipe List", EditorStyles.boldLabel);
        serializedRecipeList = serializedObject.FindProperty("recipeList");
        //EditorGUILayout.PropertyField(recipeInputReorderableList.FindPropertyRelative("Array.size"));

        EditorGUI.indentLevel = 1;
        if (expandedFlags.Count != recipeDatabase.recipeList.Count) {
            while (expandedFlags.Count < recipeDatabase.recipeList.Count) {
                expandedFlags.Add(false); // expand by default
                editModeFlags.Add(false);
            }

            while (expandedFlags.Count > recipeDatabase.recipeList.Count) {
                expandedFlags.RemoveAt(expandedFlags.Count - 1);
                editModeFlags.RemoveAt(editModeFlags.Count - 1);
            }
        }

        listExpanded = EditorGUILayout.Foldout(listExpanded, "Recipes");

        if (listExpanded) {
            EditorGUI.indentLevel++;
            for (int i = 0; i < recipeDatabase.recipeList.Count; i++) {
                recipe = recipeDatabase.recipeList[i];
                recipeInputs = recipe.InputComponents;
                label = (recipe.name != string.Empty) ? recipe.name : "Recipe " + i.ToString();
                expandedFlags[i] = EditorGUILayout.Foldout(expandedFlags[i], label);

                if (expandedFlags[i]) {

                    if (editModeFlags[i]) {
                        GUI.color = Color.green;
                        recipe.id = EditorGUILayout.TextField("ID", recipe.id);
                        recipe.name = EditorGUILayout.TextField("Name", recipe.name);
                        recipe.uiIcon = EditorGUILayout.TextField("Icon Path", recipe.uiIcon);
                        recipe.OutputCraftableID = EditorGUILayout.TextField("Output Craftable ID", recipe.OutputCraftableID);
                        GUI.color = Color.white;
                    }
                    else {
                        DrawReadOnlyProperties();
                    }

                    recipeSerializedProperty = serializedRecipeList.GetArrayElementAtIndex(i);
                    inputComponentsProperty = recipeSerializedProperty.FindPropertyRelative("InputComponents");
                    EditorGUILayout.LabelField("Input Components", EditorStyles.boldLabel);
                    EditorGUI.indentLevel += 1;
                    //EditorGUILayout.PropertyField(inputComponentsProperty.FindPropertyRelative("Array.size"));
                    serializedObject.ApplyModifiedProperties();
                    for (int j = 0; j < inputComponentsProperty.arraySize; j++) {
                        if (inputComponentExpandedFlags.Count != recipeInputs.Count) {
                            while (inputComponentExpandedFlags.Count < recipeInputs.Count) {
                                inputComponentExpandedFlags.Add(false); // expand by default
                            }
                            while (inputComponentExpandedFlags.Count > recipeInputs.Count) {
                                inputComponentExpandedFlags.RemoveAt(inputComponentExpandedFlags.Count - 1);
                            }
                        }

                        recipeInput = recipeInputs[j];
                        if (editModeFlags[i]) {
                            GUI.color = Color.green;
                            label = "Component " + j;
                            inputComponentExpandedFlags[j] = EditorGUILayout.Foldout(inputComponentExpandedFlags[j], label);
                            if (inputComponentExpandedFlags[j]) {
                                recipeInput.amount = EditorGUILayout.IntField("Amount", recipeInput.amount);
                                recipeInput.recipeID = (recipeComponentDatabase.componentsList[EditorGUILayout.Popup("Component", GetIndexOfComponent(recipeInput.recipeID), componentListOptions.ToArray())].id);
                            }
                            GUI.color = Color.white;
                        }
                        else {
                            label = recipeInput.recipeID;
                            EditorGUILayout.BeginHorizontal();
                            EditorGUILayout.LabelField(label, EditorStyles.boldLabel);
                            EditorGUILayout.LabelField("x" + recipeInput.amount, EditorStyles.boldLabel);
                            EditorGUILayout.EndHorizontal();
                        }
                    }

                    GUILayout.Space(10);

                    EditorGUILayout.BeginHorizontal();
                    GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
                    buttonLabel = (editModeFlags[i]) ? "Save" : "Edit";
                    if (GUILayout.Button(buttonLabel)) {
                        editModeFlags[i] = !editModeFlags[i];
                    }
                    GUI.color = Color.white;

                    GUI.color = Color.red;
                    if (GUILayout.Button("Delete")) {
                        recipeDatabase.DeleteIndex(i);
                        GUI.FocusControl("");
                    }
                    GUI.color = Color.white;
                    EditorGUILayout.EndHorizontal();

                    GUILayout.Space(10);

                }
                else {
                    editModeFlags[i] = false;
                }
            }
            EditorGUI.indentLevel = 1;
        }

        DrawJsonButtons();

        if (GUI.changed)
            EditorUtility.SetDirty(target);
        serializedObject.ApplyModifiedProperties();
    }

    private void DrawJsonButtons() {
        EditorGUILayout.Space();

        RecipeDatabase recipeDatabase = target as RecipeDatabase;

        EditorGUILayout.BeginHorizontal();
        GUI.color = Color.cyan;
        if (GUILayout.Button("Import", GUILayout.Height(30))) {
            recipeDatabase.Import(ReadDataFromFile());
        }

        EditorGUILayout.Space();

        GUI.color = Color.red;
        if (GUILayout.Button("Export", GUILayout.Height(30))) {
            recipeDatabase.Export();
        }

        GUI.color = Color.white;
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();
    }

    private string ReadDataFromFile() {
        string path = EditorUtility.OpenFilePanel("Load Data", "/Assets/Data/Crafting", "txt");

        WWW reader = new WWW("file:///" + path);
        while (!reader.isDone) {

        }
        return reader.text;
    }
}
