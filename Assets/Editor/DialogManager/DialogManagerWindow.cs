﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.SceneManagement;

public class DialogManagerWindow : EditorWindow {

    private static DialogManagerWindow instance = null;

    private float windowEdgePadding = 5;
    private float padding = 10;
    private float buttonPanelHeight = 90;
    private float buttonDimension = 60;

    private Texture2D windowBGTexture;
    private Texture2D toolbarBGTexture;
    private Texture2D bodyPanelBGTexture;
    private Texture2D dialogTexture;

    private float WindowWidth { get { return (position.width - (windowEdgePadding * 2)); } }

    void Awake() {
        this.titleContent = new GUIContent("Dialog Manager");
        this.minSize = new Vector2(1136, 640);
    }

    [MenuItem("Window/Dialog Manager %&d")]
    static void Init() {
        // Get existing open window or if none, make a new one:
        DialogManagerWindow window = (DialogManagerWindow)EditorWindow.GetWindow(typeof(DialogManagerWindow));
        window.Show();
    }

    void OnEnable() {
        instance = this;

        InitTextures();
    }

    private void InitTextures() {
        dialogTexture = AssetDatabase.LoadAssetAtPath("Assets/Art/2D/UI/Icons/DialogIcon.png", typeof(Texture2D)) as Texture2D;

        windowBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        windowBGTexture.SetPixel(0, 0, new Color(0.172549f, 0.2784314f, 0.4392157f, 1f));
        windowBGTexture.Apply();

        toolbarBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        toolbarBGTexture.SetPixel(0, 0, new Color(0.07843138f, 0.1843137f, 0.3294118f, 1f));
        toolbarBGTexture.Apply();

        bodyPanelBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        bodyPanelBGTexture.SetPixel(0, 0, new Color(0.4588235f, 0.5411765f, 0.6588235f, 1f));
        bodyPanelBGTexture.Apply();
    }

    private void DrawButtons() {
        GUILayout.BeginArea(new Rect(windowEdgePadding, windowEdgePadding, WindowWidth, buttonPanelHeight), GUI.skin.box);
        GUI.DrawTexture(new Rect(2, 2, WindowWidth - 4, buttonPanelHeight - 4), toolbarBGTexture, ScaleMode.StretchToFill);
        GUILayout.BeginArea(new Rect((padding / 2), (padding / 2), WindowWidth - (padding * 2), buttonPanelHeight));

        GUIStyle centeredStyle = new GUIStyle(GUI.skin.GetStyle("whiteLabel"));
        centeredStyle.alignment = TextAnchor.MiddleCenter;

        GUILayout.BeginHorizontal();

        GUILayout.BeginVertical(GUILayout.Width(buttonDimension));
        GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        if (GUILayout.Button(dialogTexture, GUILayout.Width(buttonDimension), GUILayout.Height(buttonDimension))) {
            DialogModule asset = ScriptableObject.CreateInstance<DialogModule>();
            string path = "Assets/Resources/Data/Dialog/DialogModuleDataObject.asset";
            AssetDatabase.CreateAsset(asset, path);
            AssetDatabase.SaveAssets();
            Selection.activeObject = asset;
        }
        GUI.color = Color.white;
        GUILayout.Label("MODULE", centeredStyle, GUILayout.Width(buttonDimension), GUILayout.Height(padding * 2));
        GUILayout.EndVertical();

        GUILayout.BeginVertical(GUILayout.Width(buttonDimension));
        GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
        if (GUILayout.Button(dialogTexture, GUILayout.Width(buttonDimension), GUILayout.Height(buttonDimension))) {
            DialogBlock asset = ScriptableObject.CreateInstance<DialogBlock>();
            string path = "Assets/Resources/Data/Dialog/DialogBlockDataObject.asset";
            AssetDatabase.CreateAsset(asset, path);
            AssetDatabase.SaveAssets();
            Selection.activeObject = asset;
        }
        GUI.color = Color.white;
        GUILayout.Label("BLOCK", centeredStyle, GUILayout.Width(buttonDimension), GUILayout.Height(padding * 2));
        GUILayout.EndVertical();

        GUILayout.BeginVertical(GUILayout.Width(buttonDimension));
        GUI.color = CustomColor.GetColor(ColorName.PURPLE_PLUM);
        if (GUILayout.Button(dialogTexture, GUILayout.Width(buttonDimension), GUILayout.Height(buttonDimension))) {
            DialogLine asset = ScriptableObject.CreateInstance<DialogLine>();
            string path = "Assets/Resources/Data/Dialog/DialogLineDataObject.asset";
            AssetDatabase.CreateAsset(asset, path);
            AssetDatabase.SaveAssets();
            Selection.activeObject = asset;
        }
        GUI.color = Color.white;
        GUILayout.Label("LINE", centeredStyle, GUILayout.Width(buttonDimension), GUILayout.Height(padding * 2));
        GUILayout.EndVertical();

        GUI.color = Color.white;
        GUILayout.EndHorizontal();

        GUILayout.EndArea();
        GUILayout.EndArea();
    }

    void OnGUI() {
        GUI.skin.button.wordWrap = true;
        GUI.DrawTexture(new Rect(0, 0, maxSize.x - 4, maxSize.y - 4), windowBGTexture, ScaleMode.StretchToFill);

        DrawButtons();

        if (GUI.changed) {
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }
    }

}
