﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(RadialEntityMenu))]
public class RadialEntityMenuEditor : Editor {

    public override void OnInspectorGUI() {

        GUILayout.Space(10);
        RadialEntityMenu menu = target as RadialEntityMenu;
        RadialEntityMenuDock menuDock = menu.radialEntityMenuDock;
        if (menuDock.GetComponent<RectTransform>().localScale.x == 0) {
            GUI.color = CustomColor.GetColor(ColorName.ROB_BLUE);
            if (GUILayout.Button("Show Buttons")) {
                menu.OpenForEditor();
            }
            GUI.color = Color.white;
        }
        else {
            GUI.color = Color.magenta;
            if (GUILayout.Button("Hide Buttons")) {
                menu.CloseForEditor();
            }
            GUI.color = Color.green;
            if (GUILayout.Button("Save Values")) {
                menu.SaveValuesForEditor();
            }
            GUI.color = Color.white;
        }

        GUILayout.Space(10);

        base.DrawDefaultInspector();

        if (GUI.changed) {
            EditorUtility.SetDirty(target);
        }
    }

}
